Conception de la version 3 du Projet
====================================
Décembre 2015, Steve Kossouho

Raisonnement
------------
Ce document présente les différents changements à apporter à l'application Dématérialisation Manifestations Sportives v2
afin de pouvoir proposer la solution non seulement au département de la Loire (42, Saint-Étienne) mais également aux autres départements français.
La première étape consiste à améliorer l'application pour une utilisation dans les Yvelines (78, Versailles).

