# coding: utf-8
from __future__ import unicode_literals

from django.test import TestCase

from .factories import FederationFactory, DisciplineFactory, ActiviteFactory
from .factories import FederationMultiSportFactory


class FederationMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the short name of the federation
        '''
        federation = FederationFactory.build()
        self.assertEqual(federation.__str__(), federation.short_name)


class FederationMultiSportMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the short name of the federation
        '''
        federation = FederationMultiSportFactory.build()
        self.assertEqual(federation.__str__(), federation.short_name)


class DisciplineMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the name of the sort
        '''
        discipline = DisciplineFactory.build(name='Athletisme')
        self.assertEqual(discipline.__str__(), 'Athletisme')


class ActiviteMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the name of the activity
        '''
        activite = ActiviteFactory.build(name='Cross')
        self.assertEqual(activite.__str__(), 'Cross')
