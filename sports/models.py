# coding: utf-8
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Federation(models.Model):
    short_name = models.CharField("abréviation", max_length=200,
                                  unique=True)
    name = models.CharField("nom", max_length=200, unique=True)
    email = models.EmailField("e-mail", max_length=200, unique=True)

    class Meta:
        verbose_name = "fédération"
        default_related_name = "federations"
        app_label = "sports"

    def __str__(self):
        return self.short_name


class FederationMultiSport(Federation):

    class Meta:
        verbose_name = "fédération multi-sports"
        verbose_name_plural = "fédérations multi-sports"
        default_related_name = "federationsmultisports"
        app_label = "sports"


@python_2_unicode_compatible
class Discipline(models.Model):
    name = models.CharField("nom", max_length=200, unique=True)
    federation = models.OneToOneField("sports.federation",
                                      related_name="discipline",
                                      verbose_name="fédération")
    motorise = models.BooleanField("motorisé", default=False)

    class Meta:
        verbose_name = "discipline"
        default_related_name = "disciplines"
        app_label = "sports"

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Activite(models.Model):
    name = models.CharField("nom", max_length=200, unique=True)
    discipline = models.ForeignKey("sports.discipline",
                                   verbose_name="discipline")

    class Meta:
        verbose_name = "activité"
        verbose_name_plural = "activités"
        default_related_name = "activites"
        app_label = "sports"

    def __str__(self):
        return self.name
