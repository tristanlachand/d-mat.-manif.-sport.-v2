# coding: utf-8
from __future__ import unicode_literals

import factory

from .models import Federation, Discipline, Activite, FederationMultiSport



class FederationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Federation

    name = factory.Sequence(lambda n: 'Federation{0}'.format(n))
    short_name = factory.Sequence(lambda n: 'F{0}'.format(n))
    email = factory.Sequence(lambda n: 'fede{0}@federation.fr'.format(n))


class FederationMultiSportFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FederationMultiSport

    name = factory.Sequence(lambda n: 'MultiSport Federation{0}'.format(n))
    short_name = factory.Sequence(lambda n: 'MSF{0}'.format(n))
    email = factory.Sequence(lambda n: 'multifede{0}@federation.fr'.format(n))


class DisciplineFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Discipline

    name = factory.Sequence(lambda n: 'Discipline{0}'.format(n))
    federation = factory.SubFactory(FederationFactory)


class ActiviteFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Activite

    name = factory.Sequence(lambda n: 'Activite{0}'.format(n))
    discipline = factory.SubFactory(DisciplineFactory)
