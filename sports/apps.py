# coding: utf-8
from django.apps import AppConfig


class SportsConfig(AppConfig):
    name = 'sports'
    verbose_name = 'Sports'
