# coding: utf-8
from __future__ import unicode_literals

from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from .models import Activite
from .models import Federation
from .models import FederationMultiSport
from .models import Discipline


class FederationAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'email']
    search_fields = ['name']


class FederationMultiSportAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'email']
    search_fields = ['name']


class DisciplineAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'federation', 'motorise']
    list_filter = ['motorise']
    search_fields = ['name', 'federation__name']


class ActiviteAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'discipline']
    list_filter = ['discipline', 'discipline__federation', 'discipline__motorise']
    search_fields = ['name', 'discipline__name']


# Enregistrer les configurations d'admin
admin.site.register(Federation, FederationAdmin)
admin.site.register(FederationMultiSport, FederationMultiSportAdmin)
admin.site.register(Discipline, DisciplineAdmin)
admin.site.register(Activite, ActiviteAdmin)
