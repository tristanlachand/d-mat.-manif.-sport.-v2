from django.http import JsonResponse

from sports.models import Activite


def activites_par_discipline(request):
    if request.is_ajax() and request.GET and 'discipline_id' in request.GET:
        discipline = request.GET['discipline_id']
        if discipline != '':
            objs = Activite.objects.filter(discipline=discipline)
        else:
            objs = []
        return JsonResponse([{'id': o.id, 'name': str(o)} for o in objs], safe=False)
    else:
        return JsonResponse({'error': 'Not Ajax or no GET'})
