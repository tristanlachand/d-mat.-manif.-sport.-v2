# coding: utf-8
from django.contrib.contenttypes.admin import GenericTabularInline

from .forms import ContactAdminForm
from .models import Contact, Adresse


class ContactInline(GenericTabularInline):
    model = Contact
    form = ContactAdminForm
    extra = 0


class AddressInline(GenericTabularInline):
    model = Adresse
    extra = 0
    max_num = 1
