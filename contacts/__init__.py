# coding: utf-8
from django.apps.config import AppConfig


class ContactsConfig(AppConfig):
    name = 'contacts'
    verbose_name = 'Contacts'


default_app_config = 'contacts.ContactsConfig'
