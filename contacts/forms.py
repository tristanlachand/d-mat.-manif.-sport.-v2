# coding: utf-8
from django import forms
from extra_views.generic import GenericInlineFormSet
from localflavor.fr.forms import FRPhoneNumberField

from ddcs_loire.utils import GenericForm
from .models import Contact


class ContactAdminForm(forms.ModelForm):
    """ Formulaire de contact """
    phone = FRPhoneNumberField()

    class Meta:
        model = Contact
        exclude = ('content_type', 'object_id', 'content_object')

    def __init__(self, *args, **kwargs):
        super(ContactAdminForm, self).__init__(*args, **kwargs)
        self.fields['phone'] = FRPhoneNumberField()


class ContactForm(GenericForm):
    phone = FRPhoneNumberField()

    class Meta:
        model = Contact
        exclude = ('content_type', 'object_id', 'content_object')

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['phone'] = FRPhoneNumberField()
        self.helper.form_tag = False


class ContactInline(GenericInlineFormSet):
    model = Contact
    fields = ['first_name', 'last_name', 'phone']
    extra = 1
    max_num = 1
    can_delete = False
    form_class = ContactForm
