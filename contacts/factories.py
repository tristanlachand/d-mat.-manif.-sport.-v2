from __future__ import unicode_literals

import factory

from administrative_division.factories import CommuneFactory

from .models import Contact, Adresse


class ContactFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Contact

    first_name = 'john'
    last_name = 'doe'
    phone = '06 55 55 55 55'


class AdresseFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Adresse

    address = '42, rue John Doe'
    commune = factory.SubFactory(CommuneFactory)
