# coding: utf-8
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

from administrative_division.models import Commune
from core.util.application import FILTER_ARRONDISSEMENT_CHOICES


class Contact(models.Model):
    """ Personne à contacter pour une manifestation """
    first_name = models.CharField("prénom", max_length=200)
    last_name = models.CharField("nom de famille", max_length=200)
    phone = models.CharField("numéro de téléphone", max_length=14)
    content_type = models.ForeignKey('contenttypes.contenttype', null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        """ Renvoyer le texte par défaut pour le contact """
        return ' '.join([self.first_name.capitalize(), self.last_name.capitalize()])

    class Meta:
        verbose_name = "contact"
        verbose_name_plural = "contacts"
        app_label = 'contacts'


class Adresse(models.Model):
    """ Adresse postale """
    address = models.CharField("adresse", max_length=255)
    commune = models.ForeignKey('administrative_division.commune', verbose_name='commune', limit_choices_to=FILTER_ARRONDISSEMENT_CHOICES)
    content_type = models.ForeignKey('contenttypes.contenttype', null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        """ Renvoyer la chaîne pour l'adresse """
        return ' - '.join([self.address, ' '.join([self.commune.zip_code, self.commune.name.upper()])])

    class Meta:
        verbose_name = "adresse"
        verbose_name_plural = "adresses"
        app_label = 'contacts'
