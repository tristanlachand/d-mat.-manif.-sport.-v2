# coding: utf-8
import datetime

from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

from events.models import Manifestation


class LogQuerySet(models.QuerySet):
    def in_processing(self):
        return self.filter(manifestation__end_date__gte=datetime.date.today() - datetime.timedelta(days=1))

    def last_first(self):
        return self.order_by('-creation_date')


class Action(models.Model):
    creation_date = models.DateTimeField('date', auto_now_add=True)
    action = models.CharField('action', max_length=255)
    manifestation = models.ForeignKey('events.manifestation', verbose_name="manifestation")
    user = models.ForeignKey("auth.user", verbose_name="utilisateur")
    objects = LogQuerySet.as_manager()

    class Meta:
        verbose_name = 'action'
        default_related_name = "actions"
        app_label = "notifications"

    def __str__(self):
        return ' - '.join([self.user.username, self.manifestation.name, self.action])


class Notification(models.Model):
    creation_date = models.DateTimeField('date', auto_now_add=True)
    subject = models.CharField("sujet", max_length=255)
    manifestation = models.ForeignKey("events.manifestation", verbose_name="manifestation")
    user = models.ForeignKey("auth.user", related_name='notifications', verbose_name="utilisateur")
    content_type = models.ForeignKey('contenttypes.contenttype', null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    read = models.BooleanField('lu', default=False)
    objects = LogQuerySet.as_manager()

    class Meta:
        verbose_name = "notification"
        verbose_name_plural = "notifications"
        default_related_name = "notifications"
        app_label = "notifications"

    def __str__(self):
        return ' - '.join([self.user.username, self.manifestation.name, self.subject])
