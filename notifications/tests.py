# coding: utf-8
from __future__ import unicode_literals

from django.test import TestCase

from .factories import ActionFactory
from .factories import NotificationFactory


class ActionMethodTests(TestCase):

    def test_str_(self):
        action = ActionFactory.build()
        self.assertEqual(action.__str__(), ' - '.join([action.user.username,
                                                       action.manifestation.name,
                                                       action.action]))


class NotificationMethodTests(TestCase):

    def test_str_(self):
        notification = NotificationFactory.build()
        self.assertEqual(notification.__str__(), ' - '.join([
            notification.user.username,
            notification.manifestation.name,
            notification.subject]))
