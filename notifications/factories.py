# coding: utf-8
from __future__ import unicode_literals

import factory

from events.factories import UserFactory
from events.factories import MotorizedRaceFactory

from .models import Action
from .models import Notification


class ActionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Action

    user = factory.SubFactory(UserFactory)
    manifestation = factory.SubFactory(MotorizedRaceFactory)


class NotificationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Notification

    user = factory.SubFactory(UserFactory)
    manifestation = factory.SubFactory(MotorizedRaceFactory)
