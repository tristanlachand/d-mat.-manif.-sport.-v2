# coding: utf-8
from django.db import models


class ContextHelp(models.Model):
    """ Contenu d'aide contextuelle """

    # Constantes
    ICON_CHOICES = [[0, 'question'], [1, 'warning'], [2, 'error'], ]
    name = models.CharField(max_length=64, blank=False, unique=True, verbose_name="Nom")
    icon_name =
