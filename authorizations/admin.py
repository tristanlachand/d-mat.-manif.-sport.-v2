# coding: utf-8
from django.contrib import admin

from .models import ManifestationAutorisation


class EventAuthorizationInline(admin.StackedInline):
    """ Inline admin pour les autorisations """
    model = ManifestationAutorisation
    extra = 0
    max_num = 1
    readonly_fields = ['creation_date']
