$(document).ready(function() {
    $('#id_concerned_cities').multiselect({
        maxHeight: 500,
        nonSelectedText: 'Aucune ville concernée pour l\'instant',
        nSelectedText: ' - villes selectionnées',
        numberDisplayed: 9,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'Rechercher',
        templates : {
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="fa fa-times"></i></button></span>',
        },
    });
    $('#hint_id_concerned_cities').remove();
    $('#id_concerned_services').multiselect({
        maxHeight: 300,
        nonSelectedText: 'Aucun service concerné pour l\'instant',
        nSelectedText: ' - services selectionnés',
        numberDisplayed: 9,
    });
    $('#hint_id_concerned_services').remove();
});
