from __future__ import unicode_literals

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import render

from allauth.account.decorators import verified_email_required

from administration.models import Instructeur


def instructeur_required(function=None,
                         login_url=None,
                         redirect_field_name=REDIRECT_FIELD_NAME):
    """
    For pages restricted to instructors
    """
    def decorator(view_func):
        @verified_email_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.instructeur
            except Instructeur.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
