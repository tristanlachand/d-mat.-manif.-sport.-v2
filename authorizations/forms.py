# coding: utf-8
from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from ddcs_loire.utils import GenericForm

from crispy_forms.layout import Submit, Layout, Fieldset
from crispy_forms.bootstrap import FormActions

from .models import ManifestationAutorisation


class AuthorizationForm(GenericForm):

    class Meta:
        model = ManifestationAutorisation
        exclude = ['manifestation', 'creation_date', 'dispatch_date', 'state', 'concerned_cities', 'concerned_services', 'ddsp_concerned', 'edsr_concerned', 'ggd_concerned', 'sdis_concerned', 'cg_concerned', 'bylaw',]

    def __init__(self, *args, **kwargs):
        super(AuthorizationForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
                FormActions(
                        Submit('save', "demander une autorisation".capitalize())
                )
        )


class AuthorizationDispatchForm(GenericForm):

    class Meta:
        model = ManifestationAutorisation
        exclude = ['manifestation', 'creation_date', 'dispatch_date', 'state', 'bylaw',]

    def __init__(self, *args, **kwargs):
        super(AuthorizationDispatchForm, self).__init__(*args, **kwargs)
        if settings.DEPARTMENT_SETTINGS['EDSR_OR_GGD'].startswith('GGD'):
            self.fields['ggd_concerned'].initial = True
            self.fields['edsr_concerned'].initial = False
            self.fields['edsr_concerned'].widget = forms.HiddenInput()
        elif settings.DEPARTMENT_SETTINGS['EDSR_OR_GGD'] == 'EDSR':
            self.fields['edsr_concerned'].initial = True
            self.fields['ggd_concerned'].initial = False
            self.fields['ggd_concerned'].widget = forms.HiddenInput()
        self.helper.layout = Layout(
            Fieldset("avis requis".capitalize(), 'ddsp_concerned', 'edsr_concerned', 'ggd_concerned', 'sdis_concerned', 'cg_concerned', 'concerned_cities', 'concerned_services',),
            FormActions(
                Submit('save', "distribuer".capitalize())
            )
        )


class AuthorizationPublishBylawForm(GenericForm):

    class Meta:
        model = ManifestationAutorisation
        fields = ['bylaw']

    def __init__(self, *args, **kwargs):
        super(AuthorizationPublishBylawForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset(
                "choisissez le fichier de l'arrêté".capitalize(),
                'bylaw',
            ),
            FormActions(
                Submit('save', "publier l'arrêté".capitalize())
            )
        )

    def clean_bylaw(self):
        """ Vérifier le champ de fichier d'arrêté """
        data = self.cleaned_data['bylaw']
        if not data:
            raise forms.ValidationError("Vous devez spécifier un fichier contenant l'arrêté")
        return data
