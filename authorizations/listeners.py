# coding: utf-8
from django.db.models.signals import post_save
from django.dispatch import receiver

from authorizations.models import ManifestationAutorisation


@receiver(post_save, sender=ManifestationAutorisation)
def log_authorization_creation(created, instance, **_):
    if created:
        instance.log_creation()
        instance.notify_creation()
