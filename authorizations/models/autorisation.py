# coding: utf-8
from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
from django_fsm import FSMField, transition
from authorizations.signals import authorization_published

from core.util.application import FILTER_ARRONDISSEMENT_CHOICES
from ddcs_loire.utils import add_log_entry
from ddcs_loire.utils import add_notification_entry


class ManifestationAutorisationQuerySet(models.QuerySet):
    """ Queryset des autorisations d'événements sportifs """

    def for_instructor(self, instructeur):
        """ Renvoyer les autorisations en instruction pour l'instructeur """
        return self.filter(manifestation__departure_city__arrondissement=instructeur.prefecture.arrondissement)

    def to_process(self):
        """ Renvoyer les autorisations pour lesquelles l'événement prévu n'est pas terminé """
        return self.filter(manifestation__end_date__gt=timezone.now())

    def closest_first(self):
        """ Renvoyer les autorisations, triées par date de l'événement croissante """
        return self.order_by('manifestation__begin_date')


class ManifestationAutorisation(models.Model):
    """ Autorisation pour manifestation sportive """

    state = FSMField(default='created', verbose_name="Statut de l'autorisation")
    creation_date = models.DateField("date de création", auto_now_add=True)
    dispatch_date = models.DateField("date d'envoi des demandes d'avis", blank=True, null=True)
    manifestation = models.OneToOneField('events.manifestation', related_name="manifestationautorisation", verbose_name="manifestation associée")

    concerned_services = models.ManyToManyField('administration.service', verbose_name="services concernés",
                                                blank=True)
    concerned_cities = models.ManyToManyField('administrative_division.commune', verbose_name="villes concernées",
                                              limit_choices_to=FILTER_ARRONDISSEMENT_CHOICES, blank=True)
    ddsp_concerned = models.BooleanField("avis DDSP requis", default=False)
    edsr_concerned = models.BooleanField("avis EDSR requis", default=False)
    ggd_concerned = models.BooleanField("avis GGD requis", default=False)
    sdis_concerned = models.BooleanField("avis SDIS requis", default=False)
    cg_concerned = models.BooleanField("avis Conseil Général requis", default=False)
    bylaw = models.FileField(upload_to='arretes/%Y/%m/%d', blank=True, null=True, verbose_name="arrêté d'autorisation", max_length=512)

    objects = ManifestationAutorisationQuerySet.as_manager()

    def __str__(self):
        return self.manifestation.name

    def get_absolute_url(self):
        """ Renvoyer l'URL des autorisations """
        return reverse('authorizations:dashboard')

    def get_avis(self):
        """ Renvoyer les avis pour l'autorisation """
        return self.aviss.all()

    def get_concerned_prefecture(self):
        """ Renvoyer la préfecture de la manif """
        try:
            return self.manifestation.departure_city.get_arrondissement().prefecture
        except:
            return None

    def agreements_validated(self):
        """ Renvoyer le nombre d'AVIS rendus """
        return self.get_avis().filter(state='acknowledged').count()

    def agreements_issued(self):
        """ Renvoyer le nombre d'avis """
        return self.get_avis().count()

    def all_agreements_validated(self):
        """ Renvoyer si tous les avis sont rendus """
        return self.agreements_validated() == self.agreements_issued()

    def agreements_missing(self):
        """ Renvoie le nombre d'avis non encore rendus """
        return self.agreements_issued() - self.agreements_validated()

    def log_creation(self):
        """ Notifier de la création """
        add_log_entry(agents=[self.manifestation.structure.organisateur], action="demande d'autorisation envoyée", manifestation=self.manifestation)

    def log_dispatch(self, prefecture):
        """ Notifier du dispatch """
        add_log_entry(agents=prefecture.instructeurs.all(), action="demandes d'avis envoyées", manifestation=self.manifestation)

    def log_publish(self, prefecture):
        """ Notifier la publication de l'arrêté préfectoral """
        add_log_entry(agents=prefecture.instructeurs.all(), action="arrêté d'autorisation publié", manifestation=self.manifestation)

    def notify_creation(self):
        """ Notifier et envoyer un mail """
        prefecture = self.get_concerned_prefecture()
        if prefecture is not None:
            add_notification_entry(subject="demande d'autorisation envoyée", agents=prefecture.instructeurs.all(), manifestation=self.manifestation,
                                   content_object=self.manifestation.structure, institutional=prefecture)

    def notify_dispatch(self, prefecture):
        """ Notifier et envoyer un mail lorsque les demandes d'avis sont envoyées """
        add_notification_entry(agents=[self.manifestation.structure.organisateur], manifestation=self.manifestation, subject="début de l'instruction",
                               content_object=prefecture)

    def notify_publish(self, prefecture):
        """ Notifier et envoyer un mail lorsque l'arrêté est publié """
        add_notification_entry(agents=[self.manifestation.structure.organisateur], manifestation=self.manifestation, subject="arrêté d'autorisation publié",
                               content_object=prefecture)

    @transition(field=state, source=['created', 'dispatched'], target='dispatched')
    def dispatch(self):
        """ Dispenser les demandes d'avis """
        prefecture = self.get_concerned_prefecture()
        if prefecture is not None:
            if self.state == 'created':
                self.notify_dispatch(prefecture)
                self.log_dispatch(prefecture)
                self.dispatch_date = timezone.now()
                self.save()
            elif self.state == 'dispatched':
                self.concerned_cities.clear()
                self.concerned_services.clear()
                self.save()

    @transition(field=state, source='dispatched', target='published')
    def publish(self):
        """ Publier l'arrêté """
        prefecture = self.get_concerned_prefecture()
        if prefecture is not None:
            self.notify_publish(prefecture)
            self.log_publish(prefecture)
            authorization_published.send(sender=self.__class__, instance=self)

    class Meta:
        verbose_name = "autorisation de manifestation sportive"
        verbose_name_plural = "autorisations de manifestations sportives"
        default_related_name = 'manifestationautorisations'
        app_label = 'authorizations'
