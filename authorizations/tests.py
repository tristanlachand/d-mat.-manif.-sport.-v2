from __future__ import unicode_literals

from django.test import TestCase

from administration.factories import InstructeurFactory
from administration.factories import PrefectureFactory

from administrative_division.factories import ArrondissementFactory
from administrative_division.factories import CommuneFactory

from events.factories import AutorisationNMFactory

from notifications.models import Action
from notifications.models import Notification

from .factories import EventAuthorizationFactory

from .models import ManifestationAutorisation


def create_authorization_tree():
    arrondissement = ArrondissementFactory.create()
    prefecture = PrefectureFactory.create(
        arrondissement=arrondissement
    )
    instructeur = InstructeurFactory.create(prefecture=prefecture)
    commune = CommuneFactory.create(
        arrondissement=arrondissement
    )
    manifestation = AutorisationNMFactory.create(
        departure_city=commune
    )
    authorization = EventAuthorizationFactory.create(
        manifestation=manifestation
    )
    return authorization, manifestation, instructeur


class EventAuthorizationMethodTests(TestCase):

    def test_for_instructor(self):
        authorization, _manifestation, instructeur = create_authorization_tree()
        self.assertEqual(ManifestationAutorisation.objects.for_instructor(instructeur)[0].pk,
                         authorization.pk)

    def test_str_(self):
        authorization = EventAuthorizationFactory.create()
        self.assertEqual(authorization.__str__(),
                         authorization.manifestation.__str__())

    def test_get_concerned_prefecture(self):
        authorization = EventAuthorizationFactory.create()
        prefecture = PrefectureFactory.create(
            arrondissement=authorization.manifestation.departure_city.arrondissement
        )
        self.assertEqual(authorization.get_concerned_prefecture(), prefecture)

    def test_not_all_agreements_validated(self):
        authorization = EventAuthorizationFactory.create()
        self.assertFalse(authorization.all_agreements_validated())

    def test_all_agreements_validated(self):
        authorization = EventAuthorizationFactory.create()
        a = authorization.aviss.first()
        a.state = 'acknowledged'
        a.save()
        self.assertTrue(authorization.all_agreements_validated())

    def test_log_authorization_creation(self):
        authorization = EventAuthorizationFactory.create()
        action = Action.objects.last()
        self.assertEqual(action.user,
                         authorization.manifestation.structure.organisateur.user)
        self.assertEqual(action.manifestation,
                         authorization.manifestation.manifestation_ptr)
        self.assertEqual(action.action, "demande d'autorisation envoyée")
        # when authorization is only updated,
        # no log action is created
        authorization.save()
        action2 = Action.objects.last()
        self.assertEqual(action, action2)

    def test_notify_authorization_creation(self):
        authorization, manifestation, instructeur = create_authorization_tree()
        notification = Notification.objects.last()
        self.assertEqual(notification.user, instructeur.user)
        self.assertEqual(notification.manifestation, manifestation.manifestation_ptr)
        self.assertEqual(notification.subject, "demande d'autorisation envoyée")
        self.assertEqual(notification.content_object,
                         authorization.manifestation.structure)

    def test_notify_authorization_creation_with_no_prefecture(self):
        authorization = EventAuthorizationFactory.create()
        self.assertEqual(Notification.objects.filter(manifestation=authorization.manifestation).count(),
                         0)
