"""
Signals relating to authorization.
"""
from django.dispatch import Signal

# Sent just after an authorization was published.
authorization_published = Signal(providing_args=['instance'])
