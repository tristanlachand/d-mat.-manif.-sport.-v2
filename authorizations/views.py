from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView

from declarations.models import ManifestationDeclaration

from events.decorators import organisateur_required
from events.models import Manifestation

from .decorators import instructeur_required

from .forms import AuthorizationPublishBylawForm
from .forms import AuthorizationForm
from .forms import AuthorizationDispatchForm

from .models import ManifestationAutorisation


class Dashboard(ListView):
    model = ManifestationAutorisation

    @method_decorator(instructeur_required)
    def dispatch(self, *args, **kwargs):
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        instructeur = self.request.user.instructeur
        return ManifestationAutorisation.objects.to_process().for_instructor(instructeur).closest_first()

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        instructeur = self.request.user.instructeur
        context['declarations'] = ManifestationDeclaration.objects.to_process().for_instructor(instructeur).closest_first()
        return context


class AuthorizationCreateView(CreateView):
    model = ManifestationAutorisation
    form_class = AuthorizationForm

    @method_decorator(organisateur_required)
    def dispatch(self, *args, **kwargs):
        return super(AuthorizationCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.manifestation = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        return super(AuthorizationCreateView, self).form_valid(form)

    def get_success_url(self):
        try:
            self.object.manifestation.autorisationnm
        except:
            try:
                self.object.manifestation.motorizedconcentration
            except:
                try:
                    self.object.manifestation.motorizedmanifestation
                except:
                    try:
                        self.object.manifestation.motorizedrace
                    except:
                        return reverse('events:home_page')
                    else:
                        return reverse('events:motorizedrace_detail', kwargs={'pk': self.object.manifestation.pk})
                else:
                    return reverse('events:motorizedevent_detail', kwargs={'pk': self.object.manifestation.pk})
            else:
                return reverse('events:motorizedconcentration_detail', kwargs={'pk': self.object.manifestation.pk})
        else:
            return reverse('events:autorisationnm_detail', kwargs={'pk': self.object.manifestation.pk})


class AuthorizationDetailView(DetailView):
    model = ManifestationAutorisation

    @method_decorator(instructeur_required)
    def dispatch(self, *args, **kwargs):
        return super(AuthorizationDetailView, self).dispatch(*args, **kwargs)


class AuthorizationUpdateView(UpdateView):
    model = ManifestationAutorisation
    form_class = AuthorizationDispatchForm
    success_url = "/authorizations/%(id)s"

    @method_decorator(instructeur_required)
    def dispatch(self, *args, **kwargs):
        return super(AuthorizationUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.dispatch()
        return super(AuthorizationUpdateView, self).form_valid(form)


class AuthorizationPublishBylawView(UpdateView):
    model = ManifestationAutorisation
    form_class = AuthorizationPublishBylawForm
    template_name_suffix = '_publish_form'

    @method_decorator(instructeur_required)
    def dispatch(self, *args, **kwargs):
        return super(AuthorizationPublishBylawView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.publish()
        return super(AuthorizationPublishBylawView, self).form_valid(form)
