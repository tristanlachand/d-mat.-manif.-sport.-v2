from __future__ import unicode_literals

import factory

from events.factories import AutorisationNMFactory

from .models import ManifestationAutorisation


class EventAuthorizationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ManifestationAutorisation

    manifestation = factory.SubFactory(AutorisationNMFactory)
