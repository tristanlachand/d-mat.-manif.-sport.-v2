# coding: utf-8
import factory

from .models import Association1ersSecours, SecoursPublics


class Association1ersSecoursFactory(factory.django.DjangoModelFactory):
    name = 'Croix Rouge'

    class Meta:
        model = Association1ersSecours


class SecoursPublicsFactory(factory.django.DjangoModelFactory):
    name = ''

    class Meta:
        model = SecoursPublics
