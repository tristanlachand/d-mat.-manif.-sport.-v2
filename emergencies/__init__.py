# coding: utf-8
from django.apps.config import AppConfig


class EmergenciesConfig(AppConfig):
    name = 'emergencies'
    verbose_name = "Urgences"


default_app_config = 'emergencies.EmergenciesConfig'
