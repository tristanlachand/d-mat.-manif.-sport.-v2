# coding: utf-8
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from contacts.models import Adresse, Contact


class Association1ersSecours(models.Model):
    """ Association Premiers secours """
    name = models.CharField('nom', max_length=200)
    website = models.URLField('site web', max_length=200, blank=True)
    email = models.EmailField('e-mail', max_length=200)
    address = GenericRelation('contacts.adresse', verbose_name="adresses")
    contact = GenericRelation('contacts.contact', verbose_name="contacts")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "association premiers secours"
        verbose_name_plural = "associations premiers secours"
        app_label = 'emergencies'


class SecoursPublics(models.Model):
    """ Secours publics """
    name = models.CharField("nom", max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "secours publics"
        verbose_name_plural = "secours publics"
        app_label = 'emergencies'
