from __future__ import unicode_literals

from django.test import TestCase

from .factories import Association1ersSecoursFactory
from .factories import SecoursPublicsFactory


class FirstAidAssociationMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the name of the association
        '''
        first_aid_association = Association1ersSecoursFactory.build(
            name='Ordre de Malte',
        )
        self.assertEqual(first_aid_association.__str__(), 'Ordre de Malte')


class PublicAssistanceMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the name of the public assistance
        '''
        public_assistance = SecoursPublicsFactory.build(name='Gendarmerie')
        self.assertEqual(public_assistance.__str__(), 'Gendarmerie')
