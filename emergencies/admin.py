# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from contacts.admin import AddressInline, ContactInline

from .models import Association1ersSecours
from .models import SecoursPublics


class Association1ersSecoursAdmin(ImportExportActionModelAdmin):
    inlines = [
        AddressInline,
        ContactInline,
    ]


admin.site.register(Association1ersSecours, Association1ersSecoursAdmin)
admin.site.register(SecoursPublics)
