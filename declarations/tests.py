from __future__ import unicode_literals

from django.test import TestCase

from notifications.models import Action

from .factories import EventDeclarationFactory


class EventDeclarationMethodTests(TestCase):

    def test_log_declaration_creation(self):
        declaration = EventDeclarationFactory.create()
        action = Action.objects.last()
        self.assertEqual(action.user, declaration.manifestation.structure.organisateur.user)
        self.assertEqual(action.manifestation, declaration.manifestation.manifestation_ptr)
        self.assertEqual(action.action, "déclaration envoyée")
        # when declaration is only updated,
        # no log action is created
        declaration.save()
        action2 = Action.objects.last()
        self.assertEqual(action, action2)
