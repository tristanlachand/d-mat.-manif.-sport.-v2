# coding: utf-8
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView

from authorizations.decorators import instructeur_required
from events.decorators import organisateur_required
from events.models import Manifestation
from .forms import DeclarationForm
from .forms import DeclarationPublishReceiptForm
from .models import ManifestationDeclaration


class DeclarationCreateView(CreateView):
    model = ManifestationDeclaration
    form_class = DeclarationForm

    @method_decorator(organisateur_required)
    def dispatch(self, *args, **kwargs):
        return super(DeclarationCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.manifestation = get_object_or_404(Manifestation,
                                                        pk=self.kwargs['manifestation_pk'])
        return super(DeclarationCreateView, self).form_valid(form)

    def get_success_url(self):
        try:
            self.object.manifestation.declarationnm
        except:
            return reverse('events:motorizedconcentration_detail', kwargs={'pk': self.object.manifestation.pk})
        else:
            return reverse('events:declarationnm_detail', kwargs={'pk': self.object.manifestation.pk})


class DeclarationDetailView(DetailView):
    model = ManifestationDeclaration

    @method_decorator(instructeur_required)
    def dispatch(self, *args, **kwargs):
        return super(DeclarationDetailView, self).dispatch(*args, **kwargs)


class DeclarationPublishReceiptView(UpdateView):
    model = ManifestationDeclaration
    form_class = DeclarationPublishReceiptForm
    template_name_suffix = '_publish_form'

    @method_decorator(instructeur_required)
    def dispatch(self, *args, **kwargs):
        return super(DeclarationPublishReceiptView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.publish()
        return super(DeclarationPublishReceiptView, self).form_valid(form)
