# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout, Fieldset
from django import forms

from ddcs_loire.utils import GenericForm
from .models import ManifestationDeclaration


class DeclarationForm(GenericForm):

    def __init__(self, *args, **kwargs):
        super(DeclarationForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            FormActions(
                Submit('save', "soumettre la déclaration".capitalize())
            )
        )

    class Meta:
        model = ManifestationDeclaration
        fields = ()


class DeclarationPublishReceiptForm(GenericForm):

    def __init__(self, *args, **kwargs):
        super(DeclarationPublishReceiptForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset(
                "sélection du récépissé de déclaration".capitalize(),
                'receipt',
            ),
            FormActions(
                Submit('save', "publier le récépissé de déclaration".capitalize())
            )
        )

    def clean_receipt(self):
        data = self.cleaned_data['receipt']
        if not data:
            raise forms.ValidationError("Vous devez sélectionner un récépissé de déclaration")
        return data

    class Meta:
        model = ManifestationDeclaration
        fields = ['receipt',]

