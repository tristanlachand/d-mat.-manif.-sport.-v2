# coding: utf-8
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django_fsm import FSMField, transition

from administration.models import Prefecture
from ddcs_loire.utils import add_log_entry
from ddcs_loire.utils import add_notification_entry
from events.models import Manifestation


class ManifestationDeclarationQuerySet(models.QuerySet):
    """ Queryset pour les déclarations """

    def for_instructor(self, instructeur):
        """ Renvoyer les déclarations pour l'instructeur passé (celles de son arrondissement) """
        return self.filter(manifestation__departure_city__arrondissement=instructeur.prefecture.arrondissement)

    def to_process(self):
        """ Renvoyer les déclarations dont l'événement n'est pas encore passé """
        return self.filter(manifestation__end_date__gt=timezone.now())

    def closest_first(self):
        """ Trier par date de départ de la manifestation """
        return self.order_by('manifestation__begin_date')


class ManifestationDeclaration(models.Model):
    """ Déclaration de manifestation sportive """
    state = FSMField(default='created', verbose_name="état de la déclaration")
    creation_date = models.DateField("date de création", auto_now_add=True)
    dispatch_date = models.DateField("date d'envoi", blank=True, null=True)
    manifestation = models.OneToOneField('events.manifestation', related_name='manifestationdeclaration',
                                         verbose_name='manifestation associée')
    receipt = models.FileField(upload_to='recepisses/%Y/%m/%d', blank=True, null=True, verbose_name="récépissé de déclaration", max_length=512)
    objects = ManifestationDeclarationQuerySet.as_manager()

    def get_absolute_url(self):
        return reverse('authorizations:dashboard')

    def get_concerned_prefecture(self):
        return self.manifestation.departure_city.arrondissement.prefecture

    def log_creation(self):
        add_log_entry(agents=[self.manifestation.structure.organisateur], action='déclaration envoyée', manifestation=self.manifestation)

    def log_publish(self, prefecture):
        add_log_entry(agents=prefecture.instructeurs.all(), action='récépissé de déclaration publié', manifestation=self.manifestation)

    def notify_creation(self):
        try:
            prefecture = self.get_concerned_prefecture()
            add_notification_entry(manifestation=self.manifestation, agents=prefecture.instructeurs.all(), subject='déclaration reçue', content_object=self.manifestation.structure, institutional=prefecture,)
        except Prefecture.DoesNotExist:
            pass

    def notify_publish(self, prefecture):
        add_notification_entry(agents=[self.manifestation.structure.organisateur], manifestation=self.manifestation, subject='récépissé de déclaration publié', content_object=prefecture,)

    @transition(field=state, source='created', target='published')
    def publish(self):
        try:
            prefecture = self.get_concerned_prefecture()
            self.notify_publish(prefecture)
            self.log_publish(prefecture)
        except Prefecture.DoesNotExist:
            pass

    class Meta:
        verbose_name = "déclaration de manifestation sportive"
        verbose_name_plural = "déclarations de manifestations sportives"


@receiver(post_save, sender=ManifestationDeclaration)
def log_declaration_creation(created, instance, **kwargs):
    if kwargs['raw']:
        return
    if created:
        instance.log_creation()
        instance.notify_creation()
