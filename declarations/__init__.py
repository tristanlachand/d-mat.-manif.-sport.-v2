# coding: utf-8
from django.apps.config import AppConfig


class DeclarationsConfig(AppConfig):
    name = 'declarations'
    verbose_name = 'Déclarations'


default_app_config = 'declarations.DeclarationsConfig'
