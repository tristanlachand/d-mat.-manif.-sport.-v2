from __future__ import unicode_literals

import factory

from events.factories import DeclarationNMFactory

from .models import ManifestationDeclaration


class EventDeclarationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ManifestationDeclaration

    manifestation = factory.SubFactory(DeclarationNMFactory)
