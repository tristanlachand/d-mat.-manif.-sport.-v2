from __future__ import unicode_literals

from django.contrib import admin

from .models import ManifestationDeclaration


class EventDeclarationInline(admin.StackedInline):
    model = ManifestationDeclaration
    extra = 0
    max_num = 1
    readonly_fields = ('creation_date',)
