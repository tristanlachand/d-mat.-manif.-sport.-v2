# coding: utf-8
from django.conf.urls import url

from .views import DeclarationCreateView
from .views import DeclarationDetailView
from .views import DeclarationPublishReceiptView


urlpatterns = [
    url(r'^(?P<pk>\d+)/publish/$', DeclarationPublishReceiptView.as_view(), name='declaration_publish'),
    url(r'^add/(?P<manifestation_pk>\d+)$', DeclarationCreateView.as_view(), name='declaration_add'),
    url(r'^(?P<pk>\d+)/$', DeclarationDetailView.as_view(), name='declaration_detail')
]
