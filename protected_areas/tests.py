# coding: utf-8
from __future__ import unicode_literals

from django.test import TestCase

from .factories import AdministrateurRNRFactory, RNRFactory

from .factories import OperateurSiteN2KFactory, SiteN2KFactory


class RNRMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the name of the RNR
        '''
        rnr = RNRFactory.build()
        self.assertEqual(rnr.__str__(), 'Gorges de la Loire')


class AdministrateurRNRMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the short name of the Administrator
        '''
        rnr_administrator = AdministrateurRNRFactory.build()
        self.assertEqual(rnr_administrator.__str__(), 'FRAPNA')


class SiteN2KMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the name of the natura 2000 site
        '''
        natura_2000_site = SiteN2KFactory.build()
        self.assertEqual(natura_2000_site.__str__(), 'Bois-Noirs')


class OperateurSiteN2KMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the short name of the operator
        '''
        natura_2000_site_operator = OperateurSiteN2KFactory.build()
        self.assertEqual(
            natura_2000_site_operator.__str__(),
            'Parc Naturel Regional du Livradois-Forez',
        )
