# coding: utf-8
from django.apps import AppConfig


class ProtectedAreasConfig(AppConfig):
    name = 'protected_areas'
    verbose_name = "Zones Protégées"
