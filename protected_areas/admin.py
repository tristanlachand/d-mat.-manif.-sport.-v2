# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from contacts.admin import ContactInline, AddressInline
from .models import RNR, AdministrateurRNR
from .models import SiteN2K, OperateurSiteN2K


class AdministrateurRNRAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'rnr']
    list_filter = ['rnr']
    search_fields = ['name', 'rnr__name']
    inlines = [AddressInline, ContactInline]


class OperateurSiteN2KAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'site']
    list_filter = ['site']
    search_fields = ['name', 'site__name']
    inlines = [AddressInline, ContactInline]


class RNRAdmin(ImportExportActionModelAdmin):
    """ Configuration RNR """
    list_display = ['code', 'name']
    search_fields = ['name']


class N2KAdmin(ImportExportActionModelAdmin):
    """ Configration Natura 2000 """
    list_display = ['pk', 'name', 'site_type', 'index']
    search_fields = ['name', 'index']


# enregistrer les configurations d'admin
admin.site.register(RNR, RNRAdmin)
admin.site.register(AdministrateurRNR, AdministrateurRNRAdmin)
admin.site.register(SiteN2K, N2KAdmin)
admin.site.register(OperateurSiteN2K, OperateurSiteN2KAdmin)
