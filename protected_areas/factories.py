# coding: utf-8
from __future__ import unicode_literals

import factory

from .models import AdministrateurRNR, RNR
from .models import SiteN2K, OperateurSiteN2K


class RNRFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = RNR

    name = 'Gorges de la Loire'


class AdministrateurRNRFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AdministrateurRNR

    short_name = 'FRAPNA'
    name = 'Federation Rhone-Alpes de Protection de la Nature de la Loire'
    email = 'john.doe@frapna.org'
    rnr = factory.SubFactory(RNRFactory)


class OperateurSiteN2KFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OperateurSiteN2K

    name = 'Parc Naturel Regional du Livradois-Forez'
    email = 'john.doe@parc-livradois-forez.org'


class SiteN2KFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SiteN2K

    name = 'Bois-Noirs'
    site_type = 's'
    index = 'FR8301045'
