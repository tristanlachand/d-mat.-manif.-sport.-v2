# coding: utf-8
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models


class RNR(models.Model):
    name = models.CharField("nom", max_length=255, unique=True)
    code = models.PositiveIntegerField("code", unique=True)

    class Meta:
        verbose_name = "réserve naturelle régionale"
        verbose_name_plural = "réserves naturelles régionales"
        default_related_name = "rnrs"
        app_label = "protected_areas"

    def __str__(self):
        return self.name


class AdministrateurRNR(models.Model):
    short_name = models.CharField("abréviation", max_length=10)
    name = models.CharField("nom", max_length=255)
    address = GenericRelation("contacts.adresse", verbose_name="adresse")
    email = models.EmailField("e-mail", max_length=200)
    conservator = GenericRelation("contacts.contact", verbose_name="conservateur")
    rnr = models.OneToOneField("protected_areas.rnr",
                               related_name="administrateurrnr",
                               verbose_name="RNR")

    class Meta:
        verbose_name = "Gestionnaire RNR"
        verbose_name_plural = "Gestionnaires RNR"
        default_related_name = "administrateursrnr"
        app_label = "protected_areas"

    def __str__(self):
        return self.short_name


class SiteN2K(models.Model):
    TYPE_CHOICES = (
        ('s', 'SIC'),
        ('z', 'ZPS'),
    )

    name = models.CharField("nom", max_length=255, unique=True)
    site_type = models.CharField("type", max_length=1,
                                 choices=TYPE_CHOICES)
    index = models.CharField("fiche", max_length=9, unique=True)

    class Meta:
        verbose_name = "site natura 2000"
        verbose_name_plural = "sites natura 2000"
        default_related_name = "sitesn2k"
        app_label = "protected_areas"

    def __str__(self):
        return self.name


class OperateurSiteN2K(models.Model):
    name = models.CharField("nom", max_length=255)
    address = GenericRelation("contacts.adresse", verbose_name="adresse")
    email = models.EmailField('e-mail', max_length=200)
    person_in_charge = GenericRelation("contacts.contact",
                                       verbose_name="chargé de mission")
    site = models.OneToOneField("protected_areas.siten2k",
                                related_name="operateursiten2k",
                                verbose_name="site natura 2000")

    class Meta:
        verbose_name = "opérateur de site natura 2000"
        verbose_name_plural = "opérateurs de site natura 2000"
        default_related_name = "operateurssitesn2k"
        app_label = "protected_areas"

    def __str__(self):
        return self.name
