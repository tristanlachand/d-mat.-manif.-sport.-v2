# coding: utf-8
from __future__ import unicode_literals

from django.views.generic import ListView

from protected_areas.models import RNR
from protected_areas.models import SiteN2K


class RNRList(ListView):
    model = RNR


class SiteN2KList(ListView):
    model = SiteN2K
