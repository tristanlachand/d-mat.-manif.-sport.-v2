# coding: utf-8
from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import SiteN2KList
from .views import RNRList

urlpatterns = [

    url(r'^RNR/$',
        RNRList.as_view(),
        name='rnr_list'),
    url(r'^SiteN2K/$',
        SiteN2KList.as_view(),
        name='siten2k_list'),

]
