'''
WSGI config for ddcs_loire project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
'''

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ddcs_loire.settings_production")

from django.core.wsgi import get_wsgi_application
from raven.contrib.django.raven_compat.middleware.wsgi import Sentry

application = Sentry(get_wsgi_application())
