from django.conf import settings


def custom_department(request):
    z = settings.DEPARTMENT_SETTINGS.copy()
    z.update(settings.VERSION)
    return z
