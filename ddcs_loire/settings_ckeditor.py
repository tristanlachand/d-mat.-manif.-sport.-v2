# CKEditor configuration options
# https://github.com/django-ckeditor/django-ckeditor

CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_JQUERY_URL = '//code.jquery.com/jquery-2.1.1.min.js'
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_RESTRICT_BY_USER = False
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar_Full': [
            ['Format'],
            ['PasteText'],
            ['Undo', 'Redo'],
            ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Table'],
            ['Anchor', 'Link', 'Unlink', 'Image'],
            ['HorizontalRule', 'SpecialChar'],
            ['Maximize', 'ShowBlocks'],
        ],
        'toolbar': 'Full',
        'width': '100%',
        'skin': 'bootstrapck',
    },
}
