from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

from django.contrib import admin
from django.contrib.flatpages import views as flatpages_views
from django.contrib.sitemaps import views as sitemaps_views
from django.contrib.flatpages.sitemaps import FlatPageSitemap
from events.views import ProfileDetailView
from events.views import UserUpdateView
from events.views import StructureUpdateView

admin.autodiscover()

sitemaps = {
    'flatpages': FlatPageSitemap,
}

urlpatterns = [
    # Examples:
    # url(r'^$', 'ddcs_loire.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^sitemap\.xml$', sitemaps_views.sitemap, {'sitemaps': sitemaps}),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/profile/$', ProfileDetailView.as_view(), name='profile'),
    url(r'^accounts/profile/edit$', UserUpdateView.as_view(), name='profile_edit'),
    url(r'^accounts/profile/structure/edit', StructureUpdateView.as_view(), name='structure_edit'),
    url(r'^agreements/', include('agreements.urls', namespace='agreements')),
    url(r'^sub_agreements/', include('sub_agreements.urls', namespace='sub_agreements')),
    url(r'^authorizations/', include('authorizations.urls', namespace='authorizations')),
    url(r'^declarations/', include('declarations.urls', namespace='declarations')),
    url(r'^evaluations/', include('evaluations.urls', namespace='evaluations')),
    url(r'^nouveautes/', include('nouveautes.urls', namespace='nouveautes')),
    url(r'^', include('events.urls', namespace='events')),
    url(r'^protected_areas/', include('protected_areas.urls', namespace='protected_areas')),
    url(r'^(?P<url>.*/)$', flatpages_views.flatpage),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
