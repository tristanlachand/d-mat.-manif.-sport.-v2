'''
Django settings for ddcs_loire project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
'''

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

from ddcs_loire.settings_ckeditor import *
from ddcs_loire.settings_delays import *
from ddcs_loire.settings_external_links import *

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'g&*0o49mk(t&*vvo!o!rx5vpu%d9v5n*e7ars&$qz*lpi^s$k*'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

ROOT_URLCONF = 'ddcs_loire.urls'

WSGI_APPLICATION = 'ddcs_loire.wsgi.application'


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django_extensions',
    'django_jenkins',
    'localflavor',
    'allauth',
    'allauth.account',
    'bootstrap3_datetime',
    'crispy_forms',
    'ckeditor',
    'administration',
    'administrative_division',
    'agreements',
    'authorizations',
    'contacts',
    'declarations',
    'emergencies',
    'evaluations',
    'events',
    'notifications',
    'protected_areas',
    'sports',
    'sub_agreements',
)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)


# Context Processors
# https://docs.djangoproject.com/en/1.6/ref/settings/#template-context-processors

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'allauth.account.context_processors.account',
)


# Authentication : how user can log in
# https://docs.djangoproject.com/en/1.6/ref/settings/#authentication-backends

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)
LOGIN_REDIRECT_URL = '/events'

# Password hashers : we add here password hasher for drupal
# https://docs.djangoproject.com/en/1.6/ref/settings/#password-hashers

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'events.hashers.DrupalPasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.UnsaltedMD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Sites
# https://docs.djangoproject.com/en/1.6/ref/contrib/sites/
SITE_ID = 1


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'fr-FR'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'


# Templates
# https://docs.djangoproject.com/en/1.6/ref/settings/#template-dirs

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

# List of applications tested when './manage.py jenkins' command
# https://github.com/kmmbvnr/django-jenkins/

PROJECT_APPS = (
    'administration',
    'administrative_division',
    'agreements',
    'authorizations',
    'contacts',
    'declarations',
    'emergencies',
    'evaluations',
    'events',
    'notifications',
    'protected_areas',
    'sports',
    'sub_agreements',
)

# List of Jenkins tasks executed by './manage.py jenkins' command
# https://github.com/kmmbvnr/django-jenkins/

JENKINS_TASKS = (
    'django_jenkins.tasks.run_sloccount',
    'django_jenkins.tasks.run_pylint',
    'django_jenkins.tasks.run_pep8',
)

PYLINT_RCFILE = 'ddcs_loire/pylint.rc'

# Email backend
# https://docs.djangoproject.com/en/1.6/topics/email/#email-backends

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# Account Settings (Managed by django-allauth)
# https://github.com/pennersr/django-allauth

ACCOUNT_SIGNUP_FORM_CLASS = 'events.forms.SignupForm'
ACCOUNT_LOGOUT_REDIRECT_URL = '/events'


# Sets default template pack for django-crispy-forms
# http://django-crispy-forms.readthedocs.org/en/latest/install.html#template-packs

CRISPY_TEMPLATE_PACK = 'bootstrap3'


# Openrunner settings

OPENRUNNER_HOST = 'http://ddcs.openrunner.com/'
OPENRUNNER_SECRET_KEY = 'A0E0R0R0R0R0R034'

OPENRUNNER_CREATE_ACCOUNT = 'account/cfromddcs.php?'
OPENRUNNER_GET_ROUTES = 'route-data/getRouteListByDDCSUser.php?us='
OPENRUNNER_ROUTE_DISPLAY = 'orservice/serviceDDCS.php?idr='
