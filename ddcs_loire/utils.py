# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms
from django.conf import settings
from django.core.mail import send_mail

from crispy_forms.helper import FormHelper


def add_notification_entry(agents, subject, content_object, manifestation,
                           institutional=None):
    mail_subject = '{sender} : {subject} {liaison} \"{manifestation}\"'.format(
        sender=content_object,
        subject=subject,
        liaison="Pour manifestation",
        manifestation=manifestation.name,
    )
    # Todo : à tester
    mail_footer = "".join([(
        "\n\n=== NE PAS RÉPONDRE À CET E-MAIL ===\n"
        "Pour effectuer la suite de votre démarche : \n"
        "- connectez vous sur la plateforme Manifestation Sportive\n"
        "- rendez vous sur votre tableau de bord\n"), settings.MAIN_URL])
    mail_message = "".join([mail_subject, mail_footer])
    if institutional and institutional.email:
        send_mail(mail_subject,
                  mail_message,
                  settings.DEFAULT_FROM_EMAIL,
                  [institutional.email])
    for agent in agents:
        agent.user.notifications.create(
            manifestation=manifestation,
            subject=subject,
            content_object=content_object,
        )
        if agent.user.email:
            send_mail(mail_subject,
                      mail_message,
                      settings.DEFAULT_FROM_EMAIL,
                      [agent.user.email])


def add_log_entry(agents, action, manifestation):
    for agent in agents:
        agent.user.actions.create(
            manifestation=manifestation,
            action=action,
        )


class GenericForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(GenericForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'


def get_edsr_or_ggd_config():
    """
    Renvoyer settings.DEPARTMENT_SETTINGS['EDSR_OR_GGD']

    en minuscules.
    """
    return settings.DEPARTMENT_SETTINGS['EDSR_OR_GGD'].lower()
