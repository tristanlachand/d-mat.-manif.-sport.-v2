'''
Django settings for ddcs_loire project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
'''

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

from ddcs_loire.settings_ckeditor import *
from ddcs_loire.settings_delays import *
from ddcs_loire.settings_external_links import *
from ddcs_loire.settings_openrunner import *

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'g&*0o49mk(t&*vvo!o!rx5vpu%d9v5n*e7ars&$qz*lpi^s$k*'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

ROOT_URLCONF = 'ddcs_loire.urls'

WSGI_APPLICATION = 'ddcs_loire.wsgi.application'


# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django_extensions',
    'django_jenkins',
    'localflavor',
    'allauth',
    'allauth.account',
    'bootstrap3_datetime',
    'ckeditor',
    'ckeditor_uploader',
    'crispy_forms',
    'import_export',
    'administration',
    'administrative_division',
    'agreements',
    'authorizations',
    'contacts',
    'declarations',
    'emergencies',
    'evaluations',
    'events',
    'notifications',
    'nouveautes',
    'protected_areas',
    'sports',
    'sub_agreements',
    'core',
)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)


# Context Processors
# https://docs.djangoproject.com/en/1.6/ref/settings/#template-context-processors
# Django <= 1.7
# TEMPLATE_CONTEXT_PROCESSORS = (
#     'django.contrib.auth.context_processors.auth',
#     'django.core.context_processors.debug',
#     'django.core.context_processors.i18n',
#     'django.core.context_processors.media',
#     'django.core.context_processors.request',
#     'django.core.context_processors.static',
#     'django.core.context_processors.tz',
#     'django.contrib.messages.context_processors.messages',
#     'allauth.account.context_processors.account',
#     'ddcs_loire.context_processors.custom_department',
# )

# Django >= 1.8
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Already defined Django-related contexts here

                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                # 'allauth.account.context_processors.account',
                'ddcs_loire.context_processors.custom_department',
            ],
        },
    },
]


# Authentication : how user can log in
# https://docs.djangoproject.com/en/1.6/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)


# Password hashers : we add here password hasher for drupal
# https://docs.djangoproject.com/en/1.6/ref/settings/#password-hashers
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
#    'events.hashers.DrupalPasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.UnsaltedMD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Sites
# https://docs.djangoproject.com/en/1.6/ref/contrib/sites/
SITE_ID = 1


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
LANGUAGE_CODE = 'fr-FR'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)
STATIC_ROOT = '/home/user'


# Media files (User uploaded content)
# https://docs.djangoproject.com/en/1.6/topics/files/
MEDIA_ROOT = '/var/www/ddcs/media/'
MEDIA_URL = '/media/'

FILE_UPLOAD_MAX_MEMORY_SIZE = 10485760


# Templates
# https://docs.djangoproject.com/en/1.6/ref/settings/#template-dirs
TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]


# List of applications tested when './manage.py jenkins' command
# https://github.com/kmmbvnr/django-jenkins/
PROJECT_APPS = (
    'administration',
    'administrative_division',
    'agreements',
    'authorizations',
    'contacts',
    'declarations',
    'emergencies',
    'evaluations',
    'events',
    'notifications',
    'protected_areas',
    'sports',
    'sub_agreements',
)

# List of Jenkins tasks executed by './manage.py jenkins' command
# https://github.com/kmmbvnr/django-jenkins/
JENKINS_TASKS = (
    'django_jenkins.tasks.run_pylint',
    'django_jenkins.tasks.run_pep8',
)

PEP8_RCFILE = 'ddcs_loire/pep8.rc'

PYLINT_LOAD_PLUGIN = (
    'pylint_django',
)

# Email backend
# https://docs.djangoproject.com/en/1.6/topics/email/#email-backends
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
DEFAULT_FROM_EMAIL = 'Manifestationsportive.fr <plateforme@manifestationsportive.fr>'


# Account Settings (Managed by django-allauth)
# https://github.com/pennersr/django-allauth
ACCOUNT_SIGNUP_FORM_CLASS = 'events.forms.SignupForm'
ACCOUNT_ADAPTER = 'events.adapter.MyAccountAdapter'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True


# Sets default template pack for django-crispy-forms
# http://django-crispy-forms.readthedocs.org/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = 'bootstrap3'


# Custom settings
# http://www.b-list.org/weblog/2006/jun/14/django-tips-template-context-processors/
DEPARTMENT_SETTINGS = {
    'DEPARTMENT_NAME': 'Prod',
    'DEPARTMENT_NUMBER': '42',
    'DEPARTMENT_BKG_COLOR': '#ffeeee',
    'EDSR_OR_GGD': 'EDSR',  # (GGD_AgentEDSR|GGD_SubAgentEDSR|EDSR)
    'OPENRUNNER_MAP_OPTIONAL': True
}

DELAY_SETTINGS = {
    'MANIFESTATION_DELAY': 30,
    'ANM_DELAY_NO_DEPARTMENT_CROSSED': 60,
    'ANM_DELAY_DEPARTMENTS_CROSSED': 90,
    'DVTM_DELAY': 60,
    'AVTMC_DELAY': 90,
    'AVTM_SVP_DELAY': 90,
    'AVTM_HVP_DELAY': 90
}

# Todo : remplacer DEPARTMENT_SETTINGS par CUSTOM_UI et creer CUSTOM_WORKFLOW

MAIN_URL = 'https://www.manifestationsportive.fr'

VERSION = {
    'VERSION': '2.10',
    'DATE': '10/05/2016',
    'COMMIT': 'f517c500af35de771b5d3c9df7746e1c717dcfee'
}