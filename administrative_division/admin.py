from __future__ import unicode_literals

from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from .forms import CommuneForm, DepartementForm
from .models import Arrondissement, Commune, Departement


class ArrondissementAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'code', 'name', 'get_departement', 'prefecture']
    list_filter = ['departement__name', 'prefecture']
    search_fields = ['name', 'departement__name']


class CommuneAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'zip_code', 'arrondissement', 'get_departement', 'get_prefecture']
    list_filter = ['arrondissement__departement']
    search_fields = ['name', 'zip_code', 'ascii_name']
    form = CommuneForm


class DepartementAdmin(ImportExportActionModelAdmin):
    """ Configuration admin des départements """
    list_display = ['pk', 'name']
    search_fields = ['name']
    form = DepartementForm


# Enregistrer les classes dans l'Admin
#admin.site.register(Departement, DepartementAdmin)
#admin.site.register(Arrondissement, ArrondissementAdmin)
#admin.site.register(Commune, CommuneAdmin)
