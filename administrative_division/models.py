# coding: utf-8
from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from localflavor.fr.fr_department import DEPARTMENT_CHOICES

from administrative_division.mixins.divisions import ManyPerDepartementMixin, ManyPerArrondissementMixin

CHOICES = {'arrondissement__departement__name': settings.DEPARTMENT_SETTINGS['DEPARTMENT_NUMBER']}

class Departement(models.Model):
    """ Division de niveau 2 (département) : total de 96+ en France métropolitaine """
    name = models.CharField("Département", unique=True, max_length=3, choices=DEPARTMENT_CHOICES)

    def __str__(self):
        return self.get_name_display()

    class Meta:
        verbose_name = "Département"
        verbose_name_plural = "Départements"
        ordering = ['name']
        app_label = 'administrative_division'
        default_related_name = 'departements'


class Arrondissement(ManyPerDepartementMixin):
    """ Division de niveau 3 (arrondissement, 330+ en France Métropolitaine """
    code = models.CharField("Code", unique=True, max_length=4)
    name = models.CharField("Nom", max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Arrondissement"
        verbose_name_plural = "Arrondissements"
        ordering = ['name']
        app_label = 'administrative_division'
        default_related_name = 'arrondissements'


class Commune(ManyPerArrondissementMixin):
    """ Division administrative de niveau 4 (commune) """
    code = models.CharField("Code", unique=True, max_length=5)
    name = models.CharField("Nom", max_length=255)
    ascii_name = models.CharField("Nom ASCII", max_length=255)
    zip_code = models.CharField("Code postal", max_length=5)
    # Position géographie (en coordonnées WGS84)
    latitude = models.DecimalField("Latitude", max_digits=8, decimal_places=5, null=True, blank=True)
    longitude = models.DecimalField("Longitude", max_digits=8, decimal_places=5, null=True, blank=True)

    def __str__(self):
        return self.name

    def get_full_name(self):
        """ Renvoyer un nom complet formaté pour la commune """
        return "{code} {name} ({dept})".format(code=self.code, name=self.name, dept=self.get_departement().name)

    def get_mairieagents(self):
        """ Renvoyer les agents de mairie pour la commune """
        return self.mairieagents.all()

    def get_prefecture(self):
        """ Renvoyer la préfecture de la commune """
        return self.arrondissement.prefecture

    class Meta:
        verbose_name = 'commune'
        app_label = 'administrative_division'
        ordering = ['ascii_name']
        default_related_name = 'communes'
