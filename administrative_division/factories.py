# coding: utf-8
import factory
from factory.fuzzy import FuzzyDecimal

from .models import *


class DepartementFactory(factory.django.DjangoModelFactory):
    """ Factory pour les départements """
    name = factory.Sequence(lambda n: "{0}".format(n))

    class Meta:
        model = Departement


class ArrondissementFactory(factory.django.DjangoModelFactory):
    """ Factory pour les arrondissements """
    departement = factory.SubFactory(DepartementFactory)  # automatiquement créer un département avec l'arrondissement
    code = factory.Sequence(lambda n: '{0}'.format(n))
    name = factory.Sequence(lambda n: 'Arrondissement{0}'.format(n))

    class Meta:
        model = Arrondissement


class CommuneFactory(factory.django.DjangoModelFactory):
    """ Factory pour les communes """
    arrondissement = factory.SubFactory(ArrondissementFactory)  # créer un arrondissement avec la commune
    name = factory.Sequence(lambda n: 'Commune{0}'.format(n))
    ascii_name = factory.Sequence(lambda n: 'Commune{0}'.format(n))
    code = factory.Sequence(lambda n: '{0}'.format(n))
    zip_code = '42300'
    latitude = FuzzyDecimal(low=-90.0, high=90.0)
    longitude = FuzzyDecimal(low=-180.0, high=180.0)

    class Meta:
        model = Commune
