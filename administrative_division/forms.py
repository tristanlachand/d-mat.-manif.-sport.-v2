# coding: utf-8
from __future__ import unicode_literals

from django import forms
from localflavor.fr.forms import FRZipCodeField, FRDepartmentField

from administrative_division.models import Commune, Departement


class CommuneForm(forms.ModelForm):
    """ Formulaire des communes """
    zip_code = FRZipCodeField()

    class Meta:
        model = Commune
        fields = '__all__'


class DepartementForm(forms.ModelForm):
    """ Formulaire des départements """
    name = FRDepartmentField()

    class Meta:
        model = Departement
        fields = '__all__'
