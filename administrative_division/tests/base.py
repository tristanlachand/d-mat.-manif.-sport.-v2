from __future__ import unicode_literals

from django.test import TestCase

from administrative_division.factories import ArrondissementFactory, CommuneFactory
from administrative_division.factories import DepartementFactory


class DepartementTests(TestCase):
    """ Tests départements """

    def test_departement_string(self):
        """ Teste que __str__ renvoie bien le nom du département """
        departement = DepartementFactory.build(name='42')
        self.assertEqual(str(departement), departement.get_name_display())


class ArrondissementMethodTests(TestCase):
    """ Tests arrondissements """

    def test_arrondissement_string(self):
        """ Teste que __str__ renvoie bien le nom de l'arrondissement """
        arrondissement = ArrondissementFactory.build(name="Roanne")
        self.assertEqual(str(arrondissement), "Roanne")


class CommuneMethodTests(TestCase):
    """ Tests communes """

    def test_commune_string(self):
        """ Tester que __str__ renvoie bien le nom de la commune """
        commune = CommuneFactory.build(name="Roanne")
        self.assertEqual(str(commune), "Roanne")
