# coding: utf-8
""" Classes abstraites ajoutant des champs et fonctionnalités aux modèles """
from django.db import models

from core.util.admin import set_admin_info
from core.util.application import FILTER_ARRONDISSEMENT_CHOICES


class OnePerDepartementMixin(models.Model):
    """ Ajoute un champ OneToOne vers un département à un modèle """
    departement = models.OneToOneField('administrative_division.departement', verbose_name="département")

    @set_admin_info(admin_order_field='departement', short_description="Département")
    def get_departement(self):
        """ Renvoie le département de l'objet """
        return self.departement

    class Meta:
        abstract = True


class OnePerArrondissementMixin(models.Model):
    """ Ajoute un champ OneToOne vers un arrondissement à un modèle """
    arrondissement = models.OneToOneField('administrative_division.arrondissement',
                                          # related_name="prefecture",
                                          verbose_name="Arrondissement")

    @set_admin_info(admin_order_field='arrondissement', short_description="Arrondissement")
    def get_arrondissement(self):
        """ Renvoie la commune de l'objet """
        return self.arrondissement

    @set_admin_info(admin_order_field='arrondissement__departement', short_description="Département")
    def get_departement(self):
        """ Renvoie le département de l'arrondissement de l'objet """
        return self.arrondissement.departement

    class Meta:
        abstract = True


class ManyPerArrondissementMixin(models.Model):
    """ Ajoute un champ OneToOne vers un arrondissement à un modèle """
    arrondissement = models.ForeignKey('administrative_division.arrondissement', verbose_name="Arrondissement")

    @set_admin_info(admin_order_field='arrondissement', short_description="Arrondissement")
    def get_arrondissement(self):
        """ Renvoie la commune de l'objet """
        return self.arrondissement

    @set_admin_info(admin_order_field='arrondissement__departement', short_description="Département")
    def get_departement(self):
        """ Renvoie le département de l'arrondissement de l'objet """
        return self.arrondissement.departement

    class Meta:
        abstract = True


class ManyPerDepartementMixin(models.Model):
    """ Ajoute un champ ForeignKey vers un département à un modèle """
    departement = models.ForeignKey('administrative_division.departement', verbose_name="département")

    @set_admin_info(admin_order_field='departement', short_description="Département")
    def get_departement(self):
        """ Renvoie le département de l'objet """
        return self.departement

    class Meta:
        abstract = True


class ManyPerCommuneMixin(models.Model):
    """ Ajoute un champ ForeignKey vers une commune à un modèle """
    commune = models.ForeignKey('administrative_division.commune', limit_choices_to=FILTER_ARRONDISSEMENT_CHOICES,
                                verbose_name='Commune')

    @set_admin_info(admin_order_field='commune', short_description="Commune")
    def get_commune(self):
        """ Renvoie la commune de l'objet """
        return self.commune

    @set_admin_info(admin_order_field='commune__arrondissement', short_description="Arrondissement")
    def get_arrondissement(self):
        """ Renvoie la commune de l'objet """
        return self.commune.arrondissement

    @set_admin_info(admin_order_field='commune__arrondissement__departement', short_description="Département")
    def get_departement(self):
        """ Renvoie le département de l'arrondissement de l'objet """
        return self.commune.arrondissement.departement

    class Meta:
        abstract = True
