# coding: utf-8
from __future__ import unicode_literals

from django.core.exceptions import ObjectDoesNotExist
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView
from django.views.generic.edit import UpdateView

from agreements.views import BaseAcknowledgeView
from sub_agreements.decorators import edsr_agent_required
from sub_agreements.models import PreAvisEDSR
from .decorators import cgd_agent_required
from .decorators import cgservice_agent_required
from .decorators import commissariat_agent_required
from .decorators import company_agent_required
from .decorators import sub_agent_required
from .forms import NotifyBrigadesForm
from .forms import NotifyCISForm
from .forms import PreAvisForm
from .models import PreAvisCGD
from .models import PreAvisServiceCG
from .models import PreAvisCommissariat
from .models import PreAvisCompagnie
from .models import PreAvis


class Dashboard(ListView):
    model = PreAvis

    @method_decorator(sub_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        agentlocal = self.request.user.agentlocal
        # agent = self.request.user.agent
        # Liste de ce que doit voir un agent dans son dashboard
        results = {
            'compagnieagentlocal': PreAvisCompagnie.objects.to_process().filter(
                company=agentlocal.compagnieagentlocal.compagnie
            ) if hasattr(agentlocal, 'compagnieagentlocal') else None
            ,
            'cgdagentlocal': PreAvisCGD.objects.to_process().filter(
                gendarmerie_company=agentlocal.cgdagentlocal.cgd
            ) if hasattr(agentlocal, 'cgdagentlocal') else None
            ,
            'commissariatagentlocal': PreAvisCommissariat.objects.to_process().filter(
                commissariat=agentlocal.commissariatagentlocal.commissariat
            ) if hasattr(agentlocal, 'commissariatagentlocal') else None
            ,
            'cgserviceagentlocal': PreAvisServiceCG.objects.to_process().filter(
                cg_service=agentlocal.cgserviceagentlocal.cg_service
            ) if hasattr(agentlocal, 'cgserviceagentlocal') else None
            ,
            'edsragentlocal': PreAvisEDSR.objects.to_process().filter(
                edsr=agentlocal.edsragentlocal.edsr) if hasattr(
                agentlocal, 'edsragentlocal') else None
            ,
        }
        # Parcourir les clés disponibles, en tant qu'attributs de agent
        # Si un attribut ne provoque pas d'erreur, renvoyer le queryset approprié
        # Rappel: Les querysets ne sont pas évalués même lorsqu'ils sont retournés,
        # ils ne sont évalués que lorsqu'ils sont parcourus
        for agentname in results:
            try:
                getattr(agentlocal, agentname)
                return results[agentname]
            except (ObjectDoesNotExist, AttributeError):
                pass
        raise PreAvis.DoesNotExist("L'utilisateur courant doit être un agent local.")

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        return context


###########################################################
# CGDSub Agreements Views
###########################################################
class PreAvisCGDDetail(DetailView):
    model = PreAvisCGD

    @method_decorator(cgd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(PreAvisCGDDetail, self).dispatch(*args, **kwargs)


class PreAvisCGDAcknowledge(BaseAcknowledgeView):
    model = PreAvisCGD
    form_class = PreAvisForm

    @method_decorator(cgd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(PreAvisCGDAcknowledge,
                     self).dispatch(*args, **kwargs)


class NotifyBrigadesView(UpdateView):
    model = PreAvisCGD
    form_class = NotifyBrigadesForm
    template_name_suffix = '_notify_form'

    @method_decorator(cgd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(NotifyBrigadesView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        response = super(NotifyBrigadesView, self).form_valid(form)
        form.instance.notify_brigades()
        form.instance.save()
        return response


###########################################################
# EDSR Sub Agreements Views
###########################################################
class PreAvisEDSRDetail(DetailView):
    model = PreAvisEDSR

    @method_decorator(edsr_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(PreAvisEDSRDetail, self).dispatch(*args, **kwargs)


class PreAvisEDSRAcknowledge(BaseAcknowledgeView):
    model = PreAvisEDSR
    form_class = PreAvisForm

    @method_decorator(edsr_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(PreAvisEDSRAcknowledge, self).dispatch(*args, **kwargs)


###########################################################
# CompanySub Agreements Views
###########################################################
class PreAvisCompagnieDetail(DetailView):
    model = PreAvisCompagnie

    @method_decorator(company_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(PreAvisCompagnieDetail, self).dispatch(*args, **kwargs)


class PreAvisCompagnieAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = PreAvisCompagnie
    form_class = PreAvisForm

    @method_decorator(company_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(PreAvisCompagnieAcknowledge,
                     self).dispatch(*args, **kwargs)


class NotifyCISView(UpdateView):
    model = PreAvisCompagnie
    form_class = NotifyCISForm
    template_name_suffix = '_notify_form'

    @method_decorator(company_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(NotifyCISView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        response = super(NotifyCISView, self).form_valid(form)
        form.instance.notify_cis()
        form.instance.save()
        return response


###########################################################
# CommissariatSub Agreements Views
###########################################################
class PreAvisCommissariatDetail(DetailView):
    model = PreAvisCommissariat

    @method_decorator(commissariat_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(PreAvisCommissariatDetail,
                     self).dispatch(*args, **kwargs)


class PreAvisCommissariatAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = PreAvisCommissariat
    form_class = PreAvisForm

    @method_decorator(commissariat_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(PreAvisCommissariatAcknowledge,
                     self).dispatch(*args, **kwargs)


###########################################################
# CGServiceSub Agreements Views
###########################################################
class PreAvisServiceCGDetail(DetailView):
    model = PreAvisServiceCG

    @method_decorator(cgservice_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(PreAvisServiceCGDetail,
                     self).dispatch(*args, **kwargs)


class PreAvisServiceCGAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = PreAvisServiceCG
    form_class = PreAvisForm

    @method_decorator(cgservice_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(PreAvisServiceCGAcknowledge,
                     self).dispatch(*args, **kwargs)
