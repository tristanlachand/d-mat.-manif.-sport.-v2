# coding: utf-8
from django.apps import AppConfig


class SubAgreementsConfig(AppConfig):
    name = 'sub_agreements'
    verbose_name = "Pré-Avis"
