# coding: utf-8
from __future__ import unicode_literals

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import render

from allauth.account.decorators import verified_email_required

from administration.models import AgentLocal, EDSRAgent
from administration.models import CGDAgentLocal
from administration.models import CommissariatAgentLocal
from administration.models import CompagnieAgentLocal
from administration.models import CGServiceAgentLocal


def sub_agent_required(function=None,
                       login_url=None,
                       redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @verified_email_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agentlocal
            except AgentLocal.DoesNotExist:
                try:
                    request.user.agent.edsragent
                except EDSRAgent.DoesNotExist:
                    return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def company_agent_required(function=None,
                           login_url=None,
                           redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @sub_agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agentlocal.compagnieagentlocal
            except CompagnieAgentLocal.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def cgd_agent_required(function=None,
                       login_url=None,
                       redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @sub_agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agentlocal.cgdagentlocal
            except CGDAgentLocal.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def edsr_agent_required(function=None,
                       login_url=None,
                       redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @sub_agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agentlocal.edsragentlocal
            except EDSRAgent.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def commissariat_agent_required(function=None,
                                login_url=None,
                                redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @sub_agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agentlocal.commissariatagentlocal
            except CommissariatAgentLocal.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def cgservice_agent_required(function=None,
                             login_url=None,
                             redirect_field_name=REDIRECT_FIELD_NAME):
    def decorator(view_func):
        @sub_agent_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.agentlocal.cgserviceagentlocal
            except CGServiceAgentLocal.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
