# coding: utf-8
from __future__ import unicode_literals

import factory

from administration.factories import CommissariatFactory, EDSRFactory
from administration.factories import CGDFactory
from administration.factories import CompagnieFactory
from administration.factories import CGServiceFactory
from agreements.factories import EDSRAgreementFactory
from agreements.factories import DDSPAgreementFactory
from agreements.factories import SDISAgreementFactory
from agreements.factories import CGAgreementFactory
from sub_agreements.models import PreAvisEDSR

from .models import PreAvisCGD
from .models import PreAvisCommissariat
from .models import PreAvisCompagnie
from .models import PreAvisServiceCG


class PreAvisCGDFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PreAvisCGD

    avis = factory.SubFactory(EDSRAgreementFactory)
    gendarmerie_company = factory.SubFactory(CGDFactory)


class PreAvisEDSRFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PreAvisEDSR

    avis = factory.SubFactory(EDSRAgreementFactory)
    edsr = factory.SubFactory(EDSRFactory)


class PreAvisCommissariatFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PreAvisCommissariat

    avis = factory.SubFactory(DDSPAgreementFactory)
    commissariat = factory.SubFactory(CommissariatFactory)


class PreAvisCompagnieFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PreAvisCompagnie

    avis = factory.SubFactory(SDISAgreementFactory)
    company = factory.SubFactory(CompagnieFactory)


class PreAvisServiceCGFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PreAvisServiceCG

    avis = factory.SubFactory(CGAgreementFactory)
    cg_service = factory.SubFactory(CGServiceFactory)
