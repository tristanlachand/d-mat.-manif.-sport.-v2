# coding: utf-8
from __future__ import unicode_literals

from ddcs_loire.utils import GenericForm

from crispy_forms.layout import Submit, Layout
from crispy_forms.bootstrap import FormActions

from .models import PreAvis
from .models import PreAvisCompagnie
from .models import PreAvisCGD


class PreAvisForm(GenericForm):

    class Meta:
        model = PreAvis
        fields = ('favorable', 'prescriptions')

    def __init__(self, *args, **kwargs):
        super(PreAvisForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'favorable',
            'prescriptions',
            FormActions(
                Submit('save', "Rendre le pré-avis")
            )
        )


class NotifyCISForm(GenericForm):

    class Meta:
        model = PreAvisCompagnie
        fields = ('concerned_cis',)

    def __init__(self, *args, **kwargs):
        super(NotifyCISForm, self).__init__(*args, **kwargs)
        self.fields['concerned_cis'].queryset = self.instance.company.ciss.all()  # noqa pylint: disable=E1101
        self.helper.layout = Layout(
            'concerned_cis',
            FormActions(
                Submit('save', "Informer les CIS concernés")
            )
        )


class NotifyBrigadesForm(GenericForm):

    class Meta:
        model = PreAvisCGD
        fields = ('concerned_brigades',)

    def __init__(self, *args, **kwargs):
        super(NotifyBrigadesForm, self).__init__(*args, **kwargs)
        self.fields['concerned_brigades'].queryset = self.instance.gendarmerie_company.brigades.all()  # noqa pylint: disable=E1101
        self.helper.layout = Layout(
            'concerned_brigades',
            FormActions(
                Submit('save', "Informer les brigades concernées")
            )
        )
