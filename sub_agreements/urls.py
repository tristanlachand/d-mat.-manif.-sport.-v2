# coding: utf-8
from __future__ import unicode_literals

from django.conf.urls import patterns, url

from sub_agreements.views import PreAvisEDSRDetail, PreAvisEDSRAcknowledge
from .views import Dashboard

from .views import PreAvisCGDAcknowledge
from .views import PreAvisCGDDetail
from .views import NotifyBrigadesView

from .views import PreAvisCompagnieAcknowledge
from .views import PreAvisCompagnieDetail
from .views import NotifyCISView

from .views import PreAvisCommissariatAcknowledge
from .views import PreAvisCommissariatDetail

from .views import PreAvisServiceCGAcknowledge
from .views import PreAvisServiceCGDetail

urlpatterns = [

    url(r'^dashboard/$', Dashboard.as_view(),
        name='dashboard'),
    # CGD SubAgreements
    url(r'^cgd/(?P<pk>\d+)/$',
        PreAvisCGDDetail.as_view(),
        name='cgd_subagreement_detail'),
    url(r'^cgd/(?P<pk>\d+)/acknowledge/$',
        PreAvisCGDAcknowledge.as_view(),
        name='cgd_subagreement_acknowledge'),
    url(r'^cgd/(?P<pk>\d+)/notify_brigades/$',
        NotifyBrigadesView.as_view(),
        name='notify_brigades'),
    # EDSR SubAgreements
    url(r'^edsr/(?P<pk>\d+)/$',
        PreAvisEDSRDetail.as_view(),
        name='edsr_subagreement_detail'),
    url(r'^edsr/(?P<pk>\d+)/acknowledge/$',
        PreAvisEDSRAcknowledge.as_view(),
        name='edsr_subagreement_acknowledge'),
    # Company SubAgreements
    url(r'^company/(?P<pk>\d+)/$',
        PreAvisCompagnieDetail.as_view(),
        name='company_subagreement_detail'),
    url(r'^company/(?P<pk>\d+)/acknowledge/$',
        PreAvisCompagnieAcknowledge.as_view(),
        name='company_subagreement_acknowledge'),
    url(r'^company/(?P<pk>\d+)/notify_cis/$',
        NotifyCISView.as_view(),
        name='notify_cis'),
    # Commissariat SubAgreements
    url(r'^commissariat/(?P<pk>\d+)/$',
        PreAvisCommissariatDetail.as_view(),
        name='commissariat_subagreement_detail'),
    url(r'^commissariat/(?P<pk>\d+)/acknowledge/$',
        PreAvisCommissariatAcknowledge.as_view(),
        name='commissariat_subagreement_acknowledge'),
    # CG Service SubAgreements
    url(r'^cgservice/(?P<pk>\d+)/$',
        PreAvisServiceCGDetail.as_view(),
        name='cgservice_subagreement_detail'),
    url(r'^cgservice/(?P<pk>\d+)/acknowledge/$',
        PreAvisServiceCGAcknowledge.as_view(),
        name='cgservice_subagreement_acknowledge'),
]
