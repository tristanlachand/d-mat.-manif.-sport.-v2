# coding: utf-8
from __future__ import unicode_literals

import datetime

from django.core.urlresolvers import reverse
from django.test import TestCase
from django.conf import settings

from administration.factories import CGFactory, GGDFactory
from administration.factories import EDSRFactory
from administration.factories import SDISFactory
from administration.factories import DDSPFactory

from administration.factories import CGServiceFactory
from administration.factories import CommissariatFactory
from administration.factories import CompagnieFactory
from administration.factories import CGDFactory
from administration.models import GGD

from administrative_division.factories import ArrondissementFactory
from administrative_division.factories import CommuneFactory
from administrative_division.factories import DepartementFactory

from authorizations.factories import EventAuthorizationFactory

from events.factories import AutorisationNMFactory

from agreements.factories import CGAgreementFactory, GGDAgreementFactory
from agreements.factories import DDSPAgreementFactory
from agreements.factories import EDSRAgreementFactory
from agreements.factories import SDISAgreementFactory
from sub_agreements.factories import PreAvisEDSRFactory

from .factories import PreAvisCommissariatFactory
from .factories import PreAvisCGDFactory
from .factories import PreAvisCompagnieFactory
from .factories import PreAvisServiceCGFactory


def create_ddsp_tree():
    departement = DepartementFactory.create()
    _ddsp = DDSPFactory.create(departement=departement)
    arrondissement = ArrondissementFactory.create(departement=departement)
    city = CommuneFactory.create(arrondissement=arrondissement)
    commissariat = CommissariatFactory.create(commune=city)
    manifestation = AutorisationNMFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(
        manifestation=manifestation,
    )
    avis = DDSPAgreementFactory.create(authorization=authorization)
    return avis, commissariat


class PreAvisCommissariatMethodTests(TestCase):

    def test_str(self):
        avis, commissariat = create_ddsp_tree()
        sub_agreement = PreAvisCommissariatFactory.create(
            avis=avis,
            commissariat=commissariat,
        )
        manifestation = sub_agreement.avis.get_manifestation()
        self.assertEqual(
            sub_agreement.__str__(),
            ' - '.join([
                manifestation.__str__(),
                sub_agreement.commissariat.__str__(),
            ]),
        )

    def test_get_absolute_url(self):
        avis, commissariat = create_ddsp_tree()
        sub_agreement = PreAvisCommissariatFactory.create(
            avis=avis,
            commissariat=commissariat,
        )
        self.assertEqual(
            sub_agreement.get_absolute_url(),
            reverse('sub_agreements:commissariat_subagreement_detail',
                    kwargs={'pk': sub_agreement.pk})
        )

    def test_acknowledge(self):
        avis, commissariat = create_ddsp_tree()
        sub_agreement = PreAvisCommissariatFactory.create(
            avis=avis,
            commissariat=commissariat,
        )
        sub_agreement.acknowledge()
        self.assertEqual(sub_agreement.reply_date, datetime.date.today())

    def test_creation(self):
        avis, commissariat = create_ddsp_tree()
        avis = avis
        self.assertEqual(avis.preavis_set.all().count(), 0)
        avis.commissariats_concernes.add(commissariat)
        self.assertEqual(avis.preavis_set.all().count(), 1)


def create_edsr_tree():
    departement = DepartementFactory.create()
    _edsr = EDSRFactory.create(departement=departement)
    arrondissement = ArrondissementFactory.create(departement=departement)
    cgd = CGDFactory.create(arrondissement=arrondissement)
    city = CommuneFactory.create(arrondissement=arrondissement)
    manifestation = AutorisationNMFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(
        manifestation=manifestation,
    )
    avis = EDSRAgreementFactory.create(authorization=authorization)
    return avis, cgd


class PreAvisCGDMethodTests(TestCase):

    def test_str(self):
        avis, cgd = create_edsr_tree()
        sub_agreement = PreAvisCGDFactory.create(
            avis=avis,
            gendarmerie_company=cgd,
        )
        manifestation = sub_agreement.avis.get_manifestation()
        self.assertEqual(
            sub_agreement.__str__(),
            ' - '.join([
                manifestation.__str__(),
                sub_agreement.gendarmerie_company.__str__(),
            ]),
        )

    def test_get_absolute_url(self):
        avis, cgd = create_edsr_tree()
        sub_agreement = PreAvisCGDFactory.create(
            avis=avis,
            gendarmerie_company=cgd,
        )
        self.assertEqual(
            sub_agreement.get_absolute_url(),
            reverse('sub_agreements:cgd_subagreement_detail',
                    kwargs={'pk': sub_agreement.pk})
        )

    def test_creation(self):
        avis, cgd = create_edsr_tree()
        avis = avis
        self.assertEqual(avis.preavis_set.all().count(), 0)
        avis.concerned_cgd.add(cgd)
        self.assertEqual(avis.preavis_set.all().count(), 1)


def create_sdis_tree():
    departement = DepartementFactory.create()
    _sdis = SDISFactory.create(departement=departement)
    arrondissement = ArrondissementFactory.create(departement=departement)
    city = CommuneFactory.create(arrondissement=arrondissement)
    manifestation = AutorisationNMFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(
        manifestation=manifestation,
    )
    avis = SDISAgreementFactory.create(authorization=authorization)
    return avis


class PreAvisCompagnieMethodTests(TestCase):

    def test_str(self):
        sub_agreement = PreAvisCompagnieFactory.create(
            avis=create_sdis_tree(),
        )
        manifestation = sub_agreement.avis.get_manifestation()
        self.assertEqual(
            sub_agreement.__str__(),
            ' - '.join([
                manifestation.__str__(),
                sub_agreement.company.__str__(),
            ]),
        )

    def test_get_absolute_url(self):
        sub_agreement = PreAvisCompagnieFactory.create(
            avis=create_sdis_tree(),
        )
        self.assertEqual(
            sub_agreement.get_absolute_url(),
            reverse('sub_agreements:company_subagreement_detail',
                    kwargs={'pk': sub_agreement.pk})
        )

    def test_creation(self):
        avis = create_sdis_tree()
        self.assertEqual(avis.preavis_set.all().count(), 0)
        avis.compagnies_concernees.add(
            CompagnieFactory.create()
        )
        self.assertEqual(avis.preavis_set.all().count(), 1)


def create_cg_tree():
    departement = DepartementFactory.create()
    arrondissement = ArrondissementFactory.create(departement=departement)
    city = CommuneFactory.create(arrondissement=arrondissement)
    _council = CGFactory.create(departement=departement)
    manifestation = AutorisationNMFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(
        manifestation=manifestation,
    )
    avis = CGAgreementFactory.create(authorization=authorization)
    return avis


class PreAvisServiceCGMethodTests(TestCase):

    def test_str(self):
        sub_agreement = PreAvisServiceCGFactory.create(
            avis=create_cg_tree()
        )
        manifestation = sub_agreement.avis.authorization.manifestation
        self.assertEqual(
            sub_agreement.__str__(),
            ' - '.join([
                manifestation.__str__(),
                sub_agreement.cg_service.__str__(),
            ]),
        )

    def test_get_absolute_url(self):
        sub_agreement = PreAvisServiceCGFactory.create(
            avis=create_cg_tree(),
        )
        self.assertEqual(
            sub_agreement.get_absolute_url(),
            reverse('sub_agreements:cgservice_subagreement_detail',
                    kwargs={'pk': sub_agreement.pk})
        )

    def test_creation(self):
        avis = create_cg_tree()
        self.assertEqual(avis.preavis_set.all().count(), 0)
        avis.concerned_services.add(
            CGServiceFactory.create()
        )
        self.assertEqual(avis.preavis_set.all().count(), 1)


def create_ggd_tree():
    departement = DepartementFactory.create()
    _ggd = GGDFactory.create(departement=departement)
    arrondissement = ArrondissementFactory.create(departement=departement)
    cgd = CGDFactory.create(arrondissement=arrondissement)
    edsr = EDSRFactory.create(departement=departement)
    city = CommuneFactory.create(arrondissement=arrondissement)
    manifestation = AutorisationNMFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(
        manifestation=manifestation
    )
    avis = GGDAgreementFactory.create(authorization=authorization, concerned_edsr=edsr)
    return avis, cgd, edsr


class PreAvisEDSRMethodTests(TestCase):
    """
    Tests pour les préavis EDSR
    """

    def test_str(self):
        if settings.DEPARTMENT_SETTINGS['EDSR_OR_GGD'] == 'GGD_SubAgentEDSR':
            avis, cgd, edsr = create_ggd_tree()
            sub_agreement = PreAvisEDSRFactory.create(
                avis=avis,
                edsr=edsr,
            )
            manifestation = sub_agreement.avis.get_manifestation()
            self.assertEqual(
                sub_agreement.__str__(),
                ' - '.join([
                    manifestation.__str__(),
                    sub_agreement.edsr.__str__(),
                ]),
            )

    def test_get_absolute_url(self):
        if settings.DEPARTMENT_SETTINGS['EDSR_OR_GGD'] == 'GGD_SubAgentEDSR':
            avis, cgd, edsr = create_ggd_tree()
            sub_agreement = PreAvisEDSRFactory.create(
                avis=avis,
                edsr=edsr,
            )
            self.assertEqual(
                sub_agreement.get_absolute_url(),
                reverse('sub_agreements:edsr_subagreement_detail',
                        kwargs={'pk': sub_agreement.pk})
            )

    def test_creation(self):
        """
        Créer un une arborescence impliquant un sous-avis d'EDSR
        """
        if settings.DEPARTMENT_SETTINGS['EDSR_OR_GGD'] == 'GGD_SubAgentEDSR':
            avis, cgd, edsr = create_ggd_tree()

            self.assertEqual(avis.preavis_set.all().count(), 1)
            avis.concerned_cgd.add(cgd)
            self.assertEqual(avis.preavis_set.all().count(), 2)
