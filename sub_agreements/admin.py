# coding: utf-8
from __future__ import unicode_literals

from django.contrib import admin

from .models import PreAvisCGD
from .models import PreAvisEDSR
from .models import PreAvisCompagnie
from .models import PreAvisCommissariat
from .models import PreAvisServiceCG


class PreAvisCGDInline(admin.StackedInline):
    model = PreAvisCGD
    extra = 0


class ESRPreAvisInline(admin.StackedInline):
    model = PreAvisEDSR
    extra = 0


class PreAvisCompagnieInline(admin.StackedInline):
    model = PreAvisCompagnie
    extra = 0


class PreAvisCommissariatInline(admin.StackedInline):
    model = PreAvisCommissariat
    extra = 0


class PreAvisServiceCGInline(admin.StackedInline):
    model = PreAvisServiceCG
    extra = 0
