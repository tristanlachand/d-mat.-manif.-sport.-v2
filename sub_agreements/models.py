# coding: utf-8
from __future__ import unicode_literals

import datetime

from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django_fsm import transition

from administration.models import Brigade
from administration.models import CGD
from administration.models import CGService
from administration.models import CIS
from administration.models import Commissariat
from administration.models import Compagnie, EDSR
from agreements.models import Avis
from agreements.models import AvisBase, GGDAvis
from agreements.models import CGAvis
from agreements.models import DDSPAvis
from agreements.models import EDSRAvis
from agreements.models import SDISAvis
from authorizations.models import ManifestationAutorisation
from authorizations.signals import authorization_published
from core.util.application import get_instance_settings
from ddcs_loire.utils import add_log_entry
from ddcs_loire.utils import add_notification_entry


# TODO : A voir si on renomme preavis_set en preavis ou pas
class PreAvisQuerySet(models.QuerySet):
    def to_process(self):
        return self.filter(avis__authorization__manifestation__end_date__gt=timezone.now())


class PreAvis(AvisBase):
    avis = models.ForeignKey("agreements.avis",
                             verbose_name="avis")
    objects = PreAvisQuerySet.as_manager()

    class Meta:
        default_related_name = "preavis_set"
        app_label = "sub_agreements"

    def log_ack(self, agents):
        add_log_entry(
            agents=agents,
            action="pré-avis rendu",
            manifestation=self.avis.get_manifestation(),
        )

    def notify_creation(self, agents, content_object):
        add_notification_entry(agents=agents,
                               manifestation=self.avis.get_manifestation(),
                               content_object=content_object,
                               subject="pré-avis demandés")

    def notify_ack(self, agents, content_object, institutional=None):
        add_notification_entry(agents=agents,
                               manifestation=self.avis.get_manifestation(),
                               content_object=content_object,
                               subject="pré-avis rendu",
                               institutional=institutional)

    def notify_publication(self, agents):
        prefecture = self.avis.authorization.get_concerned_prefecture()
        add_notification_entry(agents=agents,
                               manifestation=self.avis.get_manifestation(),
                               subject="arrêté d'autorisation publié",
                               content_object=prefecture)


class PreAvisCGD(PreAvis):
    preavis_ptr = models.OneToOneField("sub_agreements.preavis", parent_link=True,
                                       related_name='preaviscgd')
    gendarmerie_company = models.ForeignKey(
        "administration.cgd",
        verbose_name="CGD",
    )
    concerned_brigades = models.ManyToManyField(
        "administration.brigade",
        verbose_name="Brigades concernées",
    )
    objects = PreAvisQuerySet.as_manager()

    class Meta:
        verbose_name = "pré-avis CGD"
        verbose_name_plural = "pré-avis CGD"
        default_related_name = "preaviscgd_set"
        app_label = "sub_agreements"

    def __str__(self):
        manifestation = self.avis.get_manifestation()
        cgd = self.gendarmerie_company
        return ' - '.join([
            manifestation.__str__(),
            cgd.__str__(),
        ])

    def get_absolute_url(self):
        url = 'sub_agreements:cgd_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_edsr(self):
        return self.gendarmerie_company.arrondissement.departement.edsr

    def get_agents(self):
        return self.gendarmerie_company.ggdagentslocaux.all()

    @transition(field='state',
                source=['created', 'notified'], target='notified')
    def notify_brigades(self):
        add_notification_entry(agents=[agent for brigade in self.concerned_brigades.all() for agent in brigade.brigadeagents.all()],
                               manifestation=self.avis.get_manifestation(),
                               content_object=self.gendarmerie_company,
                               subject="prenez connaissance des informations")
        add_log_entry(
            agents=self.get_agents(),
            action="brigades informées",
            manifestation=self.avis.get_manifestation(),
        )

    @transition(field='state', source='notified', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(agents=self.get_edsr().edsragents.all(),
                        content_object=self.gendarmerie_company)
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=PreAvisCGD)
def notify_pre_avis_cgd(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 content_object=instance.get_edsr())


@receiver(m2m_changed, sender=EDSRAvis.concerned_cgd.through)
def create_pre_avis_cgd_1(sender, instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            avis, _ = PreAvisCGD.objects.get_or_create(
                    avis=instance,
                    gendarmerie_company=CGD.objects.get(pk=pk)
            )


@receiver(m2m_changed, sender=GGDAvis.concerned_cgd.through)
def create_pre_avis_cgd_2(sender, instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            avis, _ = PreAvisCGD.objects.get_or_create(
                avis=instance,
                gendarmerie_company=CGD.objects.get(pk=pk)
            )


class PreAvisEDSR(PreAvis):
    preavis_ptr = models.OneToOneField("sub_agreements.preavis", parent_link=True, related_name='preavisedsr')
    edsr = models.ForeignKey("administration.edsr", verbose_name='EDSR')
    objects = PreAvisQuerySet.as_manager()

    class Meta:
        verbose_name = 'pré-avis EDSR'
        verbose_name_plural = 'pré-avis EDSR'
        default_related_name = "preavisedsr_set"
        app_label = "sub_agreements"

    def __str__(self):
        manifestation = self.avis.get_manifestation()
        edsr = self.edsr
        return ' - '.join([manifestation.__str__(), edsr.__str__()])

    def get_absolute_url(self):
        url = 'sub_agreements:edsr_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_edsr(self):
        return self.edsr

    def get_ggd(self):
        return self.edsr.departement.ggd

    def get_agents(self):
        return self.edsr.edsragents.all()

    @transition(field='state',
                source=['created', 'notified'], target='notified')
    def notify_edsr_agents(self):
        add_notification_entry(agents=[agent for agent in self.get_agents()],
                               manifestation=self.avis.get_manifestation(),
                               content_object=self.edsr,
                               subject='prenez connaissance des informations')
        add_log_entry(
            agents=self.get_agents(),
            action='EDSR informé',
            manifestation=self.avis.get_manifestation(),
        )

    @transition(field='state', source=['created', 'notified'], target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(agents=self.get_ggd().ggdagents.all(), content_object=self.edsr)
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=PreAvisEDSR)
def notify_pre_avis_edsr(sender, instance, created, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(), content_object=instance.get_ggd())


@receiver(post_save, sender=GGDAvis)
def create_pre_avis_edsr(sender, instance, created, **kwargs):
    """
    Créer un sous-avis EDSR pour un nouvel avis GGD
    """
    if created:
        # Ne créer le préavis EDSR avec l'avis GGD que dans la configuration AgentLocalEDSR
        if get_instance_settings('EDSR_OR_GGD').lower() == 'ggd_subagentedsr':
            avis, _ = PreAvisEDSR.objects.get_or_create(avis=instance, edsr=instance.concerned_edsr)


@python_2_unicode_compatible
class PreAvisCompagnie(PreAvis):
    preavis_ptr = models.OneToOneField("sub_agreements.preavis", parent_link=True,
                                       related_name='preaviscompagnie')
    company = models.ForeignKey(
        "administration.compagnie",
        verbose_name="compagnie",
    )
    concerned_cis = models.ManyToManyField(
        "administration.cis",
        verbose_name="CIS concerné",
    )
    objects = PreAvisQuerySet.as_manager()

    class Meta:
        verbose_name = "pré-avis compagnie"
        verbose_name_plural = "pré-avis compagnies"
        default_related_name = "preaviscompagnie_set"
        app_label = "sub_agreements"

    def __str__(self):
        manifestation = self.avis.get_manifestation()
        company = self.company
        return ' - '.join([
            manifestation.__str__(),
            company.__str__(),
        ])

    def get_absolute_url(self):
        url = 'sub_agreements:company_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_sdis(self):
        return self.company.sdis

    def get_agents(self):
        return self.company.compagnieagentslocaux.all()

    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(agents=self.get_sdis().sdisagents.all(),
                        content_object=self.company)
        self.log_ack(agents=self.get_agents())

    @transition(field='state', source='acknowledged', target='acknowledged')
    def notify_cis(self):
        add_notification_entry(agents=[agent for cis in self.concerned_cis.all() for agent in cis.cisagents.all()],
                               manifestation=self.avis.get_manifestation(),
                               content_object=self.company,
                               subject="prenez connaissance des informations")
        add_log_entry(agents=self.get_agents(),
                      action="CIS informé",
                      manifestation=self.avis.get_manifestation())


@receiver(post_save, sender=PreAvisCompagnie)
def notify_pre_avis_compagnie(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 content_object=instance.get_sdis())


@receiver(m2m_changed, sender=SDISAvis.compagnies_concernees.through)
def create_pre_avis_compagnies(instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            PreAvisCompagnie.objects.get_or_create(
                avis=instance,
                company=Compagnie.objects.get(pk=pk)
            )


@python_2_unicode_compatible
class PreAvisCommissariat(PreAvis):
    preavis_ptr = models.OneToOneField("sub_agreements.preavis", parent_link=True,
                                       related_name='preaviscommissariat')
    commissariat = models.ForeignKey(
        "administration.commissariat",
        verbose_name="commissariat",
    )
    objects = PreAvisQuerySet.as_manager()

    class Meta:
        verbose_name = "pré-avis commissariat"
        verbose_name_plural = "pré-avis commissariats"
        default_related_name = "preaviscommissariat_set"
        app_label = "sub_agreements"

    def __str__(self):
        manifestation = self.avis.get_manifestation()
        commissariat = self.commissariat
        return ' - '.join([
            manifestation.__str__(),
            commissariat.__str__(),
        ])

    def get_absolute_url(self):
        url = 'sub_agreements:commissariat_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_ddsp(self):
        return self.commissariat.commune.arrondissement.departement.ddsp

    def get_agents(self):
        return self.commissariat.commissariatagentslocaux.all()

    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        self.notify_ack(agents=self.get_ddsp().ddspagents.all(),
                        content_object=self.commissariat,
                        institutional=self.get_ddsp())
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=PreAvisCommissariat)
def notify_pre_avis_commissariat(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 content_object=instance.get_ddsp())


@receiver(m2m_changed, sender=DDSPAvis.commissariats_concernes.through)
def create_pre_avis_commissariats(instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            PreAvisCommissariat.objects.get_or_create(
                avis=instance,
                commissariat=Commissariat.objects.get(pk=pk)
            )


@python_2_unicode_compatible
class PreAvisServiceCG(PreAvis):
    preavis_ptr = models.OneToOneField("sub_agreements.preavis", parent_link=True,
                                       related_name='preavisservicecg')
    cg_service = models.ForeignKey(
        "administration.cgservice",
        verbose_name="service CG",
    )
    objects = PreAvisQuerySet.as_manager()

    class Meta:
        verbose_name = "pré-avis service CG"
        verbose_name_plural = "pré-avis services CG"
        default_related_name = "preavisservicecg_set"
        app_label = "sub_agreements"

    def __str__(self):
        manifestation = self.avis.get_manifestation()
        cg_service = self.cg_service
        return ' - '.join([
            manifestation.__str__(),
            cg_service.__str__(),
        ])

    def get_absolute_url(self):
        url = 'sub_agreements:cgservice_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_agents(self):
        return self.cg_service.cgserviceagentslocaux.all()

    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        self.reply_date = datetime.date.today()
        self.save()

        council = self.cg_service.cg
        self.notify_ack(agents=council.cgagents.all(),
                        content_object=self.cg_service,
                        institutional=council)
        self.log_ack(agents=self.get_agents())


@receiver(post_save, sender=PreAvisServiceCG)
def notify_pre_avis_service_cg(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 content_object=instance.cg_service.cg)


@receiver(m2m_changed, sender=CGAvis.concerned_services.through)
def create_pre_avis_service_cgs(instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            PreAvisServiceCG.objects.get_or_create(
                avis=instance,
                cg_service=CGService.objects.get(pk=pk)
            )


@receiver(authorization_published, sender=ManifestationAutorisation)
def notify_authorization_publication(instance, **kwargs):
    for preavis in PreAvisCGD.objects.filter(avis__authorization=instance):
        preavis.notify_publication(agents=preavis.get_agents())
    for preavis in PreAvisCompagnie.objects.filter(avis__authorization=instance):
        preavis.notify_publication(agents=preavis.get_agents())
    for preavis in PreAvisCommissariat.objects.filter(avis__authorization=instance):
        preavis.notify_publication(agents=preavis.get_agents())
    for preavis in PreAvisServiceCG.objects.filter(avis__authorization=instance):
        preavis.notify_publication(agents=preavis.get_agents())
