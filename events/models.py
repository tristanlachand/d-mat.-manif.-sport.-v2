# coding: utf-8
import base64
import calendar
import datetime
import hashlib
import time
from urllib.parse import urlencode

import requests
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.flatpages.models import FlatPage
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from model_utils.managers import InheritanceManager

from administrative_division.models import CHOICES
from administrative_division.models import Commune, Departement
from contacts.models import Contact
from ddcs_loire.utils import add_log_entry
from ddcs_loire.utils import add_notification_entry
from sports.models import Activite, FederationMultiSport
from core.util.admin import set_admin_info


MAIL_LIAISON = "pour la manifestation"


class Organisateur(models.Model):
    user = models.OneToOneField('auth.user', related_name="organisateur", verbose_name="utilisateur")
    cgu = models.BooleanField("acceptation des CGU", default=False)

    class Meta:
        verbose_name = "organisateur"
        default_related_name = "organisateurs"
        app_label = "events"

    def __str__(self):
        return self.user.get_username()

    def get_department(self):

        return self.structure.commune.arrondissement.departement.name

    def hash_openrunner_secret_key(self):
        result = hashlib.md5()
        result.update(settings.OPENRUNNER_SECRET_KEY.encode())
        return result.hexdigest()

    def encrypt_for_openrunner(self, hashed_secret_key, string):
        key_index = -1
        result = b''
        for string_index in range(0, len(string)):
            key_index += 1
            if key_index > 31:
                key_index = 0
            one = ord(string[string_index])
            # all remaining chars of the hashed secret key will
            # have the value 0
            if key_index > len(hashed_secret_key) - 1:
                two = 0
            else:
                two = ord(hashed_secret_key[key_index])
            merged_char = one + two
            # if merged char is out of range for ascii representation
            if merged_char > 255:
                merged_char -= 256
            result = b''.join([result, bytes([merged_char])])
        return base64.b64encode(result)

    def decrypt_from_openrunner(self, hashed_secret_key, string):
        key_index = -1
        result = b''
        string = base64.b64decode(string)
        for string_index in range(0, len(string)):
            key_index += 1
            if key_index > 31:
                key_index = 0
            one = string[string_index]
            # all remaining chars of the hashed secret key will
            # have the value 0
            if key_index > len(hashed_secret_key) - 1:
                two = 0
            else:
                two = ord(hashed_secret_key[key_index])
            merged_char = one - two
            # if merged char is out of range for ascii representation
            if merged_char < 1:
                merged_char += 256
            result = b''.join([result, bytes([merged_char])])
        result = result.decode('iso-8859-1')
        return result

    def build_parameters(self):

        parameters = ','.join([self.user.email,
                               self.user.username,
                               self.get_department()])

        parameters = self.encrypt_for_openrunner(
                self.hash_openrunner_secret_key(),
                parameters
        )
        return urlencode({'cddcs': parameters})

    def get_compte_openrunner_creation_url(self):
        url = ''.join([settings.OPENRUNNER_HOST,
                       settings.OPENRUNNER_CREATE_ACCOUNT,
                       self.build_parameters(),
                       '&t=',
                       str(calendar.timegm(time.gmtime()))])
        return url

    def process_openrunner_response(self, response_encoding, response_text,
                                    account, created):
        response_code = int(response_text.split(':')[0])
        response_value = response_text.split(':')[1]
        if response_code == 1:
            # we save the account
            # 2 cases :
            # - account has just been created (created=False by default)
            # - account was previously created with an error
            openrunner_password = self.decrypt_from_openrunner(
                    self.hash_openrunner_secret_key(),
                    response_value,
            )
            account.set_password(openrunner_password)
            account.notify_creation()
        else:
            account.update_error_message(created, response_value)

    def create_compte_openrunner(self):
        # first we look if an openrunner account for this promoter
        # allready exists
        # if not, openrunner account is created, else, we get it
        account, created = CompteOpenRunner.objects.get_or_create(
                organisateur=self,
        )
        if created:
            try:
                response = requests.get(
                        self.get_compte_openrunner_creation_url()
                )
            except:
                account.update_error_message(created,
                                             'ddcs.openrunner.com down')
            else:
                self.process_openrunner_response(response.encoding,
                                                 response.text,
                                                 account, created)

    def get_openrunner_routes(self):
        try:

            routes = self.compteopenrunner.get_routes()

        except CompteOpenRunner.DoesNotExist:
            routes = {}
        if routes == []:
            routes = {}
        return [(int(route[0]), route[1]) for route in routes.items()]


@python_2_unicode_compatible
class CompteOpenRunner(models.Model):
    organisateur = models.OneToOneField('events.organisateur',
                                        related_name="compteopenrunner",
                                        verbose_name="organisateur")
    created = models.BooleanField("créé", default=False)
    creation_date = models.DateTimeField(
            "date de création",
            blank=True,
            null=True,
    )
    password = models.CharField(
            "mot de passe",
            max_length=255,
            blank=True
    )
    error_message = models.CharField(
            "message d'erreur",
            max_length=255,
            blank=True
    )

    class Meta:
        verbose_name = "compte OpenRunner"
        verbose_name_plural = "comptes OpenRunner"
        default_related_name = "comptesopenrunner"
        app_label = "events"

    def __str__(self):
        return self.organisateur.user.username

    def update_error_message(self, created, error_message):
        # we update the account only if it was not existing before
        if created:
            self.creation_date = timezone.now()
            self.error_message = error_message
            self.save()

    def set_password(self, openrunner_password):
        self.created = True
        self.creation_date = timezone.now()
        self.password = openrunner_password
        self.error_message = ''
        self.save()

    def notify_creation(self):
        # TODO : a enlever
        # activate(settings.LANGUAGE_CODE)
        subject = "Création de votre compte OpenRunner"
        message = ''.join([
            "Votre compte Openrunner a été créé avec succès.\n",
            "Vous trouverez ci-dessous les données vous permettant de vous identifier sur ",
            settings.OPENRUNNER_HOST,
            "\n"
            "Login : ",
            self.organisateur.user.username,
            "\n"
            "Mot de passe : ",
            self.password,
        ])
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL,
                  [self.organisateur.user.email])

    def get_routes_request_url(self):
        url = ''.join([settings.OPENRUNNER_HOST,
                       settings.OPENRUNNER_GET_ROUTES,
                       self.organisateur.user.username])
        return url

    def get_routes(self):
        try:
            response = requests.get(self.get_routes_request_url())
        except:
            return {}
        else:
            return response.json()['routelist']


@python_2_unicode_compatible
class StructureType(models.Model):
    type_of_structure = models.CharField("type de structure",
                                         max_length=255)

    class Meta:
        verbose_name = "type de structure"
        verbose_name_plural = "types de structure"
        default_related_name = "structuretypes"
        app_label = "events"

    def __str__(self):
        return self.type_of_structure


@python_2_unicode_compatible
class Structure(models.Model):
    name = models.CharField(
            "nom",
            help_text="précisez le nom de la structure organisatrice (nom de l'association ou du "
                      "club, de la société ou de la personne physique s'il s'agit d'un particulier) "
                      "de la manifestation",
            max_length=200,
            unique=True,
    )
    type_of_structure = models.ForeignKey(
            StructureType,
            verbose_name="forme juridique de la structure",
            help_text="opérez votre choix parmi les statuts juridiques proposés",
    )
    address = models.CharField("adresse", max_length=255)
    commune = models.ForeignKey(
            Commune,
            verbose_name="commune",
            limit_choices_to=CHOICES,
    )
    phone = models.CharField("numéro de téléphone", max_length=14)
    website = models.URLField("site web", max_length=200, blank=True)
    organisateur = models.OneToOneField('events.organisateur',
                                        related_name="structure",
                                        verbose_name="organisateur")

    class Meta:
        verbose_name = "structure"
        default_related_name = "structures"
        app_label = "events"

    def __str__(self):
        return self.name.title()

    def get_absolute_url(self):
        url = ''.join(['events:dashboard'])
        return reverse(url)


@receiver(post_save, sender=Structure)
def create_compte_openrunner(created, instance, **kwargs):
    if created:
        instance.organisateur.create_compte_openrunner()


class Manifestation(models.Model):
    """ Manifestation sportive """
    creation_date = models.DateField("date de création", auto_now_add=True, blank=True, null=True)
    structure = models.ForeignKey(Structure, verbose_name="structure")
    name = models.CharField("nom de la manifestation", max_length=255, unique=True)
    begin_date = models.DateTimeField("date de début")
    end_date = models.DateTimeField("date de fin")
    description = models.TextField("description", help_text="précisez les modalités d'organisation et les caractéristiques de la manifestation")
    activite = models.ForeignKey(Activite, verbose_name="activité")
    registered_calendar = models.BooleanField("la manifestation est inscrite sur le calendrier de la fédération délégataire de la discipline", default=False, )
    departure_city = models.ForeignKey(Commune, verbose_name="commune de départ", limit_choices_to=CHOICES)
    other_departments_crossed = models.ManyToManyField(Departement, limit_choices_to={'name__regex': r'^(?!42)'},
                                                       verbose_name="autre départements traversés par la manifestation", blank=True)
    crossed_cities = models.ManyToManyField(Commune, verbose_name="communes traversées", related_name="%(app_label)s_%(class)s_crossed_by",
                                            limit_choices_to=CHOICES)
    openrunner_route = models.CommaSeparatedIntegerField("parcours OpenRunner", max_length=255, blank=True,
                                                         help_text="Maintenez appuyé « Ctrl », ou « Commande (touche pomme) » sur un Mac, pour en sélectionner plusieurs.", )
    contact = GenericRelation(Contact, verbose_name="personne à contacter sur place")
    number_of_entries = models.IntegerField("nombre de participants",
                                            help_text="renseigner le nombre de participants de la précedente édition ou le nombre de participants estimé si première édition")
    max_audience = models.PositiveIntegerField("nombre max. de spéctateurs")

    # markup info
    markup_convention = models.BooleanField("vous êtes signataire de la charte du balisage temporaire",
                                            help_text="Voir : <a href='/charteecob/' target='_blank'>Charte Ecobalisage</a>", default=False, )
    rubalise_markup = models.BooleanField("balisage rubalise", default=False)
    bio_rubalise_markup = models.BooleanField("balisage rubalise biodégradable", default=False)
    chalk_markup = models.BooleanField("balisage à la craie", default=False)
    paneling_markup = models.BooleanField("balisage panneaux (sur piquets ou fixés sur supports naturels)", default=False)
    ground_markup = models.BooleanField("balisage signalétique collé au sol", default=False)
    sticker_markup = models.BooleanField("balisage autocollant", default=False)
    lime_markup = models.BooleanField("balisage à la chaux", default=False)
    plaster_markup = models.BooleanField("balisage au plâtre", default=False)
    confetti_markup = models.BooleanField("balisage confettis", default=False)
    paint_markup = models.BooleanField("balisage à la peinture (aérosol)", default=False)
    acrylic_markup = models.BooleanField("balisage à la peinture acrylique", default=False)
    withdrawn_markup = models.BooleanField("balisage retiré dans les 48h", default=False)
    oneway_roads = models.TextField("voies publiques en sens unique", blank=True,
                                    help_text="veuillez lister les voies publiques à emprunter en sens unique lors de la manifestation")
    closed_roads = models.TextField("fermeture de voies publiques", blank=True,
                                    help_text="veuillez lister les voies publiques à fermer lors de la manifestation")

    # natura 2000 related questions
    big_budget = models.BooleanField("le budget de la manifestation excède 100 000 €", default=False, )
    big_title = models.BooleanField("manifestation donnant lieu à la délivrance d'un titre national ou international", default=False, )
    lucrative = models.BooleanField("manifestation présentant un caractère lucratif et regroupant sur un même site plus de 1500 personnes", default=False, )
    motor_on_closed_road = models.BooleanField(
        "manifestation avec engagement de véhicules terrestres à moteur se déroulant en dehors des voies ouvertes à la circulation", default=False, )
    motor_on_natura2000 = models.BooleanField(
        "manifestation avec engagement de véhicules terrestres à moteur se déroulant sur des voies ouvertes à la circulation et à l'intérieur d'un périmètre Natura 2000",
        default=False, )
    approval_request = models.BooleanField("manifestation avec demande d'homologation d'un circuit de sports motorisés", default=False, )

    is_on_rnr = models.BooleanField("sur périmètre RNR", default=False, help_text="*RNR : Réserve Naturelle Régionale")
    # attached files
    cartography = models.FileField(upload_to='cartographie/%Y/%m/%d', blank=True, null=True, verbose_name="cartographie", max_length=512, )
    manifestation_rules = models.FileField(upload_to='reglements/%Y/%m/%d', blank=True, null=True,
                                           verbose_name="réglement de la manifestation", max_length=512)
    organisateur_commitment = models.FileField(upload_to='engagements/%Y/%m/%d', blank=True, null=True,
                                               verbose_name="déclaration et engagement de l'organisateur",
                                               help_text="un modèle de cette déclaration est disponible <a target='_blank' href='/engagement_organisateur/autorisation/'>ici</a>",
                                               max_length=512)
    insurance_certificate = models.FileField(upload_to='attestation_assurance/%Y/%m/%d', blank=True, null=True, verbose_name="attestation d'assurance",
                                             help_text="attestation de la police d'assurance souscrite par l'organisateur et couvrant sa responsabilité civile ainsi que celle des participants à la manifestation et de toute personne, nommément désignée par l'organisateur, prêtant son concours à l'organisation de la manifestation",
                                             max_length=512)
    safety_provisions = models.FileField(upload_to='securite/%Y/%m/%d', blank=True, null=True, verbose_name="dispositions prises pour la sécurité",
                                         help_text="recenser l'ensemble des dispositions assurant la sécurité et la protection des pratiquants et des tiers ainsi que les mesures prises par l'organisateur pour garantir la tranquilité publique pendant toute la durée de la manifestation (attestation secouristes, ambulance, extincteurs, dépanneuse...)",
                                         max_length=512)
    doctor_attendance = models.FileField(upload_to='attestations_presence_medecin/%Y/%m/%d', blank=True, null=True,
                                         verbose_name="attestation de présence de médecin(s)", max_length=512)
    rounds_safety = models.FileField(upload_to='securisation_epreuves/%Y/%m/%d', blank=True, null=True, verbose_name="sécurisation des épreuves",
                                     help_text="recenser l'ensemble des dispositions assurant la sécurité et la protection des pratiquants et des tiers ainsi que les mesures prises par l'organisateur pour garantir la tranquilité publique pendant toute la durée de la manifestation (carte détaillée et photos des zones de protection du public)",
                                     max_length=512)
    additional_docs = models.FileField(upload_to='doc_complementaires/%Y/%m/%d', blank=True, null=True, verbose_name="documents complémentaires",
                                       max_length=512)
    objects = InheritanceManager()

    class Meta:
        verbose_name = "manifestation"
        verbose_name_plural = "manifestations"
        default_related_name = "manifestations"
        app_label = "events"

    def __str__(self):
        return self.name.title()

    def get_absolute_url(self):
        url = ''.join(['events:', ContentType.objects.get_for_model(self).model, '_detail'])
        return reverse(url, kwargs={'pk': self.pk})

    @set_admin_info(boolean=True, short_description="est soumise à évaluation natura 2000")
    def has_natura2000evaluation(self):
        return hasattr(self, 'natura2000evaluations')

    @set_admin_info(boolean=True, short_description = "est soumise à évaluation RNR")
    def has_rnrevaluation(self):
        return hasattr(self, 'rnrevaluations')

    def legal_delay(self):
        """ Renvoie le délai légal en jours pour déclarer ou envoyer la demande d'autorisation """
        return settings.DELAY_SETTINGS['MANIFESTATION_DELAY']

    def get_limit_date(self):
        """
        Returns limit date for declaring or sending authorization request
        """
        return (self.begin_date - datetime.timedelta(days=self.legal_delay())).replace(hour=23, minute=59)

    def get_concerned_prefecture(self):
        """ Renvoyer la préfécture selon la ville de départ """
        return self.departure_city.arrondissement.prefecture

    def two_weeks_left(self):
        """
        Returns true or false according to the number of weeks left
        (under 2 weeks or more than 2 weeks)
        """
        return self.get_limit_date().date() < (datetime.date.today() + datetime.timedelta(weeks=2))

    def delay_exceeded(self):
        """
        If official delay to declare or send the authorization request
        is passed, return True, else, return False
        """
        return datetime.date.today() > self.get_limit_date().date()

    def get_manifestation_routes(self):
        organisateur_routes = self.structure.organisateur.compteopenrunner.get_routes()  # noqa
        return {route: organisateur_routes[route] for route in self.openrunner_route.split(',') if
                route in organisateur_routes}  # noqa

    def get_same_day_manifestations(self):
        """
        Returns all events taking place the same days
        """
        return Manifestation.objects.filter(
                Q(begin_date__range=(self.begin_date, self.end_date)) |
                Q(begin_date__lte=self.begin_date, end_date__gte=self.begin_date)
        ).exclude(Q(structure__organisateur=self.structure.organisateur))

    def get_same_day_same_town_manifestations(self):
        """
        Returns all events taking place :
        - the same days
        - in the same cities
        """
        return self.get_same_day_manifestations().filter(
                Q(departure_city=self.departure_city) |
                Q(crossed_cities__in=self.crossed_cities.all())  # noqa pylint: disable=E1101
        ).distinct()

    def get_conflicts_map_url(self):
        """
        Builds the url to display events routes on the same openrunner map
        """
        possible_conflicts = self.get_same_day_same_town_manifestations()
        routes_ids = [route for manifestation in possible_conflicts if manifestation.openrunner_route for route in
                      manifestation.openrunner_route.split(',')]  # noqa
        if possible_conflicts.count() >= 1:
            if routes_ids:
                return ''.join([settings.OPENRUNNER_HOST, settings.OPENRUNNER_MULTI_ROUTES_DISPLAY, ','.join(routes_ids),])
            else:
                return False
        else:
            return False

    def conflicts_maybe_incomplete(self):
        """
        Returns True if one or more concerned events don't have
        any openrunner route
        """
        result = []
        possible_conflicts = self.get_same_day_same_town_manifestations()
        for manifestation in possible_conflicts:
            if not manifestation.openrunner_route:
                result.append(manifestation.name)
        return result

    def display_rnr_eval_panel(self):
        if self.is_on_rnr and not hasattr(self, 'rnrevaluations'):
            return True
        else:
            return False

    def display_natura2000_eval_panel(self):
        if (not hasattr(self, 'natura2000evaluations') and
                (self.big_budget or self.lucrative)):
            return True
        else:
            return False

    def get_breadcrumbs(self):
        """
        Returns breadcrumbs representing the advencement of the file
        displayed to the user
        """
        breadcrumbs = [["description", 1]]

        if self.display_rnr_eval_panel():
            breadcrumbs.append(["évaluation RNR", 0])
        elif hasattr(self, 'rnrevaluations'):
            breadcrumbs.append(["évaluation RNR", 1])
        if self.display_natura2000_eval_panel():
            breadcrumbs.append(["évaluation Natura 2000", 0])
        elif hasattr(self, 'natura2000evaluations'):
            breadcrumbs.append(["évaluation Natura 2000", 1])
        final, exists = self.get_final_breadcrumb()

        if exists:
            breadcrumbs.append(final)
        return breadcrumbs

    def openrunner_map_optional(self):
        return settings.DEPARTMENT_SETTINGS['OPENRUNNER_MAP_OPTIONAL']

    def ready_for_instruction(self):
        ready = True
        if not self.manifestation_rules:
            ready = False
        if not self.organisateur_commitment:
            ready = False
        if not self.safety_provisions:
            ready = False
        if not self.openrunner_route and not self.openrunner_map_optional():
            ready = False
        return ready

    def processing(self):
        try:
            self.manifestationautorisation
        except:
            try:
                self.manifestationdeclaration
            except:
                return False
            return True
        return True

    processing.boolean = True
    processing.short_description = "Fomulaire validé"

    def missing_additional_data(self):
        return self.documentscomplementaires.filter(attached_document='')


@python_2_unicode_compatible
class DocumentComplementaire(models.Model):
    manifestation = models.ForeignKey(Manifestation)
    desired_information = models.CharField("information requise",
                                           max_length=1024)
    attached_document = models.FileField(
            upload_to='additional_data/%Y/%m/%d',
            blank=True,
            null=True,
            verbose_name="Document",
            max_length=512,
    )

    class Meta:
        verbose_name = "document complémentaire"
        verbose_name_plural = "documents complémentaires"
        default_related_name = "documentscomplementaires"
        app_label = "events"

    def __str__(self):
        return self.desired_information

    def log_data_provision(self):
        add_log_entry(
                agents=[self.manifestation.structure.organisateur],
                action="Documents complémentaires envoyés : " + self.desired_information,
                manifestation=self.manifestation
        )

    def log_data_request(self, prefecture):
        add_log_entry(
                agents=prefecture.instructeurs.all(),
                action="Documents complémentaires requis : " + self.desired_information,
                manifestation=self.manifestation
        )

    def notify_data_provision(self, prefecture):
        add_notification_entry(
                manifestation=self.manifestation,
                agents=prefecture.instructeurs.all(),
                subject="Documents complémentaires envoyés : " + self.desired_information,
                content_object=self.manifestation.structure,
                institutional=prefecture,
        )

    def notify_data_request(self, prefecture):
        add_notification_entry(
                agents=[self.manifestation.structure.organisateur],
                manifestation=self.manifestation,
                subject="Documents complémentaires requis : " + self.desired_information,
                content_object=prefecture,
        )

    def get_prefecture(self):
        return self.manifestation.get_concerned_prefecture()

    def request_data(self):
        prefecture = self.get_prefecture()
        self.notify_data_request(prefecture)
        self.log_data_request(prefecture)

    def provide_data(self):
        prefecture = self.get_prefecture()
        self.notify_data_provision(prefecture)
        self.log_data_provision()


@receiver(post_save, sender=DocumentComplementaire)
def log_data_request_creation(created, instance, **kwargs):
    if created:
        instance.request_data()


class DeclarationNM(Manifestation):
    manifestation_ptr = models.OneToOneField('events.manifestation', parent_link=True,
                                             related_name='declarationnm')
    GROUPED_TRAFFIC_CHOICES = (
        ('p', "plus de 75 piétons"),
        ('c', "plus de 50 cycles ou autres véhicules ou engins non motorisés"),
        ('h', "plus de 25 chevaux ou autres animaux"),
    )

    grouped_traffic = models.CharField(
            "circulation groupée", max_length=1,
            choices=GROUPED_TRAFFIC_CHOICES,
    )
    support_vehicles_number = models.PositiveIntegerField(
            "nombre de véhicules d'accompagnement",
            help_text="nombre de véhicules d'accompagnement",
    )

    class Meta:
        verbose_name = "manifestation sans véhicules terrestres à moteur soumise à déclaration"
        verbose_name_plural = "manifestations sans véhicules terrestres à moteur soumises à déclaration"
        default_related_name = "declarationsnm"
        app_label = "events"

    def get_final_breadcrumb(self):
        exists = True
        try:
            self.manifestationdeclaration
        except:
            breadcrumb = ["déclaration non envoyée", 0]
        else:
            breadcrumb = ["déclaration envoyée", 1]
        return breadcrumb, exists


class AutorisationNM(Manifestation):
    manifestation_ptr = models.OneToOneField('events.manifestation', parent_link=True,
                                             related_name='autorisationnm')
    multisport_federation = models.ForeignKey(
            FederationMultiSport,
            verbose_name="fédération multi-sports",
            blank=True,
            null=True,
    )
    signalers_list = models.FileField(
            upload_to='listes_signaleurs/%Y/%m/%d',
            blank=True,
            null=True,
            verbose_name="liste des signaleurs",
            max_length=512,
    )

    class Meta:
        verbose_name = "manifestation sans véhicules terrestres à moteur soumise à autorisation"
        verbose_name_plural = "manifestations sans véhicules terrestres à moteur soumises à autorisation"
        default_related_name = "autorisationsnm"
        app_label = "events"

    def legal_delay(self):

        if self.other_departments_crossed.count() == 0:
            return settings.DELAY_SETTINGS['ANM_DELAY_NO_DEPARTMENT_CROSSED']
        else:
            return settings.DELAY_SETTINGS['ANM_DELAY_DEPARTMENTS_CROSSED']

    def display_natura2000_eval_panel(self):

        if (not hasattr(self, 'natura2000evaluations') and
                (self.big_budget or self.lucrative or self.big_title)):
            return True
        else:
            return False

    def get_final_breadcrumb(self):
        exists = True
        try:
            self.manifestationautorisation
        except:
            breadcrumb = ["demande d'autorisation non envoyée", 0]
        else:
            breadcrumb = ["demande d'autorisation envoyée", 1]
        return breadcrumb, exists

    def ready_for_instruction(self):
        ready = super(AutorisationNM,
                      self).ready_for_instruction()
        if not self.signalers_list:
            ready = False
        return ready


# TODO : A traduire
class MotorizedConcentration(Manifestation):
    # DVTM    ou    AVTMC
    # Concentration de véhicules terrestres à moteur
    manifestation_ptr = models.OneToOneField('events.manifestation', parent_link=True,
                                             related_name='motorizedconcentration')
    vehicles = models.TextField("type et nombre de véhicules")
    big_concentration = models.BooleanField("plus de 200 autos ou 400 véhicules terrestres à moteur de 2 ou 4 roues",
                                            default=False)

    class Meta:
        verbose_name = "concentration de véhicules terrestres à moteur"
        verbose_name_plural = "concentrations de véhicules terrestres à moteur"
        # TODO : A traduire
        default_related_name = "motorizedconcentrations"
        app_label = "events"

    def legal_delay(self):
        if self.big_concentration:
            return settings.DELAY_SETTINGS['AVTMC_DELAY']
        else:
            return settings.DELAY_SETTINGS['DVTM_DELAY']

    def display_natura2000_eval_panel(self):
        if (not hasattr(self, 'natura2000evaluations') and
                (self.big_budget or self.lucrative or
                     self.big_title or self.motor_on_natura2000)):
            return True
        else:
            return False

    def get_final_breadcrumb(self):

        if self.big_concentration:
            try:
                self.manifestationautorisation
            except:
                breadcrumb = ["demande d'autorisation non envoyée", 0]
            else:
                breadcrumb = ["demande d'autorisation envoyée", 1]
        else:
            try:
                self.manifestationdeclaration
            except:
                breadcrumb = ["déclaration non envoyée", 0]
            else:
                breadcrumb = ["déclaration envoyée", 1]

        return breadcrumb, True

    def ready_for_instruction(self):
        ready = super(MotorizedConcentration, self).ready_for_instruction()
        if self.big_concentration:
            if not self.rounds_safety:
                ready = False
        return ready


# TODO : A traduire
class MotorizedEvent(Manifestation):
    # AVTM sur voie publique
    # Une manifestation avec véhicules terrestres à moteur sur voie publique
    manifestation_ptr = models.OneToOneField('events.manifestation', parent_link=True,
                                             related_name='motorizedevent')
    vehicles = models.TextField("type et nombre de véhicules")
    delegate_federation_agr = models.FileField(
            upload_to='avis_federation_delegataire/%Y/%m/%d',
            blank=True,
            null=True,
            verbose_name="avis fédération délégataire",
            max_length=512,
    )
    public_zone_map = models.FileField(
            upload_to='carte_zone_publique/%Y/%m/%d',
            blank=True,
            null=True,
            verbose_name="carte zone publique",
            max_length=512,
    )
    commissioners = models.FileField(
            upload_to='postes_commissaires/%Y/%m/%d',
            blank=True,
            null=True,
            verbose_name="liste des postes commissaires",
            max_length=512,
    )
    tech_organisateur_certificate = models.FileField(
            upload_to='attestations_orga_tech/%Y/%m/%d',
            blank=True,
            null=True,
            verbose_name="attestation organisateur technique",
            max_length=512,
    )
    hourly_itinerary = models.FileField(
            upload_to='itineraires_horaires/%Y/%m/%d',
            blank=True,
            null=True,
            verbose_name="itinéraire horaire",
            max_length=512,
    )

    class Meta:
        verbose_name = "manifestation avec véhicules terrestres à moteur"
        verbose_name_plural = "manifestations avec véhicules terrestres à moteur"
        # TODO : A traduire
        default_related_name = "motorizedevents"
        app_label = "events"

    def legal_delay(self):
        return settings.DELAY_SETTINGS['AVTM_SVP_DELAY']

    def display_natura2000_eval_panel(self):
        if (not hasattr(self, 'natura2000evaluations') and
                (self.big_budget or self.lucrative or
                     self.big_title or self.motor_on_natura2000)):
            return True
        else:
            return False

    def get_final_breadcrumb(self):
        exists = True
        try:
            self.manifestationautorisation
        except:
            breadcrumb = ["demande d'autorisation non envoyée", 0]
        else:
            breadcrumb = ["demande d'autorisation envoyée", 1]
        return breadcrumb, exists

    def ready_for_instruction(self):
        ready = super(MotorizedEvent, self).ready_for_instruction()
        if not self.commissioners:
            ready = False
        if not self.tech_organisateur_certificate:
            ready = False
        if not self.hourly_itinerary:
            ready = False
        return ready


# TODO : A traduire
class MotorizedRace(Manifestation):
    # AVTM hors voie publique
    # Manifestation hors voie publique ou ouverte à la circulation publique
    SUPPORT_CHOICES = (
        ('s', "circuit"),
        ('g', "terrain"),
        ('r', "parcours"),
    )

    manifestation_ptr = models.OneToOneField('events.manifestation', parent_link=True,
                                             related_name='motorizedrace')
    support = models.CharField("type de support", max_length=1,
                               choices=SUPPORT_CHOICES)
    mass_map = models.FileField(
            upload_to='plan_masse/%Y/%m/%d',
            blank=True,
            null=True,
            verbose_name="plan de masse",
            max_length=512,
    )

    class Meta:
        verbose_name = "manifestation sur circuit, terrain ou parcours, soumise à autorisations"
        verbose_name_plural = "courses avec véhicules terrestres à moteur"
        # TODO : A traduire
        default_related_name = "motorizedraces"
        app_label = "events"

    def legal_delay(self):
        return settings.DELAY_SETTINGS['AVTM_HVP_DELAY']

    def display_natura2000_eval_panel(self):
        if (not hasattr(self, 'natura2000evaluations') and
                (self.big_budget or self.lucrative or
                     self.big_title or self.motor_on_closed_road or
                     self.approval_request)):
            return True
        else:
            return False

    def get_final_breadcrumb(self):
        exists = True
        try:
            self.manifestationautorisation
        except:
            breadcrumb = ["demande d'autorisation non envoyée", 0]
        else:
            breadcrumb = ["demande d'autorisation envoyée", 1]
        return breadcrumb, exists


@receiver(post_save, sender=AutorisationNM)
@receiver(post_save, sender=DeclarationNM)
@receiver(post_save, sender=MotorizedConcentration)
@receiver(post_save, sender=MotorizedEvent)
@receiver(post_save, sender=MotorizedRace)
def log_manifestation_creation(created, instance, **kwargs):
    # disable log creation during fixture loading
    if kwargs['raw']:
        return
    if created:
        instance.actions.create(user=instance.structure.organisateur.user,
                                action="description de la manifestation")
