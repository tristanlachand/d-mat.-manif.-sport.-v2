# coding: utf-8
from __future__ import unicode_literals

import datetime
import factory

from factory.fuzzy import FuzzyDateTime

from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.utils.timezone import utc

from administrative_division.factories import CommuneFactory
from sports.factories import ActiviteFactory, FederationMultiSportFactory

from .models import Organisateur, Structure
from .models import CompteOpenRunner
from .models import AutorisationNM
from .models import DeclarationNM
from .models import MotorizedConcentration
from .models import MotorizedEvent
from .models import MotorizedRace
from .models import StructureType

from .models import create_compte_openrunner



class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: 'jdoe{0}'.format(n))
    email = factory.Sequence(lambda n: 'john.doe{0}@example.com'.format(n))


class OrganisateurFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Organisateur

    user = factory.SubFactory(UserFactory)


class CompteOpenRunnerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CompteOpenRunner

    organisateur = factory.SubFactory(OrganisateurFactory)


class StructureTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = StructureType

    type_of_structure = 'Association'


class StructureFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Structure

    organisateur = factory.SubFactory(OrganisateurFactory)
    name = factory.Sequence(lambda n: 'GRS{0}'.format(n))
    type_of_structure = factory.SubFactory(StructureTypeFactory)
    phone = '06 12 23 45 56'
    website = 'http://www.grs.fr'
    address = factory.Sequence(lambda n: 'address{0}'.format(n))
    commune = factory.SubFactory(CommuneFactory)

    @classmethod
    def _generate(cls, create, attrs):
        """
        Override the default _generate() to disable the post-save signal.
        """
        post_save.disconnect(create_compte_openrunner, Structure)
        structure = super(StructureFactory, cls)._generate(create, attrs)
        post_save.connect(create_compte_openrunner, Structure)
        return structure


class DeclarationNMFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DeclarationNM

    name = factory.Sequence(lambda n: 'Non Motorized Event{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                                 tzinfo=utc)).start_dt
    end_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                               tzinfo=utc)).end_dt
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activite = factory.SubFactory(ActiviteFactory)
    support_vehicles_number = 30
    max_audience = 5000
    grouped_traffic = 'p'


class AutorisationNMFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AutorisationNM

    name = factory.Sequence(lambda n: 'Non Motorized Event{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                                 tzinfo=utc)).start_dt
    end_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                               tzinfo=utc)).end_dt
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activite = factory.SubFactory(ActiviteFactory)
    multisport_federation = factory.SubFactory(FederationMultiSportFactory)
    max_audience = 5000


class MotorizedConcentrationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MotorizedConcentration

    name = factory.Sequence(lambda n: 'Motorized Concentration{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                                 tzinfo=utc)).start_dt
    end_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                               tzinfo=utc)).end_dt
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activite = factory.SubFactory(ActiviteFactory)
    max_audience = 5000


class MotorizedEventFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MotorizedEvent

    name = factory.Sequence(lambda n: 'Motorized Event{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                                 tzinfo=utc)).start_dt
    end_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                               tzinfo=utc)).end_dt
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activite = factory.SubFactory(ActiviteFactory)
    max_audience = 5000


class MotorizedRaceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MotorizedRace

    name = factory.Sequence(lambda n: 'Motorized Race{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                                 tzinfo=utc)).start_dt
    end_date = FuzzyDateTime(datetime.datetime(2010, 1, 1,
                                               tzinfo=utc)).end_dt
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activite = factory.SubFactory(ActiviteFactory)
    max_audience = 5000
