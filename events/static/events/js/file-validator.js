$(document).ready(function () {
    var method = window.location.pathname.split('/').filter(function(el){ return !!el; }).pop();
    var taille_max = 2*1024*1024;
    var files_inputs = $('.clearablefileinput');
    var validated_files_inputs = {};
    var nbr_to_validate = files_inputs.length;

    init();

    function init() {
        // Si il s'agit d'une update, on ne désactive le submit que si un fichier est trop lourd
        //if(method === 'edit') {
        $.each(files_inputs, function (index, elem) {
            validated_files_inputs[elem.id] = true;
            nbr_to_validate -= 1;
            if(elem.files && elem.files.length > 0) {
                validateFile(elem, elem.files[0].size);
            }
        });
        /*} else {
            $('#submit-id-save').prop('disabled', true);
            $.each(files_inputs, function (index, elem) {
                validated_files_inputs[elem.id] = false;
                if(elem.files && elem.files.length > 0) {
                    validateFile(elem, elem.files[0].size);
                }
            });
        }*/

        if(nbr_to_validate > 0) {
            displayFilesTooBig();
        }
    }


    files_inputs.change(function () {
        $(this).parent().find('.info_text').remove();
        validateFile(this, this.files[0].size);
    });


    function validateFile(input, taille) {
        if( taille <= taille_max ) {
            validateFilesList(input.id, true);
        } else {
            var info = document.createElement('p');
            info.className = 'info_text';
            $(info).insertAfter(input);
            $(info).text('Attention! Votre fichier fait '+displaySize(taille)+" alors qu'il doit faire moins de "+taille_max/1024/1024+" Mo");
            $(info).css('color', 'red');
            validateFilesList(input.id, false);
        }
    }


    function validateFilesList(input_id, valid) {
        // Si le fichier devient valide (<=2Mo) alors qu'il ne l'était pas avant
        if(valid && !validated_files_inputs[input_id]) {
            nbr_to_validate -= 1;
            validated_files_inputs[input_id] = valid;
            if(nbr_to_validate === 0) {
                $('#submit-id-save').prop('disabled', false);
                $('#file_too_big').remove();
            }
            // Si le fichier devient invalide (>2Mo) alors qu'il l'était avant
        } else if(!valid && validated_files_inputs[input_id]) {
            nbr_to_validate += 1;
            validated_files_inputs[input_id] = valid;

            $('#submit-id-save').prop('disabled', true);
            displayFilesTooBig();
        }
    }


    function displaySize(size) {
        if(size < 1024) {
            return size+' octets';
        } else if(size < 1024*1024) {
            return (size/1024).toFixed(2)+' Ko';
        } else if(size < 1024*1024*1024) {
            return (size/1024/1024).toFixed(2)+' Mo';
        } else {
            return (size/1024/1024/1024).toFixed(2)+' Go';
        }
    }


    function displayFilesTooBig() {
        $('#file_too_big').remove();
        var file_too_big = document.createElement('p');
        file_too_big.id = "file_too_big";
        $(file_too_big).text("Un ou plusieurs documents n'ont pas été ajoutés ou sont trop lourds." +
            " Veuillez ne sélectionner que des documents faisant moins de 2 Mo.");
        $(file_too_big).css('color', 'red');
        $(file_too_big).insertAfter($('#submit-id-save'));
    }
});
