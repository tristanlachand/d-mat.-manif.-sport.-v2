$(document).ready(function() {
    $('#id_other_departments_crossed').multiselect({
        maxHeight: 500,
        nonSelectedText: 'Aucun département selectionné',
        nSelectedText: ' - départements selectionnés',
        numberDisplayed: 6,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'Rechercher',
        templates : {
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="fa fa-times"></i></button></span>',
        },
    });
    $('#hint_id_other_departments_crossed').remove();
    $('#id_crossed_cities').multiselect({
        maxHeight: 500,
        nonSelectedText: 'Aucune ville selectionnée',
        nSelectedText: ' - villes selectionnées',
        numberDisplayed: 9,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: 'Rechercher',
        templates : {
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="fa fa-times"></i></button></span>',
        },
    });
    $('#hint_id_crossed_cities').remove();
    $('#id_openrunner_route').multiselect({
        maxHeight: 300,
        nonSelectedText: 'Aucun parcours OpenRunner selectionné',
        nSelectedText: ' - parcours selectionnés',
        numberDisplayed: 9,
    });
    $('#hint_id_openrunner_route').remove();
});
