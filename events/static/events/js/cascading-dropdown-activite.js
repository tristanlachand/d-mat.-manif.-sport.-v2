// Source : http://stackoverflow.com/questions/1671446/country-state-city-dropdown-menus-inside-the-django-admin-inline

var response_cache = {};

function fill_activites(discipline_id) {
  if (response_cache[discipline_id]) {
    $("#id_activite").html(response_cache[discipline_id]);
  } else {
    $.getJSON("/activites_par_discipline/", {discipline_id: discipline_id},
      function(ret, textStatus) {
        var options = '<option value="" selected="selected">---------</option>';
        for (var i in ret) {
          options += '<option value="' + ret[i].id + '">'
            + ret[i].name + '</option>';
        }
        response_cache[discipline_id] = options;
        $("#id_activite").html(options);
      });
  }
}

$(document).ready(function() {
  $("#id_discipline").change(function() { fill_activites($(this).val()); });
});