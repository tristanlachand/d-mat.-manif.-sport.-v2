from __future__ import print_function
from __future__ import unicode_literals

from django.core.management.base import NoArgsCommand

from django.contrib.auth.models import User


class Command(NoArgsCommand):

    def handle_noargs(self, **options):
        for user in User.objects.all():
            if User.objects.filter(username=user.username).count() > 1:
                print("{} - {}".format(user, user.email))
