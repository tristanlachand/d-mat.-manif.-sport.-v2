from __future__ import unicode_literals

from django.core.management.base import NoArgsCommand

from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission


class Command(NoArgsCommand):

    def handle_noargs(self, **options):
        ContentType.objects.all().delete()
        Permission.objects.all().delete()
