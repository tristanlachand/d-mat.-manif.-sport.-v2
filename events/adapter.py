# coding: utf-8
from __future__ import unicode_literals

import re

from django import forms
from django.core.urlresolvers import reverse

from allauth.account.adapter import DefaultAccountAdapter

from administration.models import Agent
from administration.models import Instructeur
from administration.models import AgentLocal
from events.models import Organisateur


class MyAccountAdapter(DefaultAccountAdapter):

    def get_login_redirect_url(self, request):
        _result = super(MyAccountAdapter, self).get_login_redirect_url(request)
        try:
            request.user.instructeur
        except Instructeur.DoesNotExist:
            try:
                request.user.organisateur
            except Organisateur.DoesNotExist:
                try:
                    request.user.agent
                except Agent.DoesNotExist:
                    try:
                        request.user.agentlocal
                    except AgentLocal.DoesNotExist:
                        return '/'
                    else:
                        return reverse('sub_agreements:dashboard')
                else:
                    return reverse('agreements:dashboard')
            else:
                return reverse('events:dashboard')
        else:
            return reverse('authorizations:dashboard')

    def clean_username(self, username):
        if not re.match(r'^[a-zA-Z0-9]+$', username):
            raise forms.ValidationError("Le nom d'utilisateur ne peut contenir que des lettres non-accentuées et des "
                                        "chiffres")
        return super(MyAccountAdapter, self).clean_username(username)
