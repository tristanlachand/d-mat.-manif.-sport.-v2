# coding: utf-8
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib.flatpages.admin import FlatPageAdmin, FlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site
from django.db.models import Q

from ckeditor_uploader.fields import RichTextUploadingFormField
from import_export.admin import ImportExportActionModelAdmin

from authorizations.admin import EventAuthorizationInline

from contacts.admin import ContactInline
from core.util.admin import set_admin_info
from core.util.user import UserHelper

from declarations.admin import EventDeclarationInline

from evaluations.admin import RNREvaluationInline
from evaluations.admin import Natura2000EvaluationInline

from .models import DocumentComplementaire
from .models import Organisateur, Structure
from .models import CompteOpenRunner
from .models import StructureType
from .models import AutorisationNM
from .models import DeclarationNM
from .models import MotorizedConcentration
from .models import MotorizedEvent
from .models import MotorizedRace


class StructureTypeAdmin(ImportExportActionModelAdmin):
    pass


class StructureAdmin(ImportExportActionModelAdmin):
    list_display = ['pk', 'name', 'type_of_structure']
    list_filter = ['type_of_structure__type_of_structure']
    search_fields = ['name', ]


class CompteOpenRunnerInline(admin.StackedInline):
    model = CompteOpenRunner


class OrganisateurAdmin(ImportExportActionModelAdmin):
    inlines = [
        CompteOpenRunnerInline,
    ]
    list_filter = ['cgu', 'compteopenrunner__created',
                   'compteopenrunner__error_message']
    search_fields = ['user__username',
                     'user__first_name',
                     'user__last_name',
                     'user__email']
    actions = ['create_compte_openrunner']

    def create_compte_openrunner(self, request, queryset):
        for instance in queryset:
            try:
                instance.compteopenrunner.delete()
            except:
                pass
            instance.create_compte_openrunner()
    create_compte_openrunner.short_description = "Créer un compte openrunner pour chaque organisateur sélectionné"


class DocumentComplementaireInline(admin.TabularInline):
    model = DocumentComplementaire
    extra = 0


class ProcessAskedListFilter(admin.SimpleListFilter):

    def lookups(self, request, model_admin):
        return(
            ('yes', "oui"),
            ('no', "non"),
        )


class DeclarationAskedListFilter(ProcessAskedListFilter):
    title = "déclaration demandée"
    parameter_name = 'declaration_asked'

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(manifestationdeclaration__id__gt=0)
        elif self.value() == 'no':
            return queryset.exclude(manifestationdeclaration__id__gt=0)


class AuthorizationAskedListFilter(ProcessAskedListFilter):
    title = "autorisations demandée"
    parameter_name = 'authorization_asked'

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(manifestationautorisation__id__gt=0)
        if self.value() == 'no':
            return queryset.exclude(manifestationautorisation__id__gt=0)


class ConcentrationAuthorizationAskedListFilter(ProcessAskedListFilter):
    title = "autorisation ou déclaration demandée"
    parameter_name = 'authorization_or_declaration_asked'

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(
                Q(manifestationautorisation__id__gt=0) |
                Q(manifestationdeclaration__id__gt=0)
            )
        if self.value() == 'no':
            return queryset.exclude(
                Q(manifestationautorisation__id__gt=0) |
                Q(manifestationdeclaration__id__gt=0)
            )


class DeclarationNMAdmin(ImportExportActionModelAdmin):  # noqa pylint: disable=R0904
    inlines = [ContactInline,
               DocumentComplementaireInline,
               EventDeclarationInline,
               RNREvaluationInline,
               Natura2000EvaluationInline]
    ordering = ('-begin_date', )
    readonly_fields = ('is_on_rnr', 'creation_date')
    list_display = ('__str__',
                    'begin_date',
                    'processing',
                    'has_rnrevaluation',
                    'has_natura2000evaluation')
    list_filter = ['manifestationdeclaration__state',
                   DeclarationAskedListFilter]
    search_fields = ['name']


class AutorisationNMAdmin(ImportExportActionModelAdmin):  # noqa pylint: disable=R0904
    inlines = [ContactInline,
               DocumentComplementaireInline,
               EventAuthorizationInline,
               RNREvaluationInline,
               Natura2000EvaluationInline]
    ordering = ('-begin_date', )
    readonly_fields = ('is_on_rnr', 'creation_date')
    list_display = ('__str__',
                    'begin_date',
                    'processing',
                    'has_rnrevaluation',
                    'has_natura2000evaluation')
    list_filter = ['manifestationautorisation__state',
                   AuthorizationAskedListFilter]
    search_fields = ['name']


class MotorizedConcentrationAdmin(ImportExportActionModelAdmin):
    inlines = [ContactInline,
               DocumentComplementaireInline,
               EventAuthorizationInline,
               EventDeclarationInline,
               RNREvaluationInline,
               Natura2000EvaluationInline]
    ordering = ('-begin_date', )
    readonly_fields = ('is_on_rnr', 'creation_date')
    list_display = ('__str__',
                    'begin_date',
                    'processing',
                    'has_rnrevaluation',
                    'has_natura2000evaluation')
    list_filter = ['manifestationautorisation__state',
                   'manifestationdeclaration__state',
                   ConcentrationAuthorizationAskedListFilter]
    search_fields = ['name']


class MotorizedEventAdmin(ImportExportActionModelAdmin):
    inlines = [ContactInline,
               DocumentComplementaireInline,
               EventAuthorizationInline,
               RNREvaluationInline,
               Natura2000EvaluationInline]
    ordering = ('-begin_date', )
    readonly_fields = ('is_on_rnr', 'creation_date')
    list_display = ('__str__',
                    'begin_date',
                    'processing',
                    'has_rnrevaluation',
                    'has_natura2000evaluation')
    list_filter = ['manifestationautorisation__state',
                   AuthorizationAskedListFilter]
    search_fields = ['name']


class MotorizedRaceAdmin(ImportExportActionModelAdmin):
    inlines = [ContactInline,
               DocumentComplementaireInline,
               EventAuthorizationInline,
               RNREvaluationInline,
               Natura2000EvaluationInline]
    ordering = ('-begin_date', )
    readonly_fields = ('is_on_rnr', 'creation_date')
    list_display = ('__str__',
                    'begin_date',
                    'processing',
                    'has_rnrevaluation',
                    'has_natura2000evaluation')
    list_filter = ['manifestationautorisation__state',
                   AuthorizationAskedListFilter]
    search_fields = ['name']


class MyFlatpageForm(FlatpageForm):
    # content = forms.CharField(widget=CKEditorWidget())
    content = RichTextUploadingFormField()


class MyFlatPageAdmin(FlatPageAdmin):
    form = MyFlatpageForm


class UtilisateurAdmin(UserAdmin):
    """ Configuration de l'admin utilisateur """
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_active', 'is_staff', 'date_joined', 'get_service')
    list_per_page = 25

    # Getter
    @set_admin_info(short_description="Service")
    def get_service(self, obj):
        """ Renvoie le service associé au compte """
        return UserHelper.get_service_instance(obj)


admin.site.register(Organisateur, OrganisateurAdmin)
admin.site.register(StructureType, StructureTypeAdmin)
admin.site.register(Structure, StructureAdmin)
admin.site.register(DeclarationNM, DeclarationNMAdmin)
admin.site.register(AutorisationNM, AutorisationNMAdmin)
admin.site.register(MotorizedConcentration, MotorizedConcentrationAdmin)
admin.site.register(MotorizedEvent, MotorizedEventAdmin)
admin.site.register(MotorizedRace, MotorizedRaceAdmin)

admin.site.unregister(Site)
admin.site.unregister(User)
admin.site.register(User, UtilisateurAdmin)

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, MyFlatPageAdmin)
