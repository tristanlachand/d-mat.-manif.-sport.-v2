# coding: utf-8
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin

from django.core.urlresolvers import reverse
from django.core.urlresolvers import reverse_lazy

from django.db.models import Count
from django.db.models import Q

from django.http import HttpResponseRedirect

from django.shortcuts import get_object_or_404

from django.utils import timezone
from django.utils.decorators import method_decorator

from django.views.generic import ListView, DetailView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView

from braces.views import LoginRequiredMixin, SuperuserRequiredMixin

from extra_views import CreateWithInlinesView, UpdateWithInlinesView

from administration.models import MairieAgent

from authorizations.decorators import instructeur_required

from contacts.forms import ContactInline

from sports.models import Activite

from .decorators import organisateur_required
from .decorators import owner_required

from .forms import DocumentComplementaireRequestForm
from .forms import DocumentComplementaireProvideForm
from .forms import UserUpdateForm
from .forms import AutorisationNMForm
from .forms import DeclarationNMForm
from .forms import MotorizedConcentrationForm
from .forms import MotorizedEventForm
from .forms import MotorizedRaceForm
from .forms import StructureForm

from .models import DocumentComplementaire
from .models import Manifestation
from .models import AutorisationNM
from .models import DeclarationNM
from .models import MotorizedConcentration
from .models import MotorizedEvent
from .models import MotorizedRace
from .models import Structure
from .models import Organisateur


class HomePage(TemplateView):
    template_name = 'events/home.html'


class Dashboard(ListView):
    model = Manifestation

    @method_decorator(organisateur_required)
    def dispatch(self, *args, **kwargs):
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        try:
            structure = self.request.user.organisateur.structure
        except Structure.DoesNotExist:
            structure = None
        return Manifestation.objects.filter(structure=structure).order_by('-pk')

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        return context


class ProfileDetailView(DetailView):
    model = User

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileDetailView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user


class UserUpdateView(UpdateView):
    model = User
    form_class = UserUpdateForm
    success_url = '/accounts/profile'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user


class Calendar(ListView):
    model = Manifestation
    template_name = 'events/calendar.html'

    def get_queryset(self):
        now = timezone.now()
        return Manifestation.objects.filter(
            ~Q(autorisationnm__manifestationautorisation__id=None) |
            ~Q(declarationnm__manifestationdeclaration__id=None) |
            ~Q(motorizedevent__manifestationautorisation__id=None) |
            ~Q(motorizedrace__manifestationautorisation__id=None) |
            ~Q(motorizedconcentration__manifestationautorisation__id=None) |
            ~Q(motorizedconcentration__manifestationdeclaration__id=None),
            begin_date__gt=now,
        ).order_by('begin_date')

    def get_context_data(self, **kwargs):
        context = super(Calendar, self).get_context_data(**kwargs)
        context['activities'] = Activite.objects.annotate(num_manifestations=Count('manifestation'))\
            .filter(num_manifestations__gt=0).order_by('name')
        return context


class FilteredCalendar(Calendar):

    def get_queryset(self):
        return super(FilteredCalendar, self).get_queryset().filter(activite=self.kwargs['activite'])

    def get_context_data(self, **kwargs):
        context = super(FilteredCalendar, self).get_context_data(**kwargs)
        context['current_activite'] = int(self.kwargs['activite'])
        return context


class StructureCreate(CreateView):
    model = Structure
    form_class = StructureForm

    @method_decorator(organisateur_required)
    def dispatch(self, *args, **kwargs):
        return super(StructureCreate, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.organisateur = self.request.user.organisateur
        return super(StructureCreate, self).form_valid(form)


class StructureUpdateView(UpdateView):
    model = Structure
    form_class = StructureForm
    success_url = '/accounts/profile'

    @method_decorator(organisateur_required)
    def dispatch(self, *args, **kwargs):
        return super(StructureUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user.organisateur.structure


###########################################################
# Generic Choice View
###########################################################
class ChoiceView(TemplateView):

    @method_decorator(organisateur_required)
    def dispatch(self, *args, **kwargs):
        return super(ChoiceView, self).dispatch(*args, **kwargs)


###########################################################
# Types of Manifestation choices Views
###########################################################
class CreateRoute(ChoiceView):
    template_name = 'events/create_route.html'


class ManifestationChoose(ChoiceView):
    template_name = 'events/event_choose.html'


class NonMotorizedEventChoose(ChoiceView):
    template_name = 'events/nonmotorizedevent_choose.html'


class DeclarationNMChoose(ChoiceView):
    template_name = 'events/declarednonmotorizedevent_choose.html'


class MotorizedEventChoose(ChoiceView):
    template_name = 'events/motorizedevent_choose.html'


class PublicRoadMotorizedEventChoose(ChoiceView):
    template_name = 'events/publicroadmotorizedevent_choose.html'


###########################################################
# Manifestation Views (Generic)
###########################################################
class ManifestationDetail(DetailView):

    @method_decorator(owner_required)
    def dispatch(self, *args, **kwargs):
        return super(ManifestationDetail, self).dispatch(*args, **kwargs)


class ManifestationCreate(CreateWithInlinesView):
    inlines = [ContactInline]
    template_name = 'events/event_form.html'

    @method_decorator(organisateur_required)
    def dispatch(self, *args, **kwargs):
        return super(ManifestationCreate, self).dispatch(*args, **kwargs)

    def forms_valid(self, form, inlines):
        self.object.structure = self.request.user.organisateur.structure
        self.object = form.save()
        for formset in inlines:
            formset.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        kwargs = super(ManifestationCreate, self).get_form_kwargs()
        kwargs['routes'] = self.request.user.organisateur.get_openrunner_routes()
        return kwargs


class ManifestationUpdate(UpdateWithInlinesView):
    inlines = [ContactInline]
    template_name = 'events/event_form.html'

    @method_decorator(owner_required)
    def dispatch(self, *args, **kwargs):
        return super(ManifestationUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(ManifestationUpdate, self).get_form_kwargs()
        kwargs['routes'] = self.request.user.organisateur.get_openrunner_routes()
        # Here, we have to transform event routes from a string into a list
        # We use strip(',') to avoid errors caused by leading or trailing commas
        if kwargs['instance'].__dict__['openrunner_route']:
            kwargs['instance'].__dict__['openrunner_route'] = [int(route) for route in kwargs['instance'].__dict__['openrunner_route'].strip(',').split(',')]  # noqa
        return kwargs


###########################################################
# Manifestation Public View
###########################################################
class ManifestationPublicDetail(DetailView):
    model = Manifestation
    template_name = 'events/eventpublic_detail.html'


###########################################################
# DeclarationNM Views
###########################################################
class DeclarationNMDetail(ManifestationDetail):
    model = DeclarationNM


class DeclarationNMCreate(ManifestationCreate):
    model = DeclarationNM
    form_class = DeclarationNMForm


class DeclarationNMUpdate(ManifestationUpdate):
    model = DeclarationNM
    form_class = DeclarationNMForm


###########################################################
# AutorisationNM Views
###########################################################
class AutorisationNMDetail(ManifestationDetail):
    model = AutorisationNM


class AutorisationNMCreate(ManifestationCreate):
    model = AutorisationNM
    form_class = AutorisationNMForm


class AutorisationNMUpdate(ManifestationUpdate):
    model = AutorisationNM
    form_class = AutorisationNMForm


###########################################################
# MotorizedConcentration Views
###########################################################
class MotorizedConcentrationDetail(ManifestationDetail):
    model = MotorizedConcentration


class MotorizedConcentrationCreate(ManifestationCreate):
    model = MotorizedConcentration
    form_class = MotorizedConcentrationForm


class MotorizedConcentrationUpdate(ManifestationUpdate):
    model = MotorizedConcentration
    form_class = MotorizedConcentrationForm


###########################################################
# MotorizedEvent Views
###########################################################
class MotorizedEventDetail(ManifestationDetail):
    model = MotorizedEvent


class MotorizedEventCreate(ManifestationCreate):
    model = MotorizedEvent
    form_class = MotorizedEventForm


class MotorizedEventUpdate(ManifestationUpdate):
    model = MotorizedEvent
    form_class = MotorizedEventForm


###########################################################
# MotorizedRace Views
###########################################################
class MotorizedRaceDetail(ManifestationDetail):
    model = MotorizedRace


class MotorizedRaceCreate(ManifestationCreate):
    model = MotorizedRace
    form_class = MotorizedRaceForm


class MotorizedRaceUpdate(ManifestationUpdate):
    model = MotorizedRace
    form_class = MotorizedRaceForm


###########################################################
# Additionnal Data Request Views
###########################################################
class DocumentComplementaireRequestView(SuccessMessageMixin, CreateView):
    model = DocumentComplementaire
    form_class = DocumentComplementaireRequestForm
    success_message = "Demande de documents complémentaires envoyée avec succès"

    @method_decorator(instructeur_required)
    def dispatch(self, *args, **kwargs):
        return super(DocumentComplementaireRequestView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.manifestation = get_object_or_404(Manifestation,
                                                        pk=self.kwargs['manifestation_pk'])
        return super(DocumentComplementaireRequestView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(DocumentComplementaireRequestView, self).get_context_data(**kwargs)
        context['manifestation'] = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        return context

    def get_success_url(self):
        try:
            declaration = self.object.manifestation.manifestationdeclaration
        except:
            authorization = self.object.manifestation.manifestationautorisation
            return reverse('authorizations:authorization_detail',
                           kwargs={'pk': authorization.pk})
        else:
            return reverse('declarations:declaration_detail',
                           kwargs={'pk': declaration.pk})


class DocumentComplementaireProvideView(SuccessMessageMixin, UpdateView):
    model = DocumentComplementaire
    form_class = DocumentComplementaireProvideForm
    success_message = "Documents complémentaires envoyés avec succès"
    success_url = reverse_lazy('events:dashboard')

    @method_decorator(organisateur_required)
    def dispatch(self, *args, **kwargs):
        return super(DocumentComplementaireProvideView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        self.object = form.save()
        self.object.provide_data()
        return super(DocumentComplementaireProvideView, self).form_valid(form)


###########################################################
# MISC
###########################################################
class MailToOrganisateursView(LoginRequiredMixin, SuperuserRequiredMixin, TemplateView):
    template_name = 'events/mail_to_promoters.html'

    def get_context_data(self, **kwargs):
        context = super(MailToOrganisateursView, self).get_context_data(**kwargs)
        context['mails'] = ",".join(
            [organisateur.user.email for organisateur in Organisateur.objects.all()]
        )
        return context


class MailToTownHallsView(LoginRequiredMixin, SuperuserRequiredMixin, TemplateView):
    template_name = 'events/mail_to_townhalls.html'

    def get_context_data(self, **kwargs):
        context = super(MailToTownHallsView, self).get_context_data(**kwargs)
        context['mails'] = ",".join(
            [agent.user.email for agent in MairieAgent.objects.all()]
        )
        return context
