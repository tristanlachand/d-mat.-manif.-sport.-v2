# coding: utf-8
from __future__ import unicode_literals

import copy

from itertools import groupby
from operator import attrgetter

from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.forms import widgets, ModelChoiceField
from django.forms.utils import flatatt
from django.forms.widgets import SelectMultiple
from django.utils.safestring import mark_safe

from core.util.application import get_instance_settings
from ddcs_loire.utils import GenericForm

from bootstrap3_datetime.widgets import DateTimePicker

from localflavor.fr.forms import FRPhoneNumberField

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, HTML

from administrative_division.models import Commune

from sports.models import Activite, Discipline

from .models import DocumentComplementaire
from .models import Organisateur, Structure, StructureType
from .models import DeclarationNM
from .models import AutorisationNM
from .models import MotorizedConcentration
from .models import MotorizedEvent
from .models import MotorizedRace


class StructureForm(GenericForm):
    phone = FRPhoneNumberField()

    class Meta:
        model = Structure
        exclude = ('organisateur', )

    def __init__(self, *args, **kwargs):
        super(StructureForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', "Enregistrer"))


class UserUpdateForm(GenericForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name')

    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', "Enregistrer"))


class SignupForm(forms.Form):
    first_name = forms.CharField(
        max_length=30,
        label="Prénom du déclarant",
    )
    last_name = forms.CharField(
        max_length=30,
        label="Nom du déclarant",
    )
    cgu = forms.BooleanField(
        label="En cochant cette case, j'accepte les Conditions Générale d'Utilisation dont "
              "les liens sont présents ci-dessus",
    )

    # structure fields
    structure_name = forms.CharField(
        label="Nom de la structure",
        help_text="Précisez le nom de la structure organisatrice (nom de l'association ou du "
                  "club, de la société ou de la personne physique s'il s'agit d'un particulier) "
                  "de la manifestation",
        max_length=200,
    )
    type_of_structure = forms.ModelChoiceField(
        label="Forme juridique de la structure",
        help_text="Opérez votre choix parmi les statuts juridiques proposés",
        queryset=StructureType.objects.all(),
    )
    address = forms.CharField(
        label="Adresse",
        max_length=255,
    )
    commune = forms.ModelChoiceField(
        label="Commune",
        queryset=Commune.objects.filter(
            arrondissement__departement__name=get_instance_settings('DEPARTMENT_NUMBER'),
        ),
    )
    phone = forms.CharField(
        label="Numéro de téléphone",
        max_length=14,
    )
    website = forms.URLField(
        label="Site web",
        max_length=200,
        required=False
    )

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
        self.fields['phone'] = FRPhoneNumberField()
        self.fields['username'].help_text = "le nom d'utilisateur vous servira comme identifiant lors de la connexion au site"
        self.helper.layout = Layout(
            Fieldset(
                "Organisateur",
                HTML("<div class='well'>{text}</div>".format(
                    text="renseignez votre identité en tant que représentant de la structure pour le "
                         "compte duquel vous faites la déclaration ou la demande d'autorisation"
                ).capitalize()),
                'first_name',
                'last_name',
                'username',
                'email',
                'password1',
                'password2',
                HTML(
                    "<div class='well'>{0} : <br/>"
                    " - <a href='{1}' target='_blank'>{2}</a><br/>"
                    " - <a href='{3}' target='_blank'>{4}</a><br/>"
                    "</div>".format(
                        "Conditions Générales d'Utilisation",
                        '/CGU/manifestationsportiveloire',
                        'pour la plateforme manifestationsportive.fr',
                        '/CGU/openrunner',
                        'pour la plateforme openrunner.com',
                    )
                ),
                'cgu',
            ),
            Fieldset(
                "Structure",
                'structure_name',
                'type_of_structure',
                'address',
                'commune',
                'phone',
                'website',
            )
        )
        self.helper.add_input(Submit('submit', "Inscription"))

    def clean_structure_name(self):
        structure_name = self.cleaned_data['structure_name']
        if Structure.objects.filter(name=structure_name).exists():
            raise forms.ValidationError(
                "Le nom \"{name}\" est déjà utilisé.".format(
                    name=structure_name
                )
            )
        return structure_name

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.username = self.cleaned_data['username']
        user.email = self.cleaned_data['email']
        cgu = self.cleaned_data['cgu']
        organisateur = Organisateur(user=user, cgu=cgu)
        organisateur.save()
        Structure.objects.create(
            name=self.cleaned_data['structure_name'],
            type_of_structure=self.cleaned_data['type_of_structure'],
            phone=self.cleaned_data['phone'],
            website=self.cleaned_data['website'],
            organisateur=organisateur,
            address=self.cleaned_data['address'],
            commune=self.cleaned_data['commune'],
        )


class ActiviteChoiceField(forms.ModelChoiceField):
    """
    We override this class in order to display activity sports as optgroups
    """

    def __init__(self, queryset, **kwargs):
        super(ActiviteChoiceField, self).__init__(queryset, **kwargs)

        groups = groupby(queryset, attrgetter('discipline'))
        self.choices = (
            (discipline, [(a.id, self.label_from_instance(a)) for a in activities])
            for discipline, activities in groups
        )


# Source : http://stackoverflow.com/questions/1671446/country-state-city-dropdown-menus-inside-the-django-admin-inline
class ActiviteChoiceWidget(widgets.Select):
    def render(self, name, value, attrs=None, choices=()):
        self.choices = [("", "---------")]
        if value is None:
            value = ''
            model_obj = self.form_instance.instance
            # if model_obj and model_obj.discipline:
            #     for a in model_obj.discipline.activites.all():
            #         self.choices.append((a.id, a))
        else:
            obj = Activite.objects.get(id=value)
            for a in Activite.objects.filter(discipline=obj.discipline):
                self.choices.append((a.id, a))
        # copy-paste from widgets.Select.render
        final_attrs = self.build_attrs(attrs, name=name)
        output = [u'<select%s>' % flatatt(final_attrs)]
        options = self.render_options(choices, [value])
        if options:
            output.append(options)
        output.append('</select>')
        return mark_safe(u'\n'.join(output))


FIELDS = Layout(
    Fieldset(
        "Manifestation",
        'name',
        'begin_date',
        'end_date',
        'description',
        'discipline',
        'activite',
        'registered_calendar',
        'is_on_rnr',
        'departure_city',
        'other_departments_crossed',
        'crossed_cities',
        'openrunner_route',
        'number_of_entries',
        'max_audience',
    ),
    Fieldset(
        "Gestion des voies publiques",
        'oneway_roads',
        'closed_roads',
    ),
    Fieldset(
        "Fichiers attachés - Ne pas utiliser d'accent dans le nom des fichiers !",
        'manifestation_rules',
        'organisateur_commitment',
        'insurance_certificate',
        'safety_provisions',
        'doctor_attendance',
        'additional_docs',
        'rounds_safety',
    ),
    Fieldset(
        "Charte du balisage temporaire",
        'markup_convention',
    ),
    Fieldset(
        "Type de balisage utilisé",
        'chalk_markup',
        'rubalise_markup',
        'bio_rubalise_markup',
        'paneling_markup',
        'ground_markup',
        'sticker_markup',
        'lime_markup',
        'plaster_markup',
        'confetti_markup',
        'paint_markup',
        'acrylic_markup',
        'withdrawn_markup',
    ),
    Fieldset(
        "Évaluation d'incidences Natura2000",
        'big_budget',
        'lucrative',
        'big_title',
        'motor_on_natura2000',
        'motor_on_closed_road',
        'approval_request',
    )
)

ANM_FIELDS = copy.deepcopy(FIELDS)
ANM_FIELDS[0].append('multisport_federation')
ANM_FIELDS[2].pop(6)
ANM_FIELDS[2].append('signalers_list')
ANM_FIELDS[5].pop(5)
ANM_FIELDS[5].pop(4)
ANM_FIELDS[5].pop(3)

DNM_FIELDS = copy.deepcopy(FIELDS)
DNM_FIELDS[0].extend(['grouped_traffic', 'support_vehicles_number'])
DNM_FIELDS[2].pop(6)
DNM_FIELDS[5].pop(5)
DNM_FIELDS[5].pop(4)
DNM_FIELDS[5].pop(3)
DNM_FIELDS[5].pop(2)

MC_FIELDS = copy.deepcopy(FIELDS)
MC_FIELDS[0].extend(['vehicles', 'big_concentration'])
MC_FIELDS[2].append('cartography')
MC_FIELDS[5].pop(5)
MC_FIELDS[5].pop(4)

ME_FIELDS = copy.deepcopy(FIELDS)
ME_FIELDS[0].append('vehicles')
ME_FIELDS[2].extend(['delegate_federation_agr', 'public_zone_map',
                     'commissioners', 'tech_organisateur_certificate',
                     'hourly_itinerary', 'cartography'])
ME_FIELDS[5].pop(5)
ME_FIELDS[5].pop(4)

MR_FIELDS = copy.deepcopy(FIELDS)
MR_FIELDS[0].append('support')
MR_FIELDS[2].extend(['mass_map', 'cartography'])
MR_FIELDS[5].pop(5)
MR_FIELDS[5].pop(3)


class ManifestationForm(GenericForm):
    activite = ModelChoiceField(
        Activite.objects,
        widget=ActiviteChoiceWidget,
        label="Activité",
    )

    def __init__(self, *args, **kwargs):
        self.routes = kwargs.pop('routes', None)
        super(ManifestationForm, self).__init__(*args, **kwargs)
        self.fields['openrunner_route'].widget.choices = self.routes
        self.fields['activite'].widget.form_instance = self
        # Traité dans events.models ready_for_instruction de Manifestation
        #
        # if not settings.DEPARTMENT_SETTINGS['OPENRUNNER_MAP_OPTIONAL']:
        #     self.fields['openrunner_route'].required = True
        self.helper.form_tag = False

    def clean_openrunner_route(self):
        openrunner_route = self.cleaned_data['openrunner_route']
        # Here we transform the route list into a suite of integers
        # seraparated by "," in order to match CommaSeparatedInteger
        # field where it is stored
        return ','.join([route.strip("u'") for route in openrunner_route.strip("[]").split(', ')])

    def clean(self):
        cleaned_data = super(ManifestationForm, self).clean()
        begin_date = cleaned_data.get('begin_date')
        end_date = cleaned_data.get('end_date')

        if begin_date and end_date:
            if begin_date >= end_date:
                message = "La date de fin de la manifestation doit être supérieure à la date de début"
                self._errors["end_date"] = self.error_class([message])

        return cleaned_data


WIDGETS = {
    'begin_date': DateTimePicker(
        options={"format": "DD/MM/YYYY HH:mm",
                 "pickSeconds": False}),
    'end_date': DateTimePicker(
        options={"format": "DD/MM/YYYY HH:mm",
                 "pickSeconds": False}),
    'openrunner_route': SelectMultiple(
        attrs={"size": 10}),
    'other_departments_crossed': SelectMultiple(
        attrs={"size": 10}),
    'crossed_cities': SelectMultiple(
        attrs={"size": 15}),
}


class AutorisationNMForm(ManifestationForm):
    discipline = ModelChoiceField(label="Disciplines", required=False, queryset=Discipline.objects.filter(motorise=False))

    class Meta:
        model = AutorisationNM
        exclude = ('structure', 'approval_request',
                   'motor_on_closed_road', 'motor_on_natura2000',
                   'rounds_safety')
        widgets = WIDGETS

    def __init__(self, *args, **kwargs):
        super(AutorisationNMForm, self).__init__(*args, **kwargs)
        self.helper.layout = ANM_FIELDS


class DeclarationNMForm(ManifestationForm):
    discipline = ModelChoiceField(label="Disciplines", required=False, queryset=Discipline.objects.filter(motorise=False))

    class Meta:
        model = DeclarationNM
        exclude = ('structure', 'big_title', 'approval_request',
                   'motor_on_closed_road', 'motor_on_natura2000',
                   'rounds_safety')
        widgets = WIDGETS
        help_texts = {
            'organisateur_commitment': "un modèle de cette déclaration est disponible <a target='_blank' href='/"
                                       "engagement_organisateur/declaration/'>ici</a>"
        }

    def __init__(self, *args, **kwargs):
        super(DeclarationNMForm, self).__init__(*args, **kwargs)
        self.helper.layout = DNM_FIELDS


class MotorizedConcentrationForm(ManifestationForm):
    discipline = ModelChoiceField(label="Disciplines", required=False, queryset=Discipline.objects.filter(motorise=True))

    class Meta:
        model = MotorizedConcentration
        exclude = ('structure', 'approval_request',
                   'motor_on_closed_road')
        widgets = WIDGETS
        help_texts = {
            'organisateur_commitment': "un modèle de cette déclaration est disponible : <br />- <a target='_blank' "
                                       "href='/engagement_organisateur/declaration/'>ici</a> pour les concentrations "
                                       "de moins de 200 autos ou 400 véhicules terrestres à moteur de 2 ou 4 roues "
                                       "<br />- <a target='_blank' href='/engagement_organisateur/"
                                       "autorisation/'>ici</a> pour les concentrations de plus de 200 autos ou 400 "
                                       "véhicules terrestres à moteur de 2 ou 4 roues"
        }

    def __init__(self, *args, **kwargs):
        super(MotorizedConcentrationForm, self).__init__(*args, **kwargs)
        self.helper.layout = MC_FIELDS


class MotorizedEventForm(ManifestationForm):
    discipline = ModelChoiceField(label="Disciplines", required=False, queryset=Discipline.objects.filter(motorise=True))

    class Meta:
        model = MotorizedEvent
        exclude = ('structure', 'approval_request',
                   'motor_on_closed_road')
        widgets = WIDGETS

    def __init__(self, *args, **kwargs):
        super(MotorizedEventForm, self).__init__(*args, **kwargs)
        self.helper.layout = ME_FIELDS


class MotorizedRaceForm(ManifestationForm):
    discipline = ModelChoiceField(label="Disciplines", required=False, queryset=Discipline.objects.filter(motorise=True))

    class Meta:
        model = MotorizedRace
        exclude = ('structure', 'approval_request',
                   'motor_on_natura2000')
        widgets = WIDGETS

    def __init__(self, *args, **kwargs):
        super(MotorizedRaceForm, self).__init__(*args, **kwargs)
        self.helper.layout = MR_FIELDS


class DocumentComplementaireRequestForm(GenericForm):

    class Meta:
        model = DocumentComplementaire
        fields = ('desired_information', )

    def __init__(self, *args, **kwargs):
        super(DocumentComplementaireRequestForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', "Envoyer la Demande d'Information"))


class DocumentComplementaireProvideForm(GenericForm):

    class Meta:
        model = DocumentComplementaire
        fields = ('attached_document', )

    def __init__(self, *args, **kwargs):
        super(DocumentComplementaireProvideForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', "Fournir le Document"))

    def clean_attached_document(self):
        data = self.cleaned_data['attached_document']
        if data is None:
            raise forms.ValidationError("La sélection d'un document est obligatoire")
        return data
