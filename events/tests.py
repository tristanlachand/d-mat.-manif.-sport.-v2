# coding: utf-8
from __future__ import unicode_literals

import calendar
import datetime
import time
from urllib.parse import urlencode

from django.conf import settings
from django.contrib.auth.hashers import (is_password_usable,
                                         check_password,
                                         make_password,
                                         identify_hasher,
                                         get_hashers)
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils.timezone import utc

from administrative_division.factories import DepartementFactory

from authorizations.factories import EventAuthorizationFactory

from contacts.factories import AdresseFactory

from declarations.factories import EventDeclarationFactory

from evaluations.factories import RNREvaluationFactory
from evaluations.factories import Natura2000EvaluationFactory

from notifications.models import Action

from .factories import OrganisateurFactory, StructureFactory, StructureTypeFactory
from .factories import CompteOpenRunnerFactory
from .factories import AutorisationNMFactory
from .factories import DeclarationNMFactory
from .factories import MotorizedConcentrationFactory
from .factories import MotorizedEventFactory
from .factories import MotorizedRaceFactory


class TestDrupalHashPass(TestCase):

    def setUp(self):
        get_hashers()

    def test_drupal(self):
        encoded = make_password('formationddcs', 'wX89/crt', 'drupal_sha512')
        self.assertEqual(
            encoded,
            'drupal_sha512$32768$wX89/crt$w46C97GO3Qd/Zkx/qE5BBrtVuj687bmeTIC9b56tpGC'  # noqa
        )
        self.assertTrue(is_password_usable(encoded))
        self.assertTrue(check_password('formationddcs', encoded))
        self.assertFalse(check_password('formationddcse', encoded))
        self.assertEqual(identify_hasher(encoded).algorithm, "drupal_sha512")
        # Blank passwords
        blank_encoded = make_password('', 'seasalt', 'drupal_sha512')
        self.assertTrue(blank_encoded.startswith('drupal_sha512$'))
        self.assertTrue(is_password_usable(blank_encoded))
        self.assertTrue(check_password('', blank_encoded))
        self.assertFalse(check_password(' ', blank_encoded))


class OrganisateurMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the name of the associated user
        '''
        organisateur = OrganisateurFactory.build()
        self.assertEqual(organisateur.__str__(), organisateur.user.username)

    def test_get_department(self):
        organisateur = OrganisateurFactory.create()
        _structure = StructureFactory.create(organisateur=organisateur)
        self.assertEqual(
            organisateur.get_department(),
            organisateur.structure.commune.arrondissement.departement.name
        )

    def test_hash_openrunner_secret_key(self):
        settings.OPENRUNNER_SECRET_KEY = 'A0E0R0R0R0R0R034'
        organisateur = OrganisateurFactory.build()
        self.assertEqual(organisateur.hash_openrunner_secret_key(),
                         '2dc072c3dc2140006795fa8a7001a9bd')

    def test_build_parameters(self):
        settings.OPENRUNNER_SECRET_KEY = 'A0E0R0R0R0R0R034'
        organisateur = OrganisateurFactory.create()
        structure = StructureFactory.create(organisateur=organisateur)
        _address = AdresseFactory.create(
            content_type=ContentType.objects.get_for_model(structure),
            object_id=structure.id
        )
        self.assertEqual(
            organisateur.build_parameters(),
            urlencode({'cddcs': organisateur.encrypt_for_openrunner(
                '2dc072c3dc2140006795fa8a7001a9bd',
                ','.join([organisateur.user.email,
                          organisateur.user.username,
                          organisateur.get_department()])
            )})
        )

    def test_get_compte_openrunner_creation_url(self):
        settings.OPENRUNNER_SECRET_KEY = 'A0E0R0R0R0R0R034'
        organisateur = OrganisateurFactory.create()
        structure = StructureFactory.create(organisateur=organisateur)
        _address = AdresseFactory.create(
            content_type=ContentType.objects.get_for_model(structure),
            object_id=structure.id
        )
        self.assertEqual(
            organisateur.get_compte_openrunner_creation_url(),
            ''.join([settings.OPENRUNNER_HOST,
                     settings.OPENRUNNER_CREATE_ACCOUNT,
                     organisateur.build_parameters(),
                     '&t=',
                     str(calendar.timegm(time.gmtime()))])
        )

    def test_create_compte_openrunner_failure(self):
        '''
        create_compte_openrunner() should create an openrunneraccount
        if OPENRUNNER_HOST is down, should add the related error_message
        and mark the account with created=False
        '''
        settings.OPENRUNNER_HOST = 'http://ddcs.openrunnerrrrr.com'
        organisateur = OrganisateurFactory.create()
        organisateur.create_compte_openrunner()
        self.assertEqual(organisateur.compteopenrunner.error_message,
                         'ddcs.openrunner.com down')
        self.assertFalse(organisateur.compteopenrunner.created)

    def test_process_openrunner_positive_response(self):
        '''
        process_compte_openrunner() when response code is positive
        should create the account and associates a password
        '''
        organisateur = OrganisateurFactory.create()
        CompteOpenRunner = CompteOpenRunnerFactory.create(organisateur=organisateur)
        organisateur.process_openrunner_response(
            'ISO-8859-1',
            '1:nJTGyJ2YyA==',
            organisateur.compteopenrunner,
            True,
        )
        self.assertEqual(CompteOpenRunner.password, 'j0c\x98ffe')
        self.assertEqual(CompteOpenRunner.error_message, '')
        self.assertTrue(CompteOpenRunner.created)

    def test_process_openrunner_negative_response(self):
        '''
        process_compte_openrunner() when response code is positive
        should create the account and associates a password
        '''
        organisateur = OrganisateurFactory.create()
        CompteOpenRunner = CompteOpenRunnerFactory.create(organisateur=organisateur)
        organisateur.process_openrunner_response(
            'ISO-8859-1',
            '0:Login duplicated',
            organisateur.compteopenrunner,
            True,
        )
        self.assertEqual(CompteOpenRunner.password, '')
        self.assertEqual(CompteOpenRunner.error_message,
                         'Login duplicated')
        self.assertFalse(CompteOpenRunner.created)

    def test_get_openrunner_routes_without_account(self):
        organisateur = OrganisateurFactory.build()
        self.assertEqual(organisateur.get_openrunner_routes(), [])


class CompteOpenRunnerMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the name of the associated promoter
        '''
        compte_openrunner = CompteOpenRunnerFactory.build()
        self.assertEqual(compte_openrunner.__str__(),
                         compte_openrunner.organisateur.user.username)

    def test_update_error_message_if_not_created(self):
        '''
        update_error_message() should do nothing if created is False
        (meaning openrunner is allready existing)
        '''
        compte_openrunner = CompteOpenRunnerFactory.create()
        created = False
        compte_openrunner.update_error_message(created, 'plop')
        self.assertEqual(compte_openrunner.error_message, '')

    def test_update_error_message_if_created(self):
        '''
        update_error_message() should update error message and creation date
        '''
        compte_openrunner = CompteOpenRunnerFactory.create()
        created = True
        compte_openrunner.update_error_message(created, 'plop')
        self.assertEqual(compte_openrunner.error_message, 'plop')
        self.assertFalse(compte_openrunner.created)

    def test_set_password(self):
        compte_openrunner = CompteOpenRunnerFactory.create()
        compte_openrunner.set_password('password')
        self.assertEqual(compte_openrunner.error_message, '')
        self.assertEqual(compte_openrunner.password, 'password')
        self.assertTrue(compte_openrunner.created)

    def test_get_routes_request_url(self):
        account = CompteOpenRunnerFactory.build()
        self.assertEqual(account.get_routes_request_url(),
                         ''.join([settings.OPENRUNNER_HOST,
                                  settings.OPENRUNNER_GET_ROUTES,
                                  account.organisateur.user.username]))

    def test_get_routes(self):
        settings.OPENRUNNER_HOST = 'http://ddcs.openrunnerrrrr.com'
        account = CompteOpenRunnerFactory.build()
        self.assertEqual(account.get_routes(), {})


class StructureTypeMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the type of structure
        '''
        structure_type = StructureTypeFactory.build()
        self.assertEqual(structure_type.__str__(), 'Association')


class StructureMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the name of the associated structure
        '''
        structure = StructureFactory.build(name='GRS')
        self.assertEqual(structure.__str__(), structure.name.title())


class AutorisationNMMethodTests(TestCase):

    def test_get_absolute_url(self):
        '''
        get_absolute_url() should point to the event detail's page
        '''
        manifestation = AutorisationNMFactory.build(pk=3)
        self.assertEqual(manifestation.get_absolute_url(),
                         '/AutorisationNM/3/')

    def test_display_natura2000_eval_panel(self):
        manifestation = AutorisationNMFactory.build(lucrative=True)
        self.assertTrue(manifestation.display_natura2000_eval_panel())
        manifestation3 = AutorisationNMFactory.build(big_budget=True)
        self.assertTrue(manifestation3.display_natura2000_eval_panel())
        manifestation4 = AutorisationNMFactory.build(big_title=True)
        self.assertTrue(manifestation4.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        manifestation = AutorisationNMFactory.build()
        self.assertFalse(manifestation.display_natura2000_eval_panel())

    def test_log_manifestation_creation(self):
        manifestation = AutorisationNMFactory.create()
        action = Action.objects.last()
        self.assertEqual(action.user, manifestation.structure.organisateur.user)
        self.assertEqual(action.manifestation, manifestation.manifestation_ptr)
        self.assertEqual(action.action, "description de la manifestation")
        # when event is updated,
        # no log action is created
        manifestation.save()
        action2 = Action.objects.last()
        self.assertEqual(action, action2)

    def test_legal_delay(self):
        manifestation = AutorisationNMFactory.create()
        self.assertEqual(manifestation.legal_delay(), 60)

    def test_legal_delay_2(self):
        manifestation = AutorisationNMFactory.create()
        departement = DepartementFactory.create()
        manifestation.other_departments_crossed.add(DepartementFactory.create())
        self.assertEqual(manifestation.legal_delay(), 90)

    def test_get_limit_date(self):
        manifestation = AutorisationNMFactory.create(
            begin_date=datetime.datetime.utcnow().replace(tzinfo=utc)
        )
        self.assertEqual(
            manifestation.get_limit_date(),
            (manifestation.begin_date - datetime.timedelta(days=60)).replace(
                hour=23,
                minute=59
            )
        )

    def test_delay_exceeded(self):
        manifestation = AutorisationNMFactory.create(
            begin_date=(datetime.datetime.utcnow().replace(tzinfo=utc) +
                        datetime.timedelta(days=59))
        )
        self.assertTrue(manifestation.delay_exceeded())

    def test_delay_not_exceeded(self):
        manifestation = AutorisationNMFactory.create(
            begin_date=(datetime.datetime.utcnow().replace(tzinfo=utc) +
                        datetime.timedelta(days=61))
        )
        self.assertFalse(manifestation.delay_exceeded())

    def test_not_two_weeks_left(self):
        manifestation = AutorisationNMFactory.create(
            begin_date=(datetime.datetime.today() + datetime.timedelta(days=60) + datetime.timedelta(weeks=2))
        )
        self.assertFalse(manifestation.two_weeks_left())

    def test_two_weeks_left(self):
        manifestation = AutorisationNMFactory.create(
            begin_date=(datetime.datetime.utcnow().replace(tzinfo=utc) +
                        datetime.timedelta(days=60) +
                        datetime.timedelta(weeks=1))
        )
        self.assertTrue(manifestation.two_weeks_left())

    def test_get_final_breadcrumb_not_sent(self):
        manifestation = AutorisationNMFactory.build()
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        manifestation = AutorisationNMFactory.create()
        _authorization = EventAuthorizationFactory.create(manifestation=manifestation)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)


class DeclarationNMMethodTests(TestCase):

    def test_str_(self):
        '''
        __str__() should return the name of the associated event
        '''
        manifestation = DeclarationNMFactory.build(
            name='Course des 4 colines'
        )
        self.assertEqual(manifestation.__str__(), manifestation.name.title())

    def test_get_absolute_url(self):
        '''
        get_absolute_url() should point to the event detail's page
        '''
        manifestation = DeclarationNMFactory.build(pk=2)
        self.assertEqual(manifestation.get_absolute_url(),
                         '/DeclarationNM/2/')

    def test_display_rnr_eval_panel(self):
        manifestation = DeclarationNMFactory.build(is_on_rnr=True)
        self.assertTrue(manifestation.display_rnr_eval_panel())

    def test_not_display_rnr_eval_panel(self):
        manifestation = DeclarationNMFactory.build(is_on_rnr=False)
        self.assertFalse(manifestation.display_rnr_eval_panel())

    def test_display_natura2000_eval_panel(self):
        manifestation = DeclarationNMFactory.build(lucrative=True)
        self.assertTrue(manifestation.display_natura2000_eval_panel())
        manifestation3 = DeclarationNMFactory.build(big_budget=True)
        self.assertTrue(manifestation3.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        manifestation = DeclarationNMFactory.build()
        self.assertFalse(manifestation.display_natura2000_eval_panel())

    def test_legal_delay(self):
        manifestation = DeclarationNMFactory.build()
        self.assertEqual(manifestation.legal_delay(), 30)

    def test_get_final_breadcrumb_not_sent(self):
        manifestation = DeclarationNMFactory.build()
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        manifestation = DeclarationNMFactory.create()
        _declaration = EventDeclarationFactory.create(manifestation=manifestation)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)

    def test_get_breadcrumbs(self):
        manifestation = DeclarationNMFactory.build()
        self.assertEqual(len(manifestation.get_breadcrumbs()), 2)

    def test_get_breadcrumbs_full(self):
        manifestation = DeclarationNMFactory.create()
        _rnr = RNREvaluationFactory.create(manifestation=manifestation)
        _natura2000 = Natura2000EvaluationFactory.create(manifestation=manifestation)
        self.assertEqual(len(manifestation.get_breadcrumbs()), 4)

    def test_get_breadcrumbs_full_2(self):
        manifestation = DeclarationNMFactory.create(is_on_rnr=True,
                                                        lucrative=True)
        self.assertEqual(len(manifestation.get_breadcrumbs()), 4)


class MotorizedConcentrationMethodTests(TestCase):

    def test_get_absolute_url(self):
        '''
        get_absolute_url() should point to the event detail's page
        '''
        manifestation = MotorizedConcentrationFactory.build(pk=4)
        self.assertEqual(manifestation.get_absolute_url(),
                         '/motorizedconcentrations/4/')

    def test_display_natura2000_eval_panel(self):
        manifestation = MotorizedConcentrationFactory.build(lucrative=True)
        self.assertTrue(manifestation.display_natura2000_eval_panel())
        manifestation3 = MotorizedConcentrationFactory.build(big_budget=True)
        self.assertTrue(manifestation3.display_natura2000_eval_panel())
        manifestation4 = MotorizedConcentrationFactory.build(big_title=True)
        self.assertTrue(manifestation4.display_natura2000_eval_panel())
        manifestation5 = MotorizedConcentrationFactory.build(motor_on_natura2000=True)
        self.assertTrue(manifestation5.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        manifestation = MotorizedConcentrationFactory.build()
        self.assertFalse(manifestation.display_natura2000_eval_panel())

    def test_legal_delay_for_authorized(self):
        manifestation = MotorizedConcentrationFactory.build(big_concentration=True)
        self.assertEqual(manifestation.legal_delay(), 90)

    def test_legal_delay_for_declared(self):
        manifestation = MotorizedConcentrationFactory.build(big_concentration=False)
        self.assertEqual(manifestation.legal_delay(), 60)

    def test_get_final_breadcrumb_not_sent(self):
        manifestation = MotorizedConcentrationFactory.build(big_concentration=True)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_not_sent_2(self):
        manifestation = MotorizedConcentrationFactory.build(big_concentration=False)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        manifestation = MotorizedConcentrationFactory.create(big_concentration=True)
        _authorization = EventAuthorizationFactory.create(manifestation=manifestation)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)

    def test_get_final_breadcrumb_sent_2(self):
        manifestation = MotorizedConcentrationFactory.create(big_concentration=False)
        _declaration = EventDeclarationFactory.create(manifestation=manifestation)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)


class MotorizedEventMethodTests(TestCase):

    def test_get_absolute_url(self):
        '''
        get_absolute_url() should point to the event detail's page
        '''
        manifestation = MotorizedEventFactory.build(pk=4)
        self.assertEqual(manifestation.get_absolute_url(),
                         '/motorizedevents/4/')

    def test_display_natura2000_eval_panel(self):
        manifestation = MotorizedEventFactory.build(lucrative=True)
        self.assertTrue(manifestation.display_natura2000_eval_panel())
        manifestation3 = MotorizedEventFactory.build(big_budget=True)
        self.assertTrue(manifestation3.display_natura2000_eval_panel())
        manifestation4 = MotorizedEventFactory.build(big_title=True)
        self.assertTrue(manifestation4.display_natura2000_eval_panel())
        manifestation5 = MotorizedEventFactory.build(motor_on_natura2000=True)
        self.assertTrue(manifestation5.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        manifestation = MotorizedEventFactory.build()
        self.assertFalse(manifestation.display_natura2000_eval_panel())

    def test_legal_delay(self):
        manifestation = MotorizedEventFactory.build()
        self.assertEqual(manifestation.legal_delay(), 90)

    def test_get_final_breadcrumb_not_sent(self):
        manifestation = MotorizedEventFactory.build()
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        manifestation = MotorizedEventFactory.create()
        _authorization = EventAuthorizationFactory.create(manifestation=manifestation)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)


class MotorizedRaceMethodTests(TestCase):

    def test_get_absolute_url(self):
        '''
        get_absolute_url() should point to the event detail's page
        '''
        manifestation = MotorizedRaceFactory.build(pk=5)
        self.assertEqual(manifestation.get_absolute_url(),
                         '/motorizedraces/5/')

    def test_display_natura2000_eval_panel(self):
        manifestation = MotorizedRaceFactory.build(lucrative=True)
        self.assertTrue(manifestation.display_natura2000_eval_panel())
        manifestation2 = MotorizedRaceFactory.build(approval_request=True)
        self.assertTrue(manifestation2.display_natura2000_eval_panel())
        manifestation3 = MotorizedRaceFactory.build(big_budget=True)
        self.assertTrue(manifestation3.display_natura2000_eval_panel())
        manifestation4 = MotorizedRaceFactory.build(big_title=True)
        self.assertTrue(manifestation4.display_natura2000_eval_panel())
        manifestation5 = MotorizedRaceFactory.build(motor_on_closed_road=True)
        self.assertTrue(manifestation5.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        manifestation = MotorizedRaceFactory.build()
        self.assertFalse(manifestation.display_natura2000_eval_panel())

    def test_legal_delay(self):
        manifestation = MotorizedRaceFactory.build()
        self.assertEqual(manifestation.legal_delay(), 90)

    def test_get_final_breadcrumb_not_sent(self):
        manifestation = MotorizedRaceFactory.build()
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        manifestation = MotorizedRaceFactory.create()
        _authorization = EventAuthorizationFactory.create(manifestation=manifestation)
        print(list(filter(lambda x: x.startswith('rnr'), dir(manifestation))))
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)


class ManifestationViewTests(TestCase):

    def test_index(self):
        response = self.client.get(reverse('events:home_page'))
        self.assertEqual(response.status_code, 200)
