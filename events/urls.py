# coding: utf-8
from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import HomePage
from .views import Dashboard
from .views import Calendar
from .views import FilteredCalendar
from .views import StructureCreate
from .views import CreateRoute
from .views import ManifestationChoose
from .views import MotorizedEventChoose
from .views import NonMotorizedEventChoose
from .views import DeclarationNMChoose
from .views import PublicRoadMotorizedEventChoose
from .views import ManifestationPublicDetail

from .views import DeclarationNMCreate
from .views import DeclarationNMDetail
from .views import DeclarationNMUpdate

from .views import AutorisationNMCreate
from .views import AutorisationNMDetail
from .views import AutorisationNMUpdate

from .views import MotorizedConcentrationCreate
from .views import MotorizedConcentrationDetail
from .views import MotorizedConcentrationUpdate

from .views import MotorizedEventCreate
from .views import MotorizedEventDetail
from .views import MotorizedEventUpdate

from .views import MotorizedRaceCreate
from .views import MotorizedRaceDetail
from .views import MotorizedRaceUpdate

from .views import DocumentComplementaireRequestView
from .views import DocumentComplementaireProvideView

from .views import MailToOrganisateursView
from .views import MailToTownHallsView

from sports.views import activites_par_discipline

urlpatterns = [

    url(r'^$', HomePage.as_view(),
        name='home_page'),
    url(r'^mail_to_promoters/$', MailToOrganisateursView.as_view(),
        name='mail_to_promoters'),
    url(r'^mail_to_townhalls/$', MailToTownHallsView.as_view(),
        name='mail_to_townhalls'),
    url(r'^dashboard/$', Dashboard.as_view(),
        name="dashboard"),
    url(r'^calendar/$', Calendar.as_view(),
        name="calendar"),
    url(r'^calendar/(?P<activite>\d+)/$', FilteredCalendar.as_view(),
        name="filtered_calendar"),
    url(r'^structure/add', StructureCreate.as_view(), name='structure_add'),

    url(r'^events/create_route/$', CreateRoute.as_view(),
        name='create_route'),
    url(r'^events/choose/$', ManifestationChoose.as_view(),
        name='manifestation_choose'),
    url(r'^motorizedevents/choose', MotorizedEventChoose.as_view(),
        name="motorizedevent_choose"),
    url(r'^nonmotorizedevents/choose', NonMotorizedEventChoose.as_view(),
        name="nonmotorizedevent_choose"),
    url(r'^DeclarationNM/choose',
        DeclarationNMChoose.as_view(),
        name="declarationnm_choose"),
    url(r'^publicroadmotorizedevents/choose',
        PublicRoadMotorizedEventChoose.as_view(),
        name="publicroadmotorizedevent_choose"),
    url(r'^events/(?P<pk>\d+)/$',
        ManifestationPublicDetail.as_view(),
        name="manifestation_detail"),

    # DeclarationNM urls
    url(r'^DeclarationNM/add/$',
        DeclarationNMCreate.as_view(),
        name="declarationnm_add"),
    url(r'^DeclarationNM/(?P<pk>\d+)/$',
        DeclarationNMDetail.as_view(),
        name="declarationnm_detail"),
    url(r'^DeclarationNM/(?P<pk>\d+)/edit/$',
        DeclarationNMUpdate.as_view(),
        name="declarationnm_update"),

    # AutorisationNM urls
    url(r'^AutorisationNM/add/$',
        AutorisationNMCreate.as_view(),
        name="autorisationnm_add"),
    url(r'^AutorisationNM/(?P<pk>\d+)/$',
        AutorisationNMDetail.as_view(),
        name="autorisationnm_detail"),
    url(r'^AutorisationNM/(?P<pk>\d+)/edit/$',
        AutorisationNMUpdate.as_view(),
        name="autorisationnm_update"),

    # MotorizedConcentration urls
    url(r'^motorizedconcentrations/add/$',
        MotorizedConcentrationCreate.as_view(),
        name="motorizedconcentration_add"),
    url(r'^motorizedconcentrations/(?P<pk>\d+)/$',
        MotorizedConcentrationDetail.as_view(),
        name="motorizedconcentration_detail"),
    url(r'^motorizedconcentrations/(?P<pk>\d+)/edit/$',
        MotorizedConcentrationUpdate.as_view(),
        name="motorizedconcentration_update"),

    # MotorizedEvent urls
    url(r'^motorizedevents/add/$',
        MotorizedEventCreate.as_view(),
        name="motorizedevent_add"),
    url(r'^motorizedevents/(?P<pk>\d+)/$',
        MotorizedEventDetail.as_view(),
        name="motorizedevent_detail"),
    url(r'^motorizedevents/(?P<pk>\d+)/edit/$',
        MotorizedEventUpdate.as_view(),
        name="motorizedevent_update"),

    # MotorizedRace urls
    url(r'^motorizedraces/add/$',
        MotorizedRaceCreate.as_view(),
        name="motorizedrace_add"),
    url(r'^motorizedraces/(?P<pk>\d+)/$',
        MotorizedRaceDetail.as_view(),
        name="motorizedrace_detail"),
    url(r'^motorizedraces/(?P<pk>\d+)/edit/$',
        MotorizedRaceUpdate.as_view(),
        name="motorizedrace_update"),
    url(r'^events/(?P<manifestation_pk>\d+)/additional_data/add$',
        DocumentComplementaireRequestView.as_view(),
        name="additional_data_add"),
    url(r'^additional_data/(?P<pk>\d+)/$',
        DocumentComplementaireProvideView.as_view(),
        name="additional_data_provide"),

    url(r'^activites_par_discipline/$',
        activites_par_discipline)
]
