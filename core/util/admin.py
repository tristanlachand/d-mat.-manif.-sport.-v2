# coding: utf-8
"""
Utilitaire pour les méthodes affichées dans l'admin

Le décorateur set_admin_info(attribute=value, ...) permet de simplifier l'écriture des
attributs de méthodes utilisés dans l'admin. Par exemple :

    def get_ancestor(self):
        return self.ancestor
    get_ancestor.short_description = "Ancêtre"
    get_ancestor.admin_order_field = 'ancestor'

peut être réécrit :

    @set_admin_info(short_desription="Ancêtre", admin_order_field='ancestor')
    def get_ancestor(self):
        return self.ancestor

"""


def set_admin_info(**kwargs):
    """ Définir des attributs à une méthode ou fonction """

    def decorator(func):
        for key in kwargs:
            setattr(func, key, kwargs[key])
        return func

    return decorator
