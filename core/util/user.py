# coding: utf-8
"""
Utilitaires pour les utilisateurs

UserHelper : cette classe permet plusieurs choses :
    - récupérer le type d'agent correspondant au rôle d'un utilisateur
    - récupérer le service attaché à un utilisateur. ex.: s'il est agent GGD, renvoyer son GGD
"""
from operator import itemgetter


class UserHelper:
    """ Utilitaires liés aux utilisateurs """

    # Constantes
    ROLE_TYPES = ['ggdagent', 'edsragent', 'cgdagentlocal', 'brigadeagent', 'codisagent', 'sdisagent', 'compagnieagent', 'cisagent',
                  'ddspagent', 'commissariatagentlocal', 'serviceagent', 'federationagent', 'mairieagent', 'cgagent',
                  'cgsuperieuragent', 'cgserviceagentlocal', 'organisateur', 'instructeur']
    ROLE_CHOICES = [['ggdagent', u"Agent GGD"], ['edsragent', u"Agent EDSR"], ['cgdagentlocal', u"Agent CGD"], ['brigadeagent', u"Agent brigade"],
                    ['codisagent', u"Agent CODIS"], ['sdisagent', u"Agent SDIS"], ['compagnieagent', u"Agent compagnie"], ['cisagent', u"Agent CIS"],
                    ['ddspagent', u"Agent DDSP"], ['commissariatagentlocal', u"Agent commissariat"], ['serviceagent', u"Agent service"],
                    ['federationagent', u"Agent fédération"], ['mairieagent', u"Agent mairie"], ['cgagent', u"Agent CG"],
                    ['cgsuperieuragent', u"Supérieur CG"], ['cgserviceagentlocal', u"Agent service CG"],
                    ['organisateur', "Organisateur"], ['instructeur', "Instructeur"]]
    SERVICES = {'ggdagent': 'ggd', 'edsragent': 'edsr', 'cgdagentlocal': 'cgd', 'brigadeagent': 'brigade', 'codisagent': 'codis',
                'sdis': 'sdis', 'compagnieagent': 'compagnie', 'cisagent': 'cis', 'ddspagent': 'ddsp', 'commissariatagentlocal': 'commissariat',
                'serviceagent': 'service', 'federationagent': 'federation', 'mairieagent': 'commune', 'cgagent': 'cg', 'cgsuperieuragent': 'cg',
                'cgserviceagentlocal': 'cg_service', 'instructeur': 'prefecture'}
    ROLE_CHOICES = sorted(ROLE_CHOICES, key=itemgetter(1))

    # Fonctions
    @staticmethod
    def get_role_name(user):
        """ Renvoie la chaîne correspondant au rôle de l'utilisateur """
        for role in UserHelper.ROLE_TYPES:
            # Tester les rôles dont le modèle est lié directement à User
            try:
                if getattr(user, role) is not None:
                    return role
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agent
            try:
                if getattr(user.agent, role) is not None:
                    return role
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agentlocal
            try:
                if getattr(user.agentlocal, role) is not None:
                    return role
            except AttributeError:
                pass
        return None

    @staticmethod
    def get_role_instance(user):
        """ Renvoyer l'objet de rôle pour l'utilisateur """
        for role in UserHelper.ROLE_TYPES:
            # Tester les rôles dont le modèle est lié directement à User
            try:
                if getattr(user, role) is not None:
                    return getattr(user, role)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agent
            try:
                if getattr(user.agent, role) is not None:
                    return getattr(user.agent, role)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agentlocal
            try:
                if getattr(user.agentlocal, role) is not None:
                    return getattr(user.agentlocal, role)
            except AttributeError:
                pass
        return None

    @staticmethod
    def get_service_instance(user):
        """ Renvoyer l'instance de service correspondant à l'utilisateur """
        role_instance = UserHelper.get_role_instance(user)
        role_field = UserHelper.get_role_name(user)
        service_field = UserHelper.SERVICES.get(role_field, None)
        if service_field is not None and role_instance is not None:
            return getattr(role_instance, service_field)
        else:
            return None

