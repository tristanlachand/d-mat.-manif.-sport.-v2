# coding: utf-8
"""
Envoi de notifications par email, ou consignées dans le système.
À remplacer par django-postman.
"""
from __future__ import unicode_literals

from django import forms
from django.conf import settings
from django.core.mail import send_mail

from crispy_forms.helper import FormHelper


def add_notification_entry(agents, subject, content_object, manifestation,
                           institutional=None):
    mail_subject = '{sender} : {subject} {liaison} \"{manifestation}\"'.format(
        sender=content_object,
        subject=subject,
        liaison="Pour manifestation",
        manifestation=manifestation.name,
    )
    # Todo : à tester
    mail_footer = "".join([(
        "\n\n=== NE PAS RÉPONDRE À CET E-MAIL ===\n"
        "Pour effectuer la suite de votre démarche : \n"
        "- connectez vous sur la plateforme Manifestation Sportive\n"
        "- rendez vous sur votre tableau de bord\n"), settings.MAIN_URL])
    mail_message = "".join([mail_subject, mail_footer])
    if institutional:
        send_mail(mail_subject,
                  mail_message,
                  settings.DEFAULT_FROM_EMAIL,
                  [institutional.email])
    for agent in agents:
        agent.user.notifications.create(
            manifestation=manifestation,
            subject=subject,
            content_object=content_object,
        )
        send_mail(mail_subject,
                  mail_message,
                  settings.DEFAULT_FROM_EMAIL,
                  [agent.user.email])


def add_log_entry(agents, action, manifestation):
    for agent in agents:
        agent.user.actions.create(
            manifestation=manifestation,
            action=action,
        )
