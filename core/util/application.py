# coding: utf-8
"""
Fonctions et utilitaires liés à la configuration de l'application.
Permet de récupérer les paramètres de l'instance en cours.
"""
import re

from django.conf import settings


def get_instance_settings(name=None):
    """
    Renvoyer les paramètres d'instance ou une entrée de ces paramètres

    :param name: nom de l'entrée dans DEPARTMENT_SETTINGS, ou tout le dictionnaire si None
    """
    settings_dict = settings.DEPARTMENT_SETTINGS
    return settings_dict if name is None else settings_dict.get(name, None)


def get_current_subdomain(request):
    """
    Renvoyer le nom du sous-domaine actuel
    """
    request.domain = request.META['HTTP_HOST']
    request.subdomain = ''
    parts = request.domain.split('.')
    # accepte les nnd de la forme sub.dom.ext ou sub.localhost:8000
    if len(parts) == 3 or (re.match("^localhost", parts[-1]) and len(parts) == 2):
        request.subdomain = parts[0]
        request.domain = '.'.join(parts[1:])
    return request.subdomain


# Choix de filtrage pour les modèles
FILTER_DEPARTEMENT_CHOICES = {'departement__name': get_instance_settings('DEPARTMENT_NUMBER')}
FILTER_ARRONDISSEMENT_CHOICES = {'arrondissement__departement__name': get_instance_settings('DEPARTMENT_NUMBER')}
FILTER_COMMUNE_CHOICES = {'commune_arrondissement__departement__name': get_instance_settings('DEPARTMENT_NUMBER')}
