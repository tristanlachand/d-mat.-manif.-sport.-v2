# coding: utf-8
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db import transaction
from django.conf import settings
from allauth.account.models import EmailAddress


class Command(BaseCommand):
    """ Valide tous les comptes utilisateur, ne demandant ainsi pas de validation par email """
    args = ''
    help = 'Indique que tous les comptes sont vérifiés'

    def handle(self, *args, **options):
        """ Exécuter la commande """
        if settings.DEBUG:
            count = User.objects.count()
            EmailAddress.objects.all().delete()
            failed = 0
            for user in User.objects.all().distinct():
                try:
                    EmailAddress.objects.create(user=user, email=user.email, verified=True, primary=False)
                except:
                    failed += 1
            print("{0} emails d'utilisateurs ont été validés, avec {1} erreurs.".format(count, failed))
        else:
            print("Échec : Par sécurité, vous ne pouvez lancer cette commande qu'en mode DEBUG.")
