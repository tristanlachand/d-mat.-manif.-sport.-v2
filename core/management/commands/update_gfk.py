# coding: utf-8
from optparse import make_option
from traceback import print_exc, print_tb

from django.core.management.base import BaseCommand
from django.db import transaction
from django.conf import settings

from contacts.models import Contact, Adresse
from core.util.application import get_instance_settings
from notifications.models import Notification


class Command(BaseCommand):
    """ Met à jour toutes les clés génériques étrangères avec les bons Content Types """
    args = ''
    help = 'Indique que tous les comptes sont vérifiés'
    option_list = BaseCommand.option_list + (
        make_option('--test', '-t', '--check', action='store_true', dest='check', default=False,
                    help='Vérifier seulement que tous les objets sont dans un état correct.'),
    )
    if get_instance_settings('DEPARTMENT_NUMBER') == '42':
        CT_MATCH = {12: 126, 16: 154, 17: 155, 18: 156, 19: 117, 20: 118, 21: 119, 22: 120, 23: 122, 25: 114, 27: 108,
                    30: 123, 31: 110, 34: 124, 35: 111, 38: 112, 42: 106, 44: 107, 46: 113, 48: 137, 50: 139, 51: 140,
                    58: 150, 59: 151, 60: 152, 61: 153, 63: 141, 64: 142, 65: 143, 68: 144, 69: 146, 70: 147, 74: 136,
                    79: 127, 80: 129, 81: 134, 82: 133, 83: 130, 84: 135, 85: 132, 86: 131, 89: 138, 92: 157, 93: 158,
                    94: 160, 95: 161, 96: 162, 97: 121, 98: 115, 99: 116, 103: 125, 104: 145}
        CT_CLEAR = {54, 55, 56, 57}
    elif settings.DEPARTMENT_SETTINGS['DEPARTMENT_NUMBER'] == '78' or settings.DEPARTMENT_SETTINGS['DEPARTMENT_NUMBER'] == '972':
        CT_MATCH = {11: 112, 14: 103, 15: 104, 16: 105, 17: 106, 18: 107, 19: 108, 21: 100, 23: 94, 24: 101, 25: 102,
                    28: 109, 29: 96, 34: 110, 35: 97, 36: 111, 40: 98, 44: 92, 46: 93, 48: 99, 49: 113, 50: 116,
                    51: 124, 52: 123, 53: 117, 54: 125, 55: 121, 56: 119, 58: 130, 59: 132, 60: 133, 62: 135, 65: 147,
                    66: 148, 67: 149, 68: 150, 70: 151, 71: 152, 72: 153, 74: 137, 75: 138, 78: 139, 79: 142, 80: 143,
                    81: 144, 85: 129, 86: 131, 87: 154, 88: 155, 89: 157, 90: 158, 91: 159}
        CT_CLEAR = {}
    else:
        CT_MATCH = {}
        CT_CLEAR = {}

    # Rien pour 73, 72, 71, 67, 24, 39, 15, 14

    def handle(self, *args, **options):
        """ Exécuter la commande """
        check = options.get('check', False)
        # Si la vérification n'est pas demandée
        if not check:
            with transaction.atomic():
                updated = 0
                for model in [Contact, Adresse, Notification]:
                    for item in model.objects.all():
                        if item.content_type_id and item.object_id:
                            if item.content_type_id in Command.CT_MATCH:
                                model.objects.filter(pk=item.pk).update(
                                    content_type_id=Command.CT_MATCH[item.content_type_id])
                                updated += 1
                            if item.content_type_id in Command.CT_CLEAR:
                                model.objects.filter(pk=item.pk).update(content_type_id=None, object_id=None)
                                updated += 1
                print("{count} instances ont été mises à jour vers le nouveau ContentType.".format(count=updated))
        else:
            data = {'model': None, 'ct': None}
            try:
                for model in [Contact, Adresse, Notification]:
                    for item in model.objects.all():
                        data = {'model': model, 'ct': item.content_type_id}
                        item.content_object
            except AttributeError:
                print(data)
                print("Attention : Les objets ne sont pas dans un état cohérent !")
            else:
                print("Les objets ont l'air d'être dans un état cohérent !")
