# coding: utf-8
"""
Remplacer tous les mots de passe par 123.
Utilisable uniquement en mode DEBUG.
"""
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db import transaction
from django.conf import settings


class Command(BaseCommand):
    """ Nettoyer et charger les données par défaut de la base de données """
    args = ''
    help = 'Remplace tous les mots de passe des utilisateurs par 123'

    def handle(self, *args, **options):
        """ Exécuter la commande """
        if settings.DEBUG:
            with transaction.atomic():
                count = User.objects.count()
                for user in User.objects.all():
                    user.set_password('123')
                    user.save()
                print("Les mots de passe de {0} utilisateurs ont été mis à jour.".format(count))
        else:
            print("Échec : Par sécurité, vous ne pouvez lancer cette commande qu'en mode DEBUG.")
