# coding: utf-8
"""
Préprocesseur de requête ajoutant dans le contexte des
templates tous les paramètres disponibles dans
:see settings.DEPARTMENT_SETTINGS:
"""
import re
from django.conf import settings

from core.models.instance import Instance


def instance(request):
    """
    Renvoie toutes les informations d'instance d'application
    Par défaut, les informations sont récupérées depuis le sous-domaine.
    Si l'utilisateur est authentifié, alors récupérer ses options
    au lieu de celles du sous-domaine.

    Ce que fait ce middleware :
    - Ajoute un attribut `subdomain` à l'objet request
    """

    def get_instance(name):
        """
        Renvoie une instance du modèle core.Instance

        correspondant au nom de sous-domaine passé
        """
        try:
            value = int(name)
            instance = Instance.objects.get(number=value)
            return instance
        except (ValueError, Instance.DoesNotExist):
            return None

    # Récupérer le nom de domaine et les parties du ndd
    request.domain = request.META['HTTP_HOST']
    request.subdomain = ''
    parts = request.domain.split('.')

    # accepte les nnd de la forme sub.dom.ext ou sub.localhost:8000
    if len(parts) == 3 or (re.match("^localhost", parts[-1]) and len(parts) == 2):
        request.subdomain = parts[0]
        request.domain = '.'.join(parts[1:])

    # Contexte de sortie
    context = settings.DEPARTMENT_SETTINGS.copy()

    # Le contexte final dépendra de l'utilisateur
    if not request.user.is_authenticated():
        context.update({'INSTANCE': get_instance(request.subdomain)})
    else:
        # TODO: Ici, l'instance va dépendre d'une configuration utilisateur
        context.update({'INSTANCE': get_instance(request.subdomain)})

    return context
