'''
Django Production settings for ddcs_loire project.
'''
from ddcs_loire.settings import *

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'g&*0o49mk(t&*vvo!o!rx5vpu%d9v5n*e7ars&$qz*lpi^s$k*'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['']

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django_extensions',
    'localflavor',
    'allauth',
    'allauth.account',
    'bootstrap3_datetime',
    'crispy_forms',
    'ckeditor',
    'raven.contrib.django.raven_compat',
    'administrative_division',
    'administration',
    'agreements',
    'contacts',
    'emergencies',
    'evaluations',
    'notifications',
    'protected_areas',
    'sports',
    'events',
    'authorizations',
    'declarations',
    'sub_agreements',
    'south',
)

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': 'localhost',
        'NAME': 'db_name',
        'USER': 'db_user',
        'PASSWORD': 'db_password',
    }
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATICFILES_DIRS = (
    '/home/dev/3000/env/lib/python3.4/site-packages/django/contrib/admin/static/',
    os.path.join(BASE_DIR, "static"),
)

STATIC_ROOT = '/home/dev/3000/releases/current/static/'


# Exceptions logging : 
# https://app.getsentry.com/

RAVEN_CONFIG = {
    'dsn': '',
}
