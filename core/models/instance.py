# coding: utf-8
""" Configuration """
from django.db import models
from django.db.utils import IntegrityError

from core.util.application import get_current_subdomain


class InstanceManager(models.Manager):
    """ Manager pour les instances """

    def get_for_request(self, request):
        """ Renvoyer l'objet d'instance pour l'utilisateur """
        subdomain = get_current_subdomain(request)
        try:
            number = int(subdomain)
            instance = self.get(number=number)
            return instance
        except (ValueError, Instance.DoesNotExist):
            # Si le sous-domaine n'est pas valide
            if request.user.is_authenticated():
                pass  # TODO: Retrouver l'objet instance de l'utilisateur
        pass

    def get_master(self):
        """ Renvoyer l'instance par défaut """
        return self.get(number=None)


class Instance(models.Model):
    """ Configuration de l'application """

    # Constantes
    WORKFLOW_CHOICES = [[0, "EDSR"], [1, "GGD + Préavis EDSR"], [2, "GGD + EDSR"]]

    # Champs
    name = models.CharField(max_length=96, blank=False, verbose_name="Nom")
    number = models.SmallIntegerField(null=True, verbose_name="Numéro de département")
    workflow = models.SmallIntegerField(default=0, choices=WORKFLOW_CHOICES, verbose_name="Workflow")
    or_map_required = models.BooleanField(default=False, verbose_name="Cartes Openrunner requises")
    # TODO: Choisir un widget couleur ?
    color = models.CharField(max_length=8, blank=True, verbose_name="Code couleur HTML")
    # Délais légaux par défaut
    manifestation_delay = models.SmallIntegerField(default=30, verbose_name="Délai manifestation")
    manifestation_anm_1dept_delay = models.SmallIntegerField(default=60, verbose_name="Délai autorisé non motorisé sur 1 dépt.")
    manifestation_anm_ndept_delay = models.SmallIntegerField(default=90, verbose_name="Délai autorisé non motorisé sur 2+ dépt.")
    manifestation_dvtm_delay = models.SmallIntegerField(default=60, verbose_name="Délai déclaré véhicules terrestres motorisés")
    manifestation_avtmc_delay = models.SmallIntegerField(default=90, verbose_name="Délai autorisé véhicules terrestres motorisés course")
    manifestation_avtm_vp_delay = models.SmallIntegerField(default=90, verbose_name="Délai autorisé véhicules terrestres motorisés voie publique")
    manifestation_avtm_vn_delay = models.SmallIntegerField(default=90, verbose_name="Délai autorisé véhicules terrestres motorisés hors voie publique")

    objects = InstanceManager()

    # Getter
    def is_master(self):
        """
        Renvoie si cette instance sert de modèle lorsqu'une information est indisponible dans une
        instance de département.
        """
        return self.number is None

    def is_openrunner_map_required(self):
        """ Renvoyer si le département nécessite des cartes OR dans les manifs """
        return self.or_map_required

    def get_default_color(self):
        """ Renvoyer une couleur qui correspond au nom de l'objet """
        return "#{0}".format(hex(hash(self.name.lower()))[-6:])

    # Override
    def save(self, *args, **kwargs):
        if self.pk is None:
            # Forcer l'unicité de l'instance master
            count = Instance.objects.filter(number=None).count()
            if count != 0:
                raise IntegrityError("Une seule instance master (number=None) peut exister")
        return super(Instance, self).save(*args, **kwargs)

    # Métadonnées
    class Meta:
        verbose_name = "configuration d'instance"
        verbose_name_plural = "configurations d'instance"
