# coding: utf-8
from __future__ import unicode_literals

from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from nouveautes.forms import NouveauteForm
from nouveautes.models.nouveaute import Nouveaute


class NouveauteAdmin(ImportExportActionModelAdmin):
    """ Configuration admin des nouveautés """
    list_display = ['pk', 'title', 'creation', 'public', 'get_role_names']
    search_fields = ['title', 'body']
    form = NouveauteForm


admin.site.register(Nouveaute, NouveauteAdmin)
