# coding: utf-8
from django.http.response import Http404
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils import timezone

from nouveautes.models.lecture import LectureNouveaute
from nouveautes.models.nouveaute import Nouveaute


def view_nouveautes(request, slug):
    """ Affichage d'une nouveauté """
    try:
        nouveaute = Nouveaute.objects.get(slug=slug)
        LectureNouveaute.objects.update_lecture(request.user, nouveaute.creation)
        return render_to_response("nouveautes/nouveaute_detail.html", {'nouveaute': nouveaute}, RequestContext(request))
    except Nouveaute.DoesNotExist:
        raise Http404()


def list_nouveautes(request):
    """ Affichage de la liste des nouveautés """
    if not request.user.is_staff:
        nouveautes = Nouveaute.objects.for_user(request.user)
        LectureNouveaute.objects.update_lecture(request.user, timezone.now())
    else:
        nouveautes = Nouveaute.objects.all().order_by('-id')
        LectureNouveaute.objects.update_lecture(request.user, timezone.now())
    return render_to_response("nouveautes/nouveaute_list.html", {'object_list': nouveautes}, RequestContext(request))
