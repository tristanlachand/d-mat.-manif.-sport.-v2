# coding: utf-8
from django.conf.urls import url
from nouveautes import views

urlpatterns = [
    url(r'^$', views.list_nouveautes, name='list'),
    url(r'^detail/(?P<slug>.+)', views.view_nouveautes, name='detail')
]
