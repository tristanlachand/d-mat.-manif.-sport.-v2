# coding: utf-8
"""
Cette application gère l'affichage des nouveautés à certains rôles.
Les nouveautés sont affichées aux membres concernés jusqu'à ce qu'ils
en prennent connaissance.
"""
