# coding: utf-8
from __future__ import absolute_import

from django import template

from nouveautes.models.nouveaute import Nouveaute


register = template.Library()


@register.filter(name="nouveaute_available")
def nouveautes_available(value):
    """
    Renvoyer si des nouveautés sont disponibles pour l'utilisateur connecté

    :param value: Objet HTTPRequest
    :type value: django.http.HTTPRequest
    :rtype: bool
    """
    if hasattr(value, 'user'):
        user = value.user
        if user.is_authenticated():
            return Nouveaute.objects.has_news(user)
    return False


@register.filter(name="nouveaute_list")
def nouveautes_list(value):
    """ Renvoyer les nouveautés qui sont non lues par l'utilisateur connecté """
    if hasattr(value, 'user'):
        user = value.user
        if user.is_authenticated():
            return Nouveaute.objects.news_for_user(user)
    return False
