# coding: utf-8
from django.forms import models
from django.forms.fields import TypedMultipleChoiceField
from django.forms.widgets import CheckboxSelectMultiple

from nouveautes.models.nouveaute import Nouveaute, NouveauteManager


class NouveauteForm(models.ModelForm):
    """ Formulaire admin des nouveautés """
    model = Nouveaute

    # Champs
    role = TypedMultipleChoiceField(NouveauteManager.ROLE_CHOICES, widget=CheckboxSelectMultiple(), required=False, label="Rôle")

    def __init__(self, *args, **kwargs):
        super(NouveauteForm, self).__init__(*args, **kwargs)
        if 'role' in self.initial:
            self.initial['role'] = self.initial['role'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        return super(NouveauteForm, self).save(commit=commit)

    class Meta:
        exclude = []
