# coding: utf-8
from __future__ import absolute_import

from datetime import datetime

import markdown
from operator import itemgetter

from django.core.urlresolvers import reverse
from django.db import models
from django_extensions.db.fields import AutoSlugField

from core.util.admin import set_admin_info


class NouveauteManager(models.Manager):
    """ Manager des nouveautés """

    # Constantes
    ROLE_TYPES = ['ggdagent', 'edsragent', 'cgdagentlocal', 'brigadeagent', 'codisagent', 'sdisagent', 'compagnieagent', 'cisagent',
                  'ddspagent', 'commissariatagentlocal', 'serviceagent', 'federationagent', 'mairieagent', 'cgagent',
                  'cgsuperieuragent', 'cgserviceagentlocal', 'organisateur', 'instructeur']
    ROLE_CHOICES = [['ggdagent', u"Agent GGD"], ['edsragent', u"Agent EDSR"], ['cgdagentlocal', u"Agent CGD"], ['brigadeagent', u"Agent brigade"],
                    ['codisagent', u"Agent CODIS"], ['sdisagent', u"Agent SDIS"], ['compagnieagent', u"Agent compagnie"], ['cisagent', u"Agent CIS"],
                    ['ddspagent', u"Agent DDSP"], ['commissariatagentlocal', u"Agent commissariat"], ['serviceagent', u"Agent service"],
                    ['federationagent', u"Agent fédération"], ['mairieagent', u"Agent mairie"], ['cgagent', u"Agent CG"],
                    ['cgsuperieuragent', u"Supérieur CG"], ['cgserviceagentlocal', u"Agent service CG"],
                    ['organisateur', "Organisateur"], ['instructeur', "Instructeur"]]
    ROLE_CHOICES = sorted(ROLE_CHOICES, key=itemgetter(1))

    # Privé
    def _get_role_type(self, user):
        """
        Renvoie la chaîne correspondant au rôle de l'utilisateur
        """
        for role in self.ROLE_TYPES:
            # Tester les rôles dont le modèle est lié directement à User
            try:
                if getattr(user, role) is not None:
                    return role
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agent
            try:
                if getattr(user.agent, role) is not None:
                    return role
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agentlocal
            try:
                if getattr(user.agentlocal, role) is not None:
                    return role
            except AttributeError:
                pass
        return None

    # Getter
    def has_news(self, user):
        """
        Renvoie si une nouveauté n'a jamais été lue par l'utilisateur

        :param user: utilisateur à considérer
        :returns: True si une nouveauté est non-lue par l'utilisateur, False sinon
        :rtype: bool
        """
        if user and not user.is_anonymous() and self._get_role_type(user) is not None:
            return self.for_user(user).filter(creation__gte=user.lecturenouveaute.date).exists()
        return False

    def for_user(self, user):
        """
        Renvoie les nouveautés qui peuvent être lues par l'utilisateur

        :returns: queryset des Nouveautés accessibles pour user
        """
        return self.filter(role__icontains=self._get_role_type(user) or 'undefined').order_by('-creation')

    def news_for_user(self, user):
        """
        Renvoie les nouveautés qui n'ont pas été lues par l'utilisateur

        :returns: queryset des Nouveautés accessibles pour user
        """
        if self._get_role_type(user):
            return self.for_user(user).filter(creation__gte=user.lecturenouveaute.date).order_by('-creation')
        return self.none()


class Nouveaute(models.Model):
    """ Nouveauté à afficher aux utilisateurs de groupes différents """

    # Champs
    title = models.CharField(max_length=128, blank=False, verbose_name="Titre")
    slug = AutoSlugField(max_length=128, populate_from='title', allow_duplicates=False, verbose_name="Slug")
    body = models.TextField(blank=False, help_text="Contenu au format Markdown", verbose_name="Contenu de la nouveauté")
    creation = models.DateTimeField(default=datetime.now, editable=False, verbose_name="Date de création")
    public = models.BooleanField(default=True, db_index=True, verbose_name="Publié")
    role = models.TextField(blank=True,
                            help_text="Rôles séparés par une virgule", verbose_name="Rôles autorisés")
    # TODO: Groups sera utilisé dans la marque blanche
    groups = models.ManyToManyField('auth.Group', blank=True, related_name='nouveautes', verbose_name="Groupes autorisés à voir le contenu")
    objects = NouveauteManager()

    # Getter
    def get_absolute_url(self):
        """ Renvoie l'URL de l'objet """
        return reverse('nouveautes:detail', args=None, kwargs={'slug': self.slug})

    @set_admin_info(short_description="Rôles")
    def get_role_names(self):
        """ Renvoie la liste des rôles avec leur nom lisible """
        roles = self.role.split(',')
        role_dict = dict(NouveauteManager.ROLE_CHOICES)
        names = [role_dict.get(role) for role in roles if role]
        return ', '.join(names)

    def get_body_html(self):
        """ Renvoie le corps de la nouveauté au format HTML """
        return markdown.markdown(self.body)

    # Propriétés
    html = property(get_body_html)

    # Métadonnées
    class Meta:
        verbose_name = u"Nouveauté de la plateforme"
        verbose_name_plural = u"Nouveautés de la plateforme"
        app_label = 'nouveautes'
