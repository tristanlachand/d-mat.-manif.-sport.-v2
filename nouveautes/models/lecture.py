# coding: utf-8
from __future__ import absolute_import

from django.conf import settings
from datetime import datetime
from django.db import models
from annoying.fields import AutoOneToOneField
from django.utils import timezone
import pytz


class LectureNouveauteManager(models.Manager):
    """ Manager des lectures de nouveautés """

    # Setter
    def update_lecture(self, user, lecture_date=None):
        """
        Définit la date

        :param user: utilisateur à considérer
        :returns: True si une nouveauté est non-lue par l'utilisateur, False sinon
        :rtype: bool
        """
        if user and user.is_authenticated():
            now = lecture_date or timezone.now()
            user.lecturenouveaute.date = now
            user.lecturenouveaute.save()
            return True
        return False

    def for_user(self, user):
        """
        Renvoie les nouveautés qui peuvent être lues par l'utilisateur

        :returns: queryset des Nouveautés accessibles pour user
        """
        if user and user.is_authenticated():
            return self.filter(groupes__in=user.groups.all()).order_by('-creation')
        return self.none()


class LectureNouveaute(models.Model):
    """ Informations de dernière nouveauté lue par un utilisateur """

    # Champs
    user = AutoOneToOneField(settings.AUTH_USER_MODEL, related_name='lecturenouveaute', verbose_name="Utilisateur")
    date = models.DateTimeField(default=datetime.fromtimestamp(0, tz=pytz.UTC), editable=False, verbose_name="Date de dernière lecture des nouveautés")
    # Types d'utilisateurs habilités
    objects = LectureNouveauteManager()

    # Métadonnées
    class Meta:
        verbose_name = "Dernière lecture des nouveautés"
        verbose_name_plural = "Dernières lectures des nouveautés"
        app_label = 'nouveautes'
