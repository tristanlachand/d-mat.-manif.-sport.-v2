# coding: utf-8
from importlib import import_module

from django.conf import settings
from django.contrib.auth.models import User, Group, AnonymousUser
from django.test import TestCase

from administration.factories import MairieAgentFactory
from administrative_division.factories import CommuneFactory
from nouveautes.models.lecture import LectureNouveaute
from nouveautes.models.nouveaute import Nouveaute


class NouveauteTest(TestCase):
    """ Tests des nouveautés """

    # Configuration
    fixtures = []

    def setUp(self):
        """ Définit l'environnement des tests """
        self.engine = import_module(settings.SESSION_ENGINE)
        self.session = self.engine.SessionStore()
        self.mairieagent = MairieAgentFactory.create()
        self.user = self.mairieagent.user

    def test_nouveaute_workflow(self):
        """ Vérifier le fonctionnement des nouveautés """
        # Créer une nouveauté pour les agents municipaux (rôle : mairieagent)
        Nouveaute.objects.create(title='New', body=u'Nouveauté', role='mairieagent')

        # Normalement, on a du nouveau contenu pour user, qui est un mairieagent
        self.assertTrue(Nouveaute.objects.has_news(self.user))
        self.assertFalse(Nouveaute.objects.has_news(AnonymousUser()))

        # Mettre à jour la date de dernière lecture pour user
        self.assertTrue(LectureNouveaute.objects.update_lecture(self.user))
        self.assertFalse(LectureNouveaute.objects.update_lecture(AnonymousUser()))

        # Normalement, à ce stade plus aucune nouveauté n'est à signaler pour user
        self.assertFalse(Nouveaute.objects.has_news(self.user))

    def test_nouveaute_html(self):
        """ Test du markup """
        nouveaute = Nouveaute.objects.create(title='New', body=u'Nouveauté\n=', role='mairieagent')
        self.assertTrue('h1' in nouveaute.html)
