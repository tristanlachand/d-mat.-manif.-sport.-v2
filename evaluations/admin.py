from __future__ import unicode_literals

from django.contrib import admin

from .models import RNREvaluation
from .models import N2KEvaluation


class RNREvaluationInline(admin.StackedInline):
    model = RNREvaluation
    extra = 0


class Natura2000EvaluationInline(admin.StackedInline):
    model = N2KEvaluation
    extra = 0
