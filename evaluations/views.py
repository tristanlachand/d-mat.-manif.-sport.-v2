from __future__ import unicode_literals

from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView

from events.decorators import organisateur_required
from events.models import Manifestation

from .models import N2KEvaluation, RNREvaluation
from .forms import Natura2000EvaluationForm, RNREvaluationForm


class GenericCreateView(CreateView):

    @method_decorator(organisateur_required)
    def dispatch(self, *args, **kwargs):
        return super(GenericCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form, **kwargs):
        form.instance.manifestation = Manifestation.objects.get(pk=self.kwargs['manifestation_pk'])
        return super(GenericCreateView, self).form_valid(form)


class GenericUpdateView(UpdateView):

    @method_decorator(organisateur_required)
    def dispatch(self, *args, **kwargs):
        return super(GenericUpdateView, self).dispatch(*args, **kwargs)


class Natura2000EvaluationCreate(GenericCreateView):
    model = N2KEvaluation
    form_class = Natura2000EvaluationForm


class Natura2000EvaluationUpdate(GenericUpdateView):
    model = N2KEvaluation
    form_class = Natura2000EvaluationForm


class RNREvaluationCreate(GenericCreateView):
    model = RNREvaluation
    form_class = RNREvaluationForm


class RNREvaluationUpdate(GenericUpdateView):
    model = RNREvaluation
    form_class = RNREvaluationForm
