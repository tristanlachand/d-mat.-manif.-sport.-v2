from __future__ import unicode_literals

import datetime
import factory

from factory.fuzzy import FuzzyDate

from events.factories import DeclarationNMFactory

from .models import RNREvaluation
from .models import N2KEvaluation


class RNREvaluationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = RNREvaluation

    administrator_contact_date = FuzzyDate(datetime.date(2010, 1, 1))
    rnr_entries = 1
    rnr_audience = 1
    track_total_length = 1
    manifestation = factory.SubFactory(DeclarationNMFactory)


class Natura2000EvaluationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = N2KEvaluation

    range_from_site = 5
    track_total_length = 1
    manifestation = factory.SubFactory(DeclarationNMFactory)
