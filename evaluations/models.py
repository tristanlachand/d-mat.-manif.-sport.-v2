# coding: utf-8
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.encoding import smart_text

from events.models import Manifestation
from protected_areas.models import SiteN2K, RNR


class EvaluationManifestation(models.Model):
    COST_CHOICES = (
        ('0', "moins de 5 000 €"),
        ('1', "entre 5 000 et 20 000 €"),
        ('2', "entre 20 000 et 100 000 €"),
        ('3', "plus de 100 000 €"),
    )
    IMPACT_CHOICES = (
        ('0', '0'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
    )

    content_type = models.ForeignKey(ContentType, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    manifestation = models.OneToOneField(Manifestation, verbose_name='manifestation')
    # Description
    place = models.CharField("lieu-dit", max_length=255, blank=True)
    # Frequency
    every_year = models.BooleanField("la manifestation a lieu chaque année", default=False)
    first_edition = models.BooleanField("première édition de la manifestation", default=False)
    other_frequency = models.CharField("autre fréquence", max_length=255, blank=True)
    # Budget
    cost = models.CharField("coût de la manifestation", max_length=1, choices=COST_CHOICES)
    # Localization
    track_total_length = models.PositiveIntegerField("longueur totale des parcours")
    # Related facilities
    night_lighting = models.BooleanField("éclairage nocturne", default=False)
    night_lighting_desc = models.TextField("description", blank=True)
    trails_marks = models.BooleanField(
        "balisage des sentiers",
        default=False,
        help_text="Seuls les balisages temporaires retirés sous 24h sont autorisés dans le "
                  "cadre de manifestations sportives. Par conséquent ne seront pas autorisés "
                  "l'usage de peinture, confettis ou tout autre type de balisage laissant une "
                  "empreinte sur les milieux",
    )
    trails_marks_desc = models.TextField('description', blank=True)
    other_facilities = models.BooleanField("autres types d'aménagements", default=False)
    other_facilities_desc = models.TextField('description', blank=True)
    # Potential implications
    noise_emissions = models.BooleanField(
        "les émissions sonores sont susceptibles de déranger les espèces présentes "
        "sur le site (puissance des émissions et proximité du site)",
        default=False,
        help_text="Le bruit peut entraîner le dérangement de certaines espèces animales, "
                  "notamment en période de nidification ou d’hivernage pour les oiseaux.",
    )
    waste_management = models.BooleanField(
        "vous avez prévu des dispositifs particuliers pour la gestion des déchets "
        "pendant et après la manifestation [À décrire dans le champ réservé]",
        default=False,
        help_text="Les déchets polluent le milieu naturel et représentent un danger pour les "
                  "espèces animales (risque d’ingestion notamment).",
    )
    waste_management_desc = models.TextField('description', blank=True)
    awareness = models.BooleanField(
        "vous avez prévu la mise en place d'actions de communication ou de "
        "sensibilisation concernant les enjeux environnementaux",
        default=False,
        help_text="La sensibilisation et la communication permettent de réduire les impacts sur "
                  "l’environnement. Contacter l’animateur du site pour la réalisation ou la "
                  "mise à disposition de documents de communication.",
    )
    impact_reduction = models.BooleanField(
        "vous avez pris des mesures pour réduire l'impact de la manifestation sur le "
        "milieu naturel (contacts structures, choix du lieu en fonction des enjeux, "
        "délimitation de zones interdites au public...) [À décrire dans le champ "
        "réservé]",
        default=False,
        help_text="réduction des impacts",
    )
    impact_reduction_desc = models.TextField('description', blank=True)
    # Conclusions
    estimated_impact = models.CharField(
        "impact estimé de la manifestation",
        max_length=1,
        choices=IMPACT_CHOICES,
        help_text="D'après les éléments mis en évidence dans ce formulaire, évaluer l'impact de "
                  "la manifestation en donnant un chiffre de 0 à 5 (0 correspondant à une "
                  "absence d'incidences et 5 à une incidence significative sur le ou les sites "
                  "concernés)",
    )

    def get_absolute_url(self):
        return Manifestation.objects.get_subclass(id=self.manifestation.id).get_absolute_url()

    class Meta:
        abstract = True
        verbose_name = "évaluation d'une manifestation"
        verbose_name_plural = "évaluations de manifestations"
        app_label = 'evaluations'


class RNREvaluation(EvaluationManifestation):
    """ Évalutation réserve naturelle régionale """
    DURATION_CHOICES = (
        ('1', "demande d'autorisation pour 1 année"),
        ('3', "demande d'autorisation pour 3 années avec engagement de non modification du "
              "tracé et des conditions d'organisation"),
    )
    # Frequency
    request_duration = models.CharField("durée de la demande d'autorisation", max_length=1, choices=DURATION_CHOICES)
    # Localization
    sites = models.ManyToManyField('protected_areas.rnr', verbose_name="sites RNR concernés")
    # Attendance
    rnr_entries = models.PositiveIntegerField("participants concernés par le passage dans la RNR",)
    rnr_audience = models.PositiveIntegerField("spectateurs concernés par le passage dans la RNR",)
    # Provisions taken
    administrator_contact_date = models.DateField("date de contact avec les gestionnaires de la réserve",)
    sensitization = models.CharField("dispositif de sensibilisation à l'environnement et à la réserve envisagé",
                                     max_length=255)
    forbidden_areas = models.CharField("dispositif de canalisation du public", max_length=255)
    administrator_attendance = models.BooleanField(
        "l’organisateur souhaite la présence du gestionnaire de la RNR lors de la "
        "manifestation",
        default=False,
    )
    # Potential implications
    plane_attendance = models.BooleanField("il y aura présence d'engins aériens [À décrire dans le champ réservé]",
                                           default=False)
    plane_attendance_desc = models.TextField(
        "dispositif pour la gestion des engins aériens",
        blank=True,
    )
    trampling = models.BooleanField("la présence des participants et du public entraînera un piétinement des sols "
                                    "[À décrire dans le champ réservé]", default=False)
    trampling_desc = models.TextField("dispositif pour la gestion du piétinement", blank=True)
    markup = models.BooleanField("vous avez prévu un balisage, une signalisation spécifique à la manifestation "
                                 "[À décrire dans le champ réservé]", default=False)
    markup_desc = models.TextField("dispositif pour la gestion du balisage et de la signalisation", blank=True)
    # Conclusion
    regulatory_compliance = models.BooleanField(
        "le projet, la manifestation respecte bien la réglementation propre à la "
        "réserve",
        default=False,
        help_text="voir encadré ci-dessus",
    )
    rnr_conclusion = models.BooleanField("ce formulaire permet de conclure à l'absence d'incidence significative sur "
                                         "le ou les sites RNR concernés par la manifestation", default=False)

    def __str__(self):
        return smart_text(self.manifestation.__str__())

    class Meta(EvaluationManifestation.Meta):
        verbose_name = "évaluation RNR"
        verbose_name_plural = "évaluations RNR"
        default_related_name = "rnrevaluations"
        app_label = "evaluations"


class N2KEvaluation(EvaluationManifestation):
    # Description
    diurnal = models.BooleanField("manifestation diurne", default=False)
    nocturnal = models.BooleanField("manifestation nocturne", default=False)

    # Localisation
    sites = models.ManyToManyField(SiteN2K, verbose_name="sur site Natura 2000")
    range_from_site = models.PositiveIntegerField("distance par rapport au site NATURA 2000")

    # Zone d'influence
    rnn = models.BooleanField("Réserve Naturelle Nationale", default=False)
    rnr = models.BooleanField("Réserve Naturelle Régionale", default=False)
    biotope_area = models.BooleanField("Arrêté de Protection de Biotope", default=False)
    classified_site = models.BooleanField("Site Classé", default=False)
    registered_site = models.BooleanField("Site Inscrit", default=False)
    pnr = models.BooleanField("Parc Naturel Régional", default=False)
    znieff = models.BooleanField("ZNIEFF", default=False)

    # Complexes liés
    parking_lot = models.BooleanField("parking", default=False)
    parking_lot_desc = models.TextField('description', blank=True)
    reception_area = models.BooleanField("zone d'accueil du public/des spéctateurs", default=False)
    reception_area_desc = models.TextField('description', blank=True)
    supplying_area = models.BooleanField("zones de ravitaillement", default=False)
    supplying_area_desc = models.TextField('description', blank=True)
    storage_area = models.BooleanField("zone de stockage (matériel, véhicules...)", default=False)
    storage_area_desc = models.TextField('description', blank=True)
    awards_stage = models.BooleanField("scène de remise des prix", default=False)
    awards_stage_desc = models.TextField('description', blank=True)
    bivouac_area = models.BooleanField("zone de bivouac", default=False)
    bivouac_area_desc = models.TextField('description', blank=True)
    noise = models.BooleanField("émissions sonores (sono, concert...)", default=False)
    noise_desc = models.TextField('description', blank=True)

    # Impact sur les habitats naturels
    lawn = models.BooleanField("pelouse", default=False)
    lawn_comments = models.CharField("commentaires", blank=True, max_length=255)
    semi_wooded_lawn = models.BooleanField("pelouse semi-boisée", default=False)
    semi_wooded_lawn_comments = models.CharField("commentaires", blank=True, max_length=255)
    moor = models.BooleanField("lande", default=False)
    moor_comments = models.CharField("commentaires", blank=True, max_length=255)
    scrubland_maquis = models.BooleanField("garrigue/maquis", default=False)
    scrubland_maquis_comments = models.CharField("commentaires", blank=True, max_length=255)

    coniferous_forest = models.BooleanField("forêt de résineux", default=False)
    coniferous_forest_comments = models.CharField("commentaires", blank=True, max_length=255)
    deciduous_forest = models.BooleanField("forêt de feuillus", default=False)
    deciduous_forest_comments = models.CharField("commentaires", blank=True, max_length=255)
    mixed_forest = models.BooleanField("forêt mixte", default=False)
    mixed_forest_comments = models.CharField("commentaires", blank=True, max_length=255)
    plantation = models.BooleanField("plantation", default=False)
    plantation_comments = models.CharField("commentaires", blank=True, max_length=255)

    cliff = models.BooleanField("falaise", default=False)
    cliff_comments = models.CharField("commentaires", blank=True, max_length=255)
    outcrop = models.BooleanField("affleurement rocheux", default=False)
    outcrop_comments = models.CharField("commentaires", blank=True, max_length=255)
    scree = models.BooleanField("éboulis", default=False)
    scree_comments = models.CharField("commentaires", blank=True, max_length=255)
    blocks = models.BooleanField("blocs", default=False)
    blocks_comments = models.CharField("commentaires", blank=True, max_length=255)

    ditch = models.BooleanField("fossé", default=False)
    ditch_comments = models.CharField("commentaires", blank=True, max_length=255)
    watercourse = models.BooleanField("cours d'eau", default=False)
    watercourse_comments = models.CharField("commentaires", blank=True, max_length=255)
    pound = models.BooleanField("étang", default=False)
    pound_comments = models.CharField("commentaires", blank=True, max_length=255)
    bog = models.BooleanField("tourbière", default=False)
    bog_comments = models.CharField("commentaires", blank=True, max_length=255)
    gravel = models.BooleanField("gravière", default=False)
    gravel_comments = models.CharField("commentaires", blank=True, max_length=255)
    wet_meadow = models.BooleanField("prairie humide", default=False)
    wet_meadow_comments = models.CharField("commentaires", blank=True, max_length=255)

    # Impact sur les espèces
    amphibia = models.BooleanField("amphibiens", default=False)
    amphibia_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    reptiles = models.BooleanField("réptiles", default=False)
    reptiles_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    crustaceans = models.BooleanField("crustacés", default=False)
    crustaceans_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    insects = models.BooleanField("insectes", default=False)
    insects_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    terrestrial_mammals = models.BooleanField("mammifères terrestres", default=False)
    terrestrial_mammals_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    birds = models.BooleanField("oiseaux", default=False)
    birds_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    plants = models.BooleanField("plantes", default=False)
    plants_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    fish = models.BooleanField("poissons", default=False)
    fish_species = models.CharField("nom de l'espèce", blank=True, max_length=255)

    # Implications potentielles
    water_crossing = models.BooleanField(
        "franchissement d'un cours d'eau hors d'un passage aménagé",
        default=False,
        help_text="la traversée des cours d’eau par des engins motorisés, des VTT ou des "
                  "personnes à pied entraîne une dégradation du lit et des berges, la "
                  "destruction des habitats et le dérangement des espèces",
    )
    off_trail = models.BooleanField(
        "passage des participants en dehors des sentiers (à localiser sur la carte)",
        default=False,
        help_text="le passage d’individus en dehors des sentiers entraîne le piétinement et/ou "
                  "la destruction des habitats et des espèces végétales (prairies naturelles, "
                  "zones humides, tourbières, etc.). Le passage entraîne également le "
                  "dérangement des espèces animales telles que les oiseaux en période de "
                  "nidification et d’hivernage.",
    )
    bio_implications = models.BooleanField(
        "la présence du public est susceptible d'avoir une incidence sur les habitats "
        "naturels et/ou les espèces animales et végétales",
        default=False,
        help_text="En fonction de sa localisation et du nombre de personnes attendues, la "
                  "présence du public peut déranger les espèces animales via le bruit ou la "
                  "présence visuelle. Un rassemblement peut également entraîner un piétinement "
                  "important et endommager les espèces végétales.",
    )
    related_facilities = models.BooleanField(
        "présence d'aménagements connexes",
        default=False,
        help_text="Les aménagements (zones de ravitaillement, d’accueil, de restauration, etc.) "
                  "sont à prendre en compte dans l'organisation de la manifestation car ils "
                  "peuvent avoir une incidence à la fois sur les milieux et sur les espèces.",
    )
    light_emissions = models.BooleanField(
        "les émissions lumineuses nocturnes sont-elles susceptibles de déranger les "
        "espèces présentes sur le site",
        default=False,
        help_text="Les sources lumineuses sont susceptibles de perturber les espèces animales "
                  "telles que les chauves-souris. Ces perturbations sont à prendre en compte "
                  "uniquement de nuit.",
    )

    # Conclusion
    natura_2000_conclusion = models.BooleanField(
        "ce formulaire permet de conclure à l'absence d'incidence significative sur "
        "le ou les sites NATURA 2000 concernés par la manifestation",
        default=False,
        help_text="Si laissé décoché, ce formulaire ne permet pas de conclure à l'absence "
                  "d'incidences significatives sur le ou les sites NATURA 2000 concernés par la "
                  "manifestation. L'évaluation d'incidences doit se poursuivre. Contactez la "
                  "DDT au plus vite.",
    )

    def __str__(self):
        return smart_text(self.manifestation.__str__())

    class Meta(EvaluationManifestation.Meta):
        verbose_name = "évaluation Natura 2000"
        verbose_name_plural = "évaluations Natura 2000"
        default_related_name = "natura2000evaluations"
        app_label = "evaluations"
