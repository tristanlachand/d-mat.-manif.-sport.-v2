# coding: utf-8
from django.apps.config import AppConfig


class EvaluationsConfig(AppConfig):
    name = 'evaluations'
    verbose_name = 'Évaluations Naturelles'


default_app_config = 'evaluations.EvaluationsConfig'
