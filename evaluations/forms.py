from __future__ import unicode_literals

from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from crispy_forms.layout import Submit, Layout, Fieldset, HTML
from crispy_forms.bootstrap import FormActions, AppendedText

from bootstrap3_datetime.widgets import DateTimePicker

from ddcs_loire.utils import GenericForm

from .models import N2KEvaluation, RNREvaluation


IMPACTS_HELP = HTML(
    "<div class='well'>"
    "<p>{0}</p><a href={1} target='_blank'>{2}</a></div>".format(
        'Renseigner les tableaux ci-dessous en fonction de vos connaissances : vous '
        'pouvez vous aider en téléchargeant les fiches synthétiques propres à chaque '
        'site présentes sur le site internet de la DDT de la Loire.'.capitalize(),
        settings.DDT_SITE,
        'Liste des sites natura 2000 par communes'.capitalize(),
    )
)

NATURAL_IMPACTS_FIELDSET = (
    'lawn',
    'lawn_comments',
    'semi_wooded_lawn',
    'semi_wooded_lawn_comments',
    'moor',
    'moor_comments',
    'scrubland_maquis',
    'scrubland_maquis_comments',
    'coniferous_forest',
    'coniferous_forest_comments',
    'deciduous_forest',
    'deciduous_forest_comments',
    'mixed_forest',
    'mixed_forest_comments',
    'plantation',
    'plantation_comments',
    'cliff',
    'cliff_comments',
    'outcrop',
    'outcrop_comments',
    'scree',
    'scree_comments',
    'blocks',
    'blocks_comments',
    'ditch',
    'ditch_comments',
    'watercourse',
    'watercourse_comments',
    'pound',
    'pound_comments',
    'bog',
    'bog_comments',
    'gravel',
    'gravel_comments',
    'wet_meadow',
    'wet_meadow_comments',
)
SPECIES_IMPACTS_FIELDSET = (
    'amphibia',
    'amphibia_species',
    'reptiles',
    'reptiles_species',
    'crustaceans',
    'crustaceans_species',
    'insects',
    'insects_species',
    'terrestrial_mammals',
    'terrestrial_mammals_species',
    'birds',
    'birds_species',
    'plants',
    'plants_species',
    'fish',
    'fish_species',
)
FACILITIES_FIELDSET_N2000 = (
    'parking_lot',
    'parking_lot_desc',
    'reception_area',
    'reception_area_desc',
    'supplying_area',
    'supplying_area_desc',
    'storage_area',
    'storage_area_desc',
    'awards_stage',
    'awards_stage_desc',
    'bivouac_area',
    'bivouac_area_desc',
    'noise',
    'noise_desc',
    'night_lighting',
    'night_lighting_desc',
    'trails_marks',
    'trails_marks_desc',
    'other_facilities',
    'other_facilities_desc',
)

FACILITIES_FIELDSET_RNR = (
    'night_lighting',
    'night_lighting_desc',
    'trails_marks',
    'trails_marks_desc',
    'other_facilities',
    'other_facilities_desc',
)

N2K_IMPLICATIONS_FIELDSET = (
    'water_crossing',
    'off_trail',
    'bio_implications',
    'related_facilities',
    'noise_emissions',
    'light_emissions',
    'waste_management',
    'waste_management_desc',
    'awareness',
    'impact_reduction',
    'impact_reduction_desc',
)
RNR_IMPLICATIONS_FIELDSET = (
    HTML(
        "<div class='well'>"
        "<p>{paragraph}</p>"
        "<ul>"
        "<li>{parking}</li>"
        "<li>{trash}</li>"
        "<li>{picking}</li>"
        "<li>{light}</li>"
        "<li>{noise}</li>"
        "<li>{animals}</li>"
        "</ul>"
        "</div>".format(
            paragraph='sont interdits sur la réserve naturelle : '.capitalize(),
            parking='le stationnement et passage de participants et du public en dehors des '
                    'sentiers autorisés'.capitalize(),
            trash="l'abandon de déchets ou produits".capitalize(),
            picking='la cueillette de végétaux'.capitalize(),
            light="d'utiliser un éclairage artificiel (en dehors de ceux utilisés par les "
                  "services publics de secours)".capitalize(),
            noise="de troubler le calme et la tranquillité des lieux et des animaux, par des "
                  "cris ou bruits divers, par l'utilisation d'un appareil radiophonique, ou "
                  "tout autre instrument sonore".capitalize(),
            animals="les animaux domestiques non tenus en laisse et la divagation de ces mêmes "
                    "animaux".capitalize(),
        )
    ),
    'noise_emissions',
    'waste_management',
    'waste_management_desc',
    'awareness',
    'impact_reduction',
    'impact_reduction_desc',
    'plane_attendance',
    'plane_attendance_desc',
    'trampling',
    'trampling_desc',
    'markup',
    'markup_desc',
)
AREA_OF_INFLUENCE_FIELDSET = (
    HTML(
        "<div class='well'>"
        "<p>1. {0} <a href={1} target='_blank'>{2}</a> {3}</p>"
        "<p>2. {4}</p>"
        "<p>3. {5}</p>"
        "<p>4. {6}</p>"
        "<p></p>"
        "<p><a href={7} target='_blank'>{8}</a></p>"
        "</div>".format(
            "accéder au site internet de cartographie".capitalize(),
            settings.CARMEN_WEBSITE,
            "carmen".upper(),
            "de la DREAL Rhône-Alpes",
            "sélectionner le département puis la commune concernée par la manifestation".capitalize(),
            "repérer le secteur de la manifestation".capitalize(),
            "dans les rubrique « zonages nature », « zonages paysage » et « inventaire "
            "nature-biodiversité » sélectionner uniquement les items demandés".capitalize(),
            settings.LOIRE_PROTECTED_AREAS,
            "sites naturels protégés du département".capitalize(),
        )
    ),
    'rnn',
    'rnr',
    'biotope_area',
    'classified_site',
    'registered_site',
    'pnr',
    'znieff',
)
N2K_CONCLUSIONS_FIELDSET = (
    'estimated_impact',
    'natura_2000_conclusion',
)
N2K_EVALUATION_FIELDSET = (
    'place',
    'diurnal',
    'nocturnal',
)
FREQUENCY_FIELDSET = (
    'every_year',
    'first_edition',
    'other_frequency',
)
RNR_CONCLUSIONS_FIELDSET = (
    HTML(
        "<div class='well'>"
        "{0} "
        "<a href='/rnr/reglement/gorges-de-la-loire/' target='_blank'>"
        "{1}</a></div>".format(
            "Réglement de la RNR des",
            "Gorges de la Loire",
        )
    ),
    'regulatory_compliance',
    'rnr_conclusion',
    'estimated_impact',
)
PROVISIONS_FIELDSET = (
    'administrator_contact_date',
    'sensitization',
    'forbidden_areas',
    'administrator_attendance',
)
ATTENDANCE_FIELDSET = (
    AppendedText('rnr_entries', 'personnes'),
    AppendedText('rnr_audience', 'personnes'),
)
LOCALIZATION_FIELDSET = (
    HTML(
        "<div class='well'>"
        "<a href='/protected_areas/RNR/' target='_blank'>"
        "{contacts}</a></div>".format(
            contacts="coordonnées des opérateurs des différents sites RNR"
        )
    ),
    'sites',
    AppendedText('track_total_length', 'km'),
)
DESCRIPTION_FIELDSET = (
    'place',
    'request_duration',
)


class Natura2000EvaluationForm(GenericForm):

    class Meta:
        model = N2KEvaluation
        exclude = ('content_type', 'object_id', 'manifestation')

    def __init__(self, *args, **kwargs):
        super(Natura2000EvaluationForm,
              self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset(
                "formulaire d'évaluation Natura 2000".capitalize(),
                *N2K_EVALUATION_FIELDSET
            ),
            Fieldset(
                "fréquence de la manifestation".capitalize(),
                *FREQUENCY_FIELDSET
            ),
            Fieldset(
                "budget de la manifestation".capitalize(),
                'cost',
            ),
            Fieldset(
                "localisation de la manifestation par rapport aux sites Natura 2000".capitalize(),
                HTML(
                    "<div class='well'>"
                    "<a href='/protected_areas/SiteN2K/'"
                    " target='_blank'>"
                    "{contacts}</a></div>".format(
                        contacts="coordonnées des opérateurs des différents sites Natura2000"
                    )
                ),
                'sites',
                AppendedText('track_total_length', 'km'),
                AppendedText('range_from_site', 'km'),
            ),
            Fieldset(
                "état des lieux de la zone d'influence".capitalize(),
                *AREA_OF_INFLUENCE_FIELDSET
            ),
            Fieldset(
                "impacts potentiels sur les milieux naturels".capitalize(),
                IMPACTS_HELP,
                *NATURAL_IMPACTS_FIELDSET
            ),
            Fieldset(
                "impacts potentiels sur les espèces".capitalize(),
                IMPACTS_HELP,
                *SPECIES_IMPACTS_FIELDSET
            ),
            Fieldset(
                "présence d'aménagements connexes".capitalize(),
                *FACILITIES_FIELDSET_N2000
            ),
            Fieldset(
                "incidences potentielles sur Natura 2000".capitalize(),
                *N2K_IMPLICATIONS_FIELDSET
            ),
            Fieldset(
                "conclusions".capitalize(),
                *N2K_CONCLUSIONS_FIELDSET
            ),
            FormActions(
                Submit('save', "soumettre".capitalize())
            )
        )


class RNREvaluationForm(GenericForm):

    class Meta:
        model = RNREvaluation
        exclude = ('content_type', 'object_id', 'manifestation')
        labels = {
            'track_total_length': "longueur totale des parcours en RNR",
        }
        widgets = {
            'administrator_contact_date': DateTimePicker(
                options={"format": "DD/MM/YYYY",
                         "pickTime": False}
                ),
        }

    def __init__(self, *args, **kwargs):
        super(RNREvaluationForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset(
                "formulaire d'évaluation RNR".capitalize(),
                *DESCRIPTION_FIELDSET
            ),
            Fieldset(
                "fréquence de la manifestation".capitalize(),
                *FREQUENCY_FIELDSET
            ),
            Fieldset(
                "localisation de la manifestation par rapport aux sites RNR".capitalize(),
                *LOCALIZATION_FIELDSET
            ),
            Fieldset(
                "fréquentation".capitalize(),
                *ATTENDANCE_FIELDSET
            ),
            Fieldset(
                "budget de la manifestation".capitalize(),
                'cost',
            ),
            Fieldset(
                "prise en compte de l'environnement".capitalize(),
                *PROVISIONS_FIELDSET
            ),
            Fieldset(
                "présence d'aménagements connexes".capitalize(),
                *FACILITIES_FIELDSET_RNR
            ),
            Fieldset(
                "incidences potentielles sur RNR".capitalize(),
                *RNR_IMPLICATIONS_FIELDSET
            ),
            Fieldset(
                "conclusion".capitalize(),
                *RNR_CONCLUSIONS_FIELDSET
            ),
            FormActions(
                Submit('save', "soumettre".capitalize())
            )
        )
