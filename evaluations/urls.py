from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import Natura2000EvaluationCreate
from .views import Natura2000EvaluationUpdate
from .views import RNREvaluationCreate
from .views import RNREvaluationUpdate

urlpatterns = [

    url(r'^natura2000evaluation/add/(?P<manifestation_pk>\d+)/$',
        Natura2000EvaluationCreate.as_view(),
        name='natura2000evaluation_add'),
    url(r'^natura2000evaluation/(?P<pk>\d+)/edit$',
        Natura2000EvaluationUpdate.as_view(),
        name='natura2000evaluation_update'),
    url(r'^rnrevaluation/add/(?P<manifestation_pk>\d+)/$',
        RNREvaluationCreate.as_view(),
        name='rnrevaluation_add'),
    url(r'^rnrevaluation/(?P<pk>\d+)/edit$',
        RNREvaluationUpdate.as_view(),
        name='rnrevaluation_update'),

]
