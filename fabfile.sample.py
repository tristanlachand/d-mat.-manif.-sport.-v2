'''
This file describes all operations related to settting up a server
to host the django application and deploying the same application
after a new release
'''
import os
import time

from fabric.api import env, task, local, cd, run, settings, execute, get

from fabtools import require
from fabtools.python import virtualenv

env.colorize_errors = True

# OWNER = 'USER'  # Pour la plateforme de preproduction
OWNER = 'USER'  # Pour la plateforme de production

# Where lives the entire project
PROJECT_PATH = ''.join(['/home/', OWNER, '/ddcs_loire/'])
# Where project tarballs are uploaded during deployment
PACKAGES_PATH = ''.join([PROJECT_PATH, 'packages/'])
# Where live all releases
RELEASES_PATH = ''.join([PROJECT_PATH, 'releases/'])
# Latest release available
# DO NOT USE BEFORE get_project_sources()
CURRENT_RELEASE_PATH = ''.join([PROJECT_PATH, 'releases/current/'])
# Where live the settings, wsgi files
PROJECT_PACKAGE_PATH = ''.join([PROJECT_PATH, 'releases/current/ddcs_loire/'])
# Where lives user uploaded content
# We should consider importing django settings for that
MEDIA_PATH = '/var/www/ddcs/media/'
# Where lives the virtualenv with all required python packages
VIRTUALENV_PATH = ''.join([PROJECT_PATH, 'env/'])


@task
def preproduction():
    '''
    Set the preproduction server as host
    '''
    env.server = 'preproduction'
    env.hostname = 'HOSTNAME'
    env.hosts = [''.join([OWNER, '@', env.hostname])]
    env.db_name = 'db_name'
    env.db_user = 'db_user'
    env.db_password = 'db_password'


@task
def production():
    '''
    Set the production server as host
    '''
    env.server = 'production'
    env.hostname = 'HOSTNAME'
    env.hosts = [''.join([OWNER, '@', env.hostname])]
    env.db_name = 'db_name'
    env.db_user = 'db_user'
    env.db_password = 'db_password'


@task
def install_deb_packages():
    '''
    Install distribution packages needed to run the application
    '''
    require.deb.packages([
        'python',
        'python-dev',
        'curl',
        'tar',
        'libpq-dev',
        'libtiff4-dev',  # required by Pillow
        'libjpeg8-dev',
        'zlib1g-dev',
        'libfreetype6-dev',
        'liblcms2-dev',
        'libwebp-dev',
    ])


@task
def install_pip():
    '''
    Install pip (required for running a virtualenv)
    '''
    require.python.pip()


@task
def install_virtualenv():
    '''
    Install and creates a virtualenv dedicated to the Django application
    This isolates python packages required by the application from the
    packages of the server
    Avoids crashes when server packages are upgraded
    '''
    require.python.virtualenv(VIRTUALENV_PATH, use_sudo=False)


@task
def install_git():
    '''
    Install git package
    Required to get the project source code
    '''
    require.git.command()


@task
def set_hostname():
    '''
    Set hostname considering the targeted host
    '''
    require.system.hostname(env.hostname)


@task
def install_web_server():
    '''
    Install web server and mod_wsgi required to run a django application
    '''
    require.apache.server()
    require.deb.package('libapache2-mod-wsgi')
    require.apache.disabled('000-default')


@task
def install_database():
    '''
    Install postgresql
    '''
    require.postgres.server()


@task
def setup_directories():
    '''
    Make directories where to place the application and dependencies
    '''
    require.files.directories([PROJECT_PATH, PACKAGES_PATH, RELEASES_PATH],
                              owner=OWNER)


@task
def get_project_sources():
    '''
    Get source code of the application
    For each release, we create a specific directory
    Thereby, it's possible to rollback to a previous release if
    a deployment goes wrong
    '''
    env.release = time.strftime('%Y-%m-%d-%H-%M-%S')
    local('git archive --format=tar %(server)s | gzip > %(release)s.tar.gz'
          % env)
    env.current_release_path = ''.join([RELEASES_PATH, '%(release)s/' % env])
    require.files.directory(env.current_release_path, owner=OWNER)
    require.files.file(''.join([PACKAGES_PATH, '%(release)s.tar.gz' % env]),
                       source='%(release)s.tar.gz' % env)
    with cd(env.current_release_path):
        run('tar -zxf ../../packages/%(release)s.tar.gz' % env)
    local('rm %(release)s.tar.gz' % env)


@task
def install_requirements():
    '''
    Install requirements of the django application
    These packages are installed in the application's virtualenv
    '''
    with virtualenv(VIRTUALENV_PATH):
        require.python.requirements(''.join([env.current_release_path,
                                             'requirements.txt']))


@task
def symlink_current_release():
    '''
    Creates symlinks pointing to the current and previous releases
    Deletes old symlinks
    '''
    with settings(warn_only=True):
        with cd(RELEASES_PATH):
            run('rm -f previous')
            run('mv -f current previous')
            run('ln -s %(release)s current' % env)


@task
def get_django_settings():
    '''
    Get django settings from the developper machine
    Settings are not present on git repository for security reasons
    Sensible information are stored in these settings
    '''
    require.files.file(''.join([PROJECT_PACKAGE_PATH,
                                'settings_%(server)s.py' % env]),
                       source='ddcs_loire/settings_%(server)s.py' % env)
    require.files.file(''.join([PROJECT_PACKAGE_PATH,
                                'settings.py']),
                       source='ddcs_loire/settings.py')
    require.files.file(''.join([PROJECT_PACKAGE_PATH,
                                'settings_openrunner.py']),
                       source='ddcs_loire/settings_openrunner.py')


@task
def create_database():
    '''
    Creates the database with right proper encoding and locale
    Database name, user and password are those from the settings file
    '''
    require.postgres.user(env.db_user, password=env.db_password,
                          encrypted_password=True)
    require.postgres.database(env.db_name,
                              owner=env.db_user,
                              encoding='UTF8',
                              locale='fr_FR.UTF-8')


@task
def collect_static_files():
    '''
    Collects project's static files (enumerated in STATICFILES_DIRS)
    and copy them in the STATIC_ROOT directory. These constants are
    specified in the settings file.
    '''
    with cd(CURRENT_RELEASE_PATH):
        with virtualenv(VIRTUALENV_PATH):
            require.files.directory(env.current_release_path + '/static', owner=OWNER)
            require.files.directory(env.current_release_path + '/media', owner=OWNER)
            run('python manage.py collectstatic -v0 --noinput \
                --settings=ddcs_loire.settings_%(server)s' % env)


@task
def migrate_database():
    '''
    Migrates the database
    '''
    with cd(CURRENT_RELEASE_PATH):
        with virtualenv(VIRTUALENV_PATH):
            run('python manage.py migrate \
                --settings=ddcs_loire.settings_%(server)s' % env)


@task
def create_superuser():
    '''
    Creates superuser for site's admin interface
    '''
    with cd(CURRENT_RELEASE_PATH):
        with virtualenv(VIRTUALENV_PATH):
            run('python manage.py createsuperuser \
                --settings=ddcs_loire.settings_%(server)s' % env)


@task
def setup():
    '''
    Setup a regular server to host the django application
    There should lie all operations needed to be ready to serve the
    application
    '''
    execute(install_deb_packages)

    execute(install_pip)
    execute(install_virtualenv)

    execute(install_git)
    execute(set_hostname)

    execute(install_web_server)
    execute(install_database)

    execute(setup_directories)

    execute(get_project_sources)
    execute(symlink_current_release)
    execute(get_django_settings)
    execute(install_requirements)

    execute(create_database)
    execute(migrate_database)
    execute(create_superuser)


@task()
def disable_site():
    '''
    Disable site in apache.
    '''
    if env.server == 'production':
        require.apache.site_disabled(''.join(['rewrite-', env.hostname]))
    require.apache.site_disabled(env.hostname)


@task()
def enable_mod_ssl():
    '''
    Enable SSL module in apache
    '''
    require.apache.module_enabled('ssl')


@task()
def enable_mod_rewrite():
    '''
    Enable mod_rewrite module in apache
    '''
    require.apache.module_enabled('rewrite')


@task
def enable_site():
    '''
    Enable site in apache with the apache.conf file attached
    '''
    require.files.directory(MEDIA_PATH,
                            owner='www-data',
                            use_sudo=True)
    if env.server == 'production':
        template_source = 'ddcs_loire/apache.conf-ssl'
        port = 443
        require.files.file(
            ''.join(['/etc/apache2/sites-available/',
                     'rewrite-',
                     env.hostname,
                     '.conf']),
            source='ddcs_loire/apache.conf-rewrite',
            use_sudo=True,
        )
        require.apache.site_enabled(''.join(['rewrite-', env.hostname]))
    else:
        template_source = 'ddcs_loire/apache.conf'
        port = 80
    require.apache.site(
        env.hostname,
        template_source=template_source,
        port=port,
        hostname=env.hostname,
        static_files_path=''.join([CURRENT_RELEASE_PATH, 'static/']),
        media_files_path=MEDIA_PATH,
        wsgi_file_path=''.join([PROJECT_PACKAGE_PATH,
                                'wsgi_%(server)s.py' % env]),
        python_path=''.join([VIRTUALENV_PATH, 'lib/python2.7/site-packages/']),
        application_path=CURRENT_RELEASE_PATH,
        project_package=PROJECT_PACKAGE_PATH,
    )


@task
def deploy():
    '''
    Deploy the latest version of the site on the server
    '''
    execute(disable_site)
    if env.server == 'production':
        execute(enable_mod_ssl)
        execute(enable_mod_rewrite)

    execute(get_project_sources)
    execute(symlink_current_release)
    execute(get_django_settings)
    execute(install_requirements)
    execute(collect_static_files)

    execute(enable_site)


@task
def migrate():
    '''
    Performs all unrunned migrations on the database
    '''
    execute(migrate_database)


@task
def deploy_and_migrate():
    '''
    Performs a full deploy, migrations included
    '''
    execute(deploy)
    execute(migrate)


@task
def dump_and_restore():
    # for now, we can't load authorizations
    apps_to_dump = [
        'contenttypes',
        'auth',
        'admin',
        'sports',
        'emergencies',
        'administrative_division',
        'administration',
        'events',
        'authorizations',
        #'agreements',
        'contacts',
        'declarations',
        'protected_areas',
        'evaluations',
        'sites',
        'flatpages',
        #'notifications',
        #'sub_agreements',
    ]

    with cd(CURRENT_RELEASE_PATH):
        with virtualenv(VIRTUALENV_PATH):
            run('mkdir -p fixtures')
            for app_to_dump in apps_to_dump:
                if app_to_dump == 'events':
                    run('python manage.py dumpdata {app} --natural-foreign'
                        '> fixtures/{app}.json '
                        '--settings=ddcs_loire.settings_{server}'.format(app=app_to_dump,
                                                                        server=env.server)
                    )
                else:
                    run('python manage.py dumpdata {app} --natural-foreign '
                        '> fixtures/{app}.json '
                        '--settings=ddcs_loire.settings_{server}'.format(app=app_to_dump,
                                                                        server=env.server)
                    )

    local('rm -f db.sqlite3')
    local('python manage.py migrate')
    local('python manage.py pre_loaddata')
    get(''.join([CURRENT_RELEASE_PATH, 'fixtures']), os.getcwd())
    for app_to_dump in apps_to_dump:
        local('python manage.py loaddata fixtures/{}.json'.format(app_to_dump))
    with cd(CURRENT_RELEASE_PATH):
        with virtualenv(VIRTUALENV_PATH):
            run('rm -rf fixtures/')
    local('rm -rf fixtures/')
