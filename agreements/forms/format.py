# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout

from agreements.models import Avis
from ddcs_loire.utils import GenericForm


class AvisFormatForm(GenericForm):
    """ Formulaire de mise en forme d'avis """

    def __init__(self, *args, **kwargs):
        super(AvisFormatForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
                'favorable',
                'prescriptions',
                'attached_document',
                FormActions(
                        Submit('save', "mettre en forme l'avis".capitalize())
                )
        )

    class Meta:
        model = Avis
        fields = ('favorable', 'prescriptions', 'attached_document')
