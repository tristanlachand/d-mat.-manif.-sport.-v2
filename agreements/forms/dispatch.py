# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout

from agreements.models import CGAvis
from agreements.models import DDSPAvis
from agreements.models import EDSRAvis
from agreements.models import GGDAvis
from agreements.models import SDISAvis
from ddcs_loire.utils import GenericForm


class DDSPAvisDispatchForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    def __init__(self, *args, **kwargs):
        super(DDSPAvisDispatchForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
                'commissariats_concernes',
                FormActions(
                        Submit('save', "envoyer les demandes de pré-avis".capitalize())
                )
        )

    class Meta:
        model = DDSPAvis
        fields = ('commissariats_concernes',)


class EDSRAvisDispatchForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    def __init__(self, *args, **kwargs):
        super(EDSRAvisDispatchForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
                'concerned_cgd',
                FormActions(
                        Submit('save', "envoyer les demandes de pré-avis".capitalize())
                )
        )

    class Meta:
        model = EDSRAvis
        fields = ('concerned_cgd',)


class GGDAvisDispatchForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    def __init__(self, *args, **kwargs):
        super(GGDAvisDispatchForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
                'concerned_cgd',
                'concerned_edsr',
                FormActions(
                        Submit('save', "Envoyer la demande de préavis".capitalize())
                )
        )

    class Meta:
        model = GGDAvis
        fields = ('concerned_cgd', 'concerned_edsr')


class SDISAvisDispatchForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    def __init__(self, *args, **kwargs):
        super(SDISAvisDispatchForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
                'compagnies_concernees',
                FormActions(
                        Submit('save', "envoyer les demandes de pré-avis".capitalize())
                )
        )

    class Meta:
        model = SDISAvis
        fields = ("compagnies_concernees",)


class CGAvisDispatchForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    def __init__(self, *args, **kwargs):
        super(CGAvisDispatchForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
                'concerned_services',
                FormActions(
                        Submit('save', "envoyer les demandes de pré-avis".capitalize())
                )
        )

    class Meta:
        model = CGAvis
        fields = ['concerned_services']
