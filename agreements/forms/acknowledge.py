# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout

from agreements.models import Avis
from ddcs_loire.utils import GenericForm


class AvisAcknowledgeForm(GenericForm):
    """ Formulaire de rendu d'avis (acknowledge) """

    def __init__(self, *args, **kwargs):
        super(AvisAcknowledgeForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
                'favorable',
                'prescriptions',
                'attached_document',
                FormActions(Submit('save', "rendre l'avis".capitalize()))
        )

    class Meta:
        model = Avis
        fields = ['favorable', 'prescriptions', 'attached_document']
