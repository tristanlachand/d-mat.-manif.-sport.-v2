# coding: utf-8
from django.apps import AppConfig


class AgreementsConfig(AppConfig):
    """ Configuration de l'application """
    name = 'agreements'
    verbose_name = "Avis"

    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        from agreements import listeners


default_app_config = 'agreements.AgreementsConfig'
