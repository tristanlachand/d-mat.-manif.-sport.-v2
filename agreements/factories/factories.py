from __future__ import unicode_literals

import factory

from administration.factories import ServiceFactory
from administrative_division.factories import CommuneFactory
from agreements.models import GGDAvis
from authorizations.factories import EventAuthorizationFactory
from agreements.models import Avis
from agreements.models import CGAvis
from agreements.models import DDSPAvis
from agreements.models import EDSRAvis
from agreements.models import FederationAvis
from agreements.models import SDISAvis
from agreements.models import ServiceAvis
from agreements.models import MairieAvis


class AgreementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Avis

    authorization = factory.SubFactory(EventAuthorizationFactory)


class FederalAgreementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FederationAvis

    authorization = factory.SubFactory(EventAuthorizationFactory)


class ServiceAgreementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ServiceAvis

    authorization = factory.SubFactory(EventAuthorizationFactory)
    service = factory.SubFactory(ServiceFactory)


class TownHallAgreementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MairieAvis

    authorization = factory.SubFactory(EventAuthorizationFactory)
    commune = factory.SubFactory(CommuneFactory)


class SDISAgreementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SDISAvis

    authorization = factory.SubFactory(EventAuthorizationFactory)


class DDSPAgreementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DDSPAvis

    authorization = factory.SubFactory(EventAuthorizationFactory)


class EDSRAgreementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = EDSRAvis

    authorization = factory.SubFactory(EventAuthorizationFactory)


class CGAgreementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CGAvis

    authorization = factory.SubFactory(EventAuthorizationFactory)


class GGDAgreementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = GGDAvis

    authorization = factory.SubFactory(EventAuthorizationFactory)
