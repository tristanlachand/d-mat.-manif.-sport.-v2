# coding: utf-8
from __future__ import unicode_literals

import datetime

from django.conf import settings
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils.translation import ugettext_lazy as _

from administration.factories import InstructeurFactory, GGDFactory
from administration.factories import FederationAgentFactory
from administration.factories import PrefectureFactory
from administration.factories import ServiceFactory
from administration.factories import ServiceAgentFactory
from administration.factories import MairieAgentFactory
from administration.factories import CGFactory
from administration.factories import EDSRFactory
from administration.factories import SDISFactory
from administration.factories import DDSPFactory

from administrative_division.factories import CommuneFactory
from administrative_division.factories import ArrondissementFactory
from administrative_division.factories import DepartementFactory
from agreements.factories import GGDAgreementFactory

from authorizations.factories import EventAuthorizationFactory
from core.util.application import get_instance_settings

from events.factories import AutorisationNMFactory

from sports.factories import ActiviteFactory

from .factories import AgreementFactory
from .factories import FederalAgreementFactory
from .factories import ServiceAgreementFactory
from .factories import TownHallAgreementFactory
from .factories import SDISAgreementFactory
from .factories import DDSPAgreementFactory
from .factories import EDSRAgreementFactory
from .factories import CGAgreementFactory


class AgreementMethodTests(TestCase):

    def test_get_deadline(self):
        avis = AgreementFactory.create()
        self.assertEqual(avis.get_deadline(),
                         datetime.date.today() + settings.AGREEMENT_DELAY)

    def test_delay_not_exceeded(self):
        avis = AgreementFactory.create()
        self.assertEqual(avis.delay_exceeded(), False)

    def test_delay_exceeded(self):
        avis = AgreementFactory.create()
        avis.request_date = datetime.date.today() - settings.AGREEMENT_DELAY - datetime.timedelta(days=2)
        self.assertEqual(avis.delay_exceeded(), True)

    def test_preavis_validated(self):
        avis = AgreementFactory.create()
        self.assertTrue(avis.preavis_validated())


class FederalAgreementMethodTests(TestCase):

    def test_str_(self):
        avis = FederalAgreementFactory.create()
        manifestation = avis.get_manifestation()
        self.assertEqual(
            avis.__str__(),
            ' - '.join([
                manifestation.__str__(),
                manifestation.activite.discipline.federation.__str__()
            ])
        )

    def test_get_absolute_url(self):
        avis = FederalAgreementFactory.create()
        self.assertEqual(avis.get_absolute_url(),
                         reverse('agreements:federal_agreement_detail',
                                 kwargs={'pk': avis.pk}))

    def test_get_deadline(self):
        avis = FederalAgreementFactory.create()
        self.assertEqual(avis.get_deadline(),
                         datetime.date.today() + settings.FEDERAL_AGREEMENT_DELAY)

    def test_creation(self):
        authorization = EventAuthorizationFactory.create()
        self.assertEqual(authorization.aviss.all().count(), 1)
        authorization.save()
        self.assertEqual(authorization.aviss.all().count(), 1)

    def test_life_cycle(self):
        activite = ActiviteFactory.create()
        _federal_agent = FederationAgentFactory.create(
            federation=activite.discipline.federation
        )
        city = CommuneFactory.create()
        prefecture = PrefectureFactory.create(
            arrondissement=city.arrondissement,
        )
        _instructor = InstructeurFactory(prefecture=prefecture)
        manifestation = AutorisationNMFactory.create(activite=activite,
                                                          departure_city=city)
        authorization = EventAuthorizationFactory.create(
            manifestation=manifestation
        )
        for agent in manifestation.activite.discipline.federation.federationagents.all():
            self.assertEqual(agent.user.notifications.all().count(), 1)
        authorization.aviss.first().federationavis.save()
        for agent in manifestation.activite.discipline.federation.federationagents.all():
            self.assertEqual(agent.user.notifications.all().count(), 1)
        # test dispatch
        f_agreement = authorization.aviss.first().federationavis
        f_agreement.acknowledge()
        f_agreement.save()
        self.assertEqual(f_agreement.reply_date, datetime.date.today())
        # test federal agent action created
        for agent in manifestation.activite.discipline.federation.federationagents.all():
            self.assertEqual(agent.user.actions.all().count(), 1)
            self.assertEqual(agent.user.actions.first().action,
                             'avis rendu')
        # test instructor notification created
        for agent in prefecture.instructeurs.all():
            self.assertEqual(agent.user.notifications.all().count(), 2)
            self.assertEqual(
                agent.user.notifications.last().content_object,
                activite.discipline.federation,
            )


class ServiceAgreementMethodTests(TestCase):

    def test_str_(self):
        avis = ServiceAgreementFactory.create()
        manifestation = avis.get_manifestation()
        self.assertEqual(
            avis.__str__(),
            ' - '.join([
                manifestation.__str__(),
                avis.service.__str__()
            ])
        )

    def test_get_absolute_url(self):
        avis = ServiceAgreementFactory.create()
        self.assertEqual(avis.get_absolute_url(),
                         reverse('agreements:service_agreement_detail',
                                 kwargs={'pk': avis.pk}))

    def test_creation(self):
        authorization = EventAuthorizationFactory.create()
        self.assertEqual(authorization.aviss.all().count(), 1)
        service = ServiceFactory.create()
        authorization.concerned_services.add(service)
        self.assertEqual(authorization.aviss.all().count(), 2)

    def test_life_cycle(self):
        # test notify after creation
        service = ServiceFactory.create()
        _service_agent = ServiceAgentFactory.create(service=service)
        authorization = EventAuthorizationFactory.create()
        city = authorization.manifestation.departure_city
        prefecture = PrefectureFactory.create(
            arrondissement=city.arrondissement,
        )
        _instructor = InstructeurFactory(prefecture=prefecture)
        avis = ServiceAgreementFactory.create(service=service,
                                                   authorization=authorization)
        for agent in avis.service.serviceagents.all():
            self.assertEqual(agent.user.notifications.all().count(), 1)
        avis.save()
        for agent in avis.service.serviceagents.all():
            self.assertEqual(agent.user.notifications.all().count(), 1)
        # test dispatch
        avis.acknowledge()
        self.assertEqual(avis.reply_date, datetime.date.today())
        # test service agent action created
        for agent in avis.service.serviceagents.all():
            self.assertEqual(agent.user.actions.all().count(), 1)
            self.assertEqual(agent.user.actions.first().action,
                             'avis rendu')
        # test instructor notification created
        for agent in prefecture.instructeurs.all():
            self.assertEqual(agent.user.notifications.all().count(), 1)
            self.assertEqual(
                agent.user.notifications.first().content_object,
                avis.service,
            )


class TownHallAgreementMethodTests(TestCase):

    def test_str_(self):
        avis = TownHallAgreementFactory.create()
        manifestation = avis.get_manifestation()
        self.assertEqual(
            avis.__str__(),
            ' - '.join([
                manifestation.__str__(),
                avis.commune.__str__()
            ])
        )

    def test_get_absolute_url(self):
        avis = TownHallAgreementFactory.create()
        self.assertEqual(avis.get_absolute_url(),
                         reverse('agreements:townhall_agreement_detail',
                                 kwargs={'pk': avis.pk}))

    def test_creation(self):
        authorization = EventAuthorizationFactory.create()
        self.assertEqual(authorization.aviss.all().count(), 1)
        authorization.concerned_cities.add(
            CommuneFactory.create()
        )
        self.assertEqual(authorization.aviss.all().count(), 2)

    def test_life_cycle(self):
        # test notify after creation
        commune = CommuneFactory.create()
        _townhall_agent = MairieAgentFactory.create(commune=commune)
        authorization = EventAuthorizationFactory.create()
        city = authorization.manifestation.departure_city
        prefecture = PrefectureFactory.create(
            arrondissement=city.arrondissement,
        )
        _instructor = InstructeurFactory(prefecture=prefecture)
        avis = TownHallAgreementFactory.create(
            commune=commune,
            authorization=authorization,
        )
        for agent in avis.commune.mairieagents.all():
            self.assertEqual(agent.user.notifications.all().count(), 1)
        avis.save()
        for agent in avis.commune.mairieagents.all():
            self.assertEqual(agent.user.notifications.all().count(), 1)
        # test dispatch
        avis.acknowledge()
        self.assertEqual(avis.reply_date, datetime.date.today())
        # test townhall agent action created
        for agent in avis.commune.mairieagents.all():
            self.assertEqual(agent.user.actions.all().count(), 1)
            self.assertEqual(agent.user.actions.first().action,
                             'avis rendu')
        # test instructor notification created
        for agent in prefecture.instructeurs.all():
            self.assertEqual(agent.user.notifications.all().count(), 1)
            self.assertEqual(
                agent.user.notifications.first().content_object,
                avis.commune,
            )


def create_tree():
    departement = DepartementFactory.create()
    _sdis = SDISFactory.create(departement=departement)
    _edsr = EDSRFactory.create(departement=departement)
    _ggd = GGDFactory(departement=departement)
    _ddsp = DDSPFactory.create(departement=departement)
    _council = CGFactory.create(departement=departement)
    arrondissement = ArrondissementFactory.create(departement=departement)
    city = CommuneFactory.create(arrondissement=arrondissement)
    manifestation = AutorisationNMFactory.create(departure_city=city)
    authorization = EventAuthorizationFactory.create(manifestation=manifestation)
    return authorization, departement


class SDISAgreementMethodTests(TestCase):

    def test_get_absolute_url(self):
        authorization, _ = create_tree()
        avis = SDISAgreementFactory.create(
            authorization = authorization
        )
        self.assertEqual(avis.get_absolute_url(),
                         reverse('agreements:sdis_agreement_detail',
                                 kwargs={'pk': avis.pk}))

    def test_preavis_validated(self):
        authorization, _ = create_tree()
        avis = SDISAgreementFactory.create(
            authorization=authorization
        )
        self.assertTrue(avis.preavis_validated())

    def test_creation(self):
        authorization, _ = create_tree()
        authorization.sdis_concerned = True
        authorization.save()
        self.assertEqual(authorization.get_avis().count(), 2)


class EDSRAgreementMethodTests(TestCase):

    def test_get_absolute_url(self):
        authorization, _ = create_tree()
        avis = EDSRAgreementFactory.create(
            authorization=authorization
        )
        self.assertEqual(avis.get_absolute_url(),
                         reverse('agreements:edsr_agreement_detail',
                                 kwargs={'pk': avis.pk}))

    def test_preavis_validated(self):
        authorization, _ = create_tree()
        avis = EDSRAgreementFactory.create(
            authorization=authorization
        )
        self.assertTrue(avis.preavis_validated())

    def test_creation(self):
        authorization, _ = create_tree()
        authorization.edsr_concerned = True
        authorization.save()
        print(', '.join([str(a) for a in authorization.aviss.all()]))
        # Le nombre d'avis va dépendre de la config en cours.
        # L'avis EDSR peut être un préavis, ce qui réduit le nombre d'avis à 1.
        if get_instance_settings('EDSR_OR_GGD').lower() != 'ggd_subagentedsr':
            self.assertEqual(authorization.aviss.all().count(), 2)
        else:
            self.assertEqual(authorization.aviss.all().count(), 1)

class GGDAgreementMethodTests(TestCase):
    """
    Tests des avis GGD (workflow 7)
    """

    def test_get_absolute_url(self):
        authorization, departement = create_tree()
        avis = GGDAgreementFactory.create(
            authorization=authorization,
            concerned_edsr=departement.edsr
        )
        self.assertEqual(avis.get_absolute_url(),
                         reverse('agreements:ggd_agreement_detail',
                                 kwargs={'pk': avis.pk}))

    def test_preavis_validated(self):
        authorization, departement = create_tree()
        avis = GGDAgreementFactory.create(
            authorization=authorization, concerned_edsr=departement.edsr
        )
        # Les pré-avis ne sont pas tous validés pour une bonne raison :
        # les pré-avis CGD ne sont pas créés par défaut (pas de CGD ajouté)
        # mais un pré-avis EDSR l'est avec l'état created (!= acknowledged)

        # Dans une config sans préavis, c'est automatiquement True
        if get_instance_settings('EDSR_OR_GGD') == 'GGD_SubAgentEDSR':
            self.assertFalse(avis.preavis_validated())
        elif get_instance_settings('EDSR_OR_GGD') in ('EDSR', 'GGD_AgentEDSR'):
            self.assertTrue(avis.preavis_validated())

    def test_creation(self):
        authorization, _ = create_tree()
        authorization.ggd_concerned = True
        authorization.save()
        self.assertEqual(authorization.aviss.all().count(), 2)


class DDSPAgreementMethodTests(TestCase):

    def test_get_absolute_url(self):
        authorization, _ = create_tree()
        avis = DDSPAgreementFactory.create(
            authorization=authorization,
        )
        self.assertEqual(avis.get_absolute_url(),
                         reverse('agreements:ddsp_agreement_detail',
                                 kwargs={'pk': avis.pk}))

    def test_preavis_validated(self):
        authorization, _ = create_tree()
        avis = DDSPAgreementFactory.create(
            authorization=authorization,
        )
        self.assertTrue(avis.preavis_validated())

    def test_creation(self):
        authorization, _ = create_tree()
        authorization.ddsp_concerned = True
        authorization.save()
        self.assertEqual(authorization.aviss.all().count(), 2)


class CGAgreementMethodTests(TestCase):

    def test_get_absolute_url(self):
        authorization, _ = create_tree()
        avis = CGAgreementFactory.create(
            authorization=authorization
        )
        self.assertEqual(avis.get_absolute_url(),
                         reverse('agreements:cg_agreement_detail',
                                 kwargs={'pk': avis.pk}))

    def test_preavis_validated(self):
        authorization, _ = create_tree()
        avis = CGAgreementFactory.create(
            authorization=authorization,
        )
        self.assertTrue(avis.preavis_validated())

    def test_creation(self):
        authorization, _ = create_tree()
        authorization.cg_concerned = True
        authorization.save()
        self.assertEqual(authorization.aviss.all().count(), 2)
