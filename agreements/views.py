# coding: utf-8
from __future__ import unicode_literals

from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView, View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import UpdateView

from agreements.forms import GGDAvisDispatchForm
from agreements.models import GGDAvis
from agreements.util.decorators import agent_required
from agreements.util.decorators import cg_agent_required
from agreements.util.decorators import cg_required
from agreements.util.decorators import cg_superior_required
from agreements.util.decorators import ddsp_agent_required
from agreements.util.decorators import edsr_agent_required
from agreements.util.decorators import federal_agent_required
from agreements.util.decorators import fireman_required
from agreements.util.decorators import gendarmerie_agent_required
from agreements.util.decorators import ggd_agent_required
from agreements.util.decorators import sdis_agent_required
from agreements.util.decorators import service_agent_required
from agreements.util.decorators import townhall_agent_required
from authorizations.decorators import instructeur_required
from ddcs_loire.utils import get_edsr_or_ggd_config
from .forms import AvisAcknowledgeForm
from .forms import AvisFormatForm
from .forms import CGAvisDispatchForm
from .forms import DDSPAvisDispatchForm
from .forms import EDSRAvisDispatchForm
from .forms import SDISAvisDispatchForm
from .models import Avis
from .models import CGAvis
from .models import DDSPAvis
from .models import EDSRAvis
from .models import FederationAvis
from .models import MairieAvis
from .models import SDISAvis
from .models import ServiceAvis


class Dashboard(ListView):
    model = Avis

    @method_decorator(agent_required)
    def dispatch(self, *args, **kwargs):
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        agent = self.request.user.agent
        config = get_edsr_or_ggd_config()
        # Que doit voir un agent dans son dashboard ?
        results = {'federationagent': FederationAvis.objects.to_process().for_federation(agent.federationagent.federation) if hasattr(agent, 'federationagent') else None,
                   'serviceagent': ServiceAvis.objects.to_process().filter(service=agent.serviceagent.service) if hasattr(agent, 'serviceagent') else None,
                   'mairieagent': MairieAvis.objects.to_process().filter(commune=agent.mairieagent.commune) if hasattr(agent, 'mairieagent') else
                   None,
                   'edsragent': EDSRAvis.objects.to_process() if config == 'edsr' else GGDAvis.objects.to_process() if config == 'ggd_agentedsr' else
                   EDSRAvis.objects.none(),
                   'sdisagent': SDISAvis.objects.to_process(),
                   'ddspagent': DDSPAvis.objects.to_process(),
                   'ggdagent': GGDAvis.objects.to_process() if config != 'edsr' else EDSRAvis.objects.to_acknowledge(),
                   'cgagent': CGAvis.objects.to_process(),
                   'cgsuperieuragent': CGAvis.objects.to_process().to_acknowledge(),
                   'codisagent': SDISAvis.objects.to_process().acknowledged(),
                   'cisagent': SDISAvis.objects.to_process().for_fire_service(agent.cisagent.cis).acknowledged() if hasattr(agent, 'cisagent') else None,
                   'brigadeagent': EDSRAvis.objects.to_process() if config == 'edsr' else GGDAvis.objects.to_process() if config == 'ggd_agentedsr' or config == 'ggd_subagentedsr' else None
                   }
        # Parcourir les clés disponibles, en tant qu'attributs de agent
        # Si un attribut ne provoque pas d'erreur, renvoyer le queryset approprié
        # Rappel: Les querysets ne sont pas évalués même lorsqu'ils sont retournés
        for agentname in results:
            try:
                getattr(agent, agentname)
                return results[agentname]
            except ObjectDoesNotExist:
                pass

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        return context


class BaseAcknowledgeView(UpdateView):
    model = Avis
    form_class = AvisAcknowledgeForm
    template_name_suffix = '_ack_form'

    def form_valid(self, form):
        if form.instance.state == 'acknowledged':
            info_message = "Cet avis a déjà été validé. Il n'y a nul besoin de recommencer."
            messages.error(self.request, info_message)
            return HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.acknowledge()
            return super(BaseAcknowledgeView, self).form_valid(form)


class BaseFormatView(UpdateView):
    model = Avis
    form_class = AvisFormatForm
    template_name_suffix = '_format_form'

    def form_valid(self, form):
        if form.instance.state == 'formatted':
            info_message = "Cet avis a déjà été mis en forme. Il n'y a nul besoin de recommencer."
            messages.error(self.request, info_message)
            return HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.format()
            return super(BaseFormatView, self).form_valid(form)


class BaseDispatchView(UpdateView):
    template_name_suffix = '_dispatch_form'

    def form_valid(self, form):
        if form.instance.state == 'dispatched':
            info_message = "Les demandes de pré-avis ont déjà été envoyées. Il n'y a nul besoin de recommencer."
            messages.error(self.request, info_message)
            return HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.dispatch()
            return super(BaseDispatchView, self).form_valid(form)


class BaseResendView(SingleObjectMixin, View):
    model = Avis

    @method_decorator(instructeur_required)
    def dispatch(self, *args, **kwargs):
        return super(BaseResendView, self).dispatch(*args, **kwargs)


###########################################################
# Service Agreements Views
###########################################################
class ServiceAgreementDetail(DetailView):
    model = ServiceAvis

    @method_decorator(service_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(ServiceAgreementDetail, self).dispatch(*args, **kwargs)


class ServiceAgreementAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = ServiceAvis

    @method_decorator(service_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(ServiceAgreementAcknowledge, self).dispatch(*args,
                                                                 **kwargs)


class ServiceAgreementResend(BaseResendView):
    model = ServiceAvis

    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        avis.notify_creation(agents=avis.service.serviceagents.all())
        avis.log_resend(recipient=avis.service.__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect('/authorizations/{id}'.format(id=avis.authorization.id))


###########################################################
# Federal Agreements Views
###########################################################
class FederalAgreementDetail(DetailView):
    model = FederationAvis

    @method_decorator(federal_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(FederalAgreementDetail, self).dispatch(*args, **kwargs)


class FederalAgreementAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = FederationAvis

    @method_decorator(federal_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(FederalAgreementAcknowledge, self).dispatch(*args,
                                                                 **kwargs)


class FederalAgreementResend(BaseResendView):
    model = FederationAvis

    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        avis.notify_creation(agents=avis.authorization.manifestation.activite.discipline.federation.federationagents.all())
        avis.log_resend(recipient=avis.get_federation().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect('/authorizations/{id}'.format(id=avis.authorization.id))


###########################################################
# TownHall Agreements Views
###########################################################
class TownHallAgreementDetail(DetailView):
    model = MairieAvis

    @method_decorator(townhall_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(TownHallAgreementDetail, self).dispatch(*args, **kwargs)


class TownHallAgreementAcknowledge(BaseAcknowledgeView):  # noqa pylint: disable=R0901
    model = MairieAvis

    @method_decorator(townhall_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(TownHallAgreementAcknowledge,
                     self).dispatch(*args, **kwargs)


class TownHallAgreementResend(BaseResendView):
    model = MairieAvis

    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        avis.notify_creation(agents=avis.commune.mairieagents.all())
        avis.log_resend(recipient=avis.commune.__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect('/authorizations/{id}'.format(id=avis.authorization.id))


###########################################################
# EDSR Agreements Views
###########################################################
class EDSRAgreementDetail(DetailView):
    model = EDSRAvis

    @method_decorator(gendarmerie_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(EDSRAgreementDetail, self).dispatch(*args, **kwargs)


class EDSRAgreementDispatch(BaseDispatchView):
    model = EDSRAvis
    form_class = EDSRAvisDispatchForm

    @method_decorator(edsr_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(EDSRAgreementDispatch, self).dispatch(*args, **kwargs)


class EDSRAgreementFormat(BaseFormatView):
    model = EDSRAvis

    @method_decorator(edsr_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(EDSRAgreementFormat, self).dispatch(*args, **kwargs)


class EDSRAgreementAcknowledge(BaseAcknowledgeView):
    model = EDSRAvis

    @method_decorator(ggd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(EDSRAgreementAcknowledge, self).dispatch(*args, **kwargs)


class EDSRAgreementResend(BaseResendView):
    model = EDSRAvis

    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        avis.notify_creation(agents=avis.authorization.manifestation.departure_city.get_departement().edsr.edsragents.all())
        avis.log_resend(recipient=avis.get_edsr().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect('/authorizations/{id}'.format(id=avis.authorization.id))


###########################################################
# GGD Agreements Views
###########################################################
class GGDAgreementDetail(DetailView):
    model = GGDAvis

    @method_decorator(gendarmerie_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(GGDAgreementDetail, self).dispatch(*args, **kwargs)


class GGDAgreementDispatch(BaseDispatchView):
    model = GGDAvis
    form_class = GGDAvisDispatchForm

    @method_decorator(ggd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(GGDAgreementDispatch, self).dispatch(*args, **kwargs)


class GGDAgreementFormat(BaseFormatView):
    model = GGDAvis

    @method_decorator(edsr_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(GGDAgreementFormat, self).dispatch(*args, **kwargs)


class GGDAgreementAcknowledge(BaseAcknowledgeView):
    model = GGDAvis

    @method_decorator(ggd_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(GGDAgreementAcknowledge, self).dispatch(*args, **kwargs)


class GGDAgreementResend(BaseResendView):
    model = GGDAvis

    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        avis.notify_creation(agents=avis.authorization.manifestation.departure_city.get_departement().ggd.ggdagents.all())
        avis.log_resend(recipient=avis.get_ggd().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect('/authorizations/{id}'.format(id=avis.authorization.id))


###########################################################
# SDIS Agreements Views
###########################################################
class SDISAgreementDetail(DetailView):
    model = SDISAvis

    @method_decorator(fireman_required)
    def dispatch(self, *args, **kwargs):
        return super(SDISAgreementDetail, self).dispatch(*args, **kwargs)


class SDISAgreementDispatch(BaseDispatchView):
    model = SDISAvis
    form_class = SDISAvisDispatchForm

    @method_decorator(sdis_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(SDISAgreementDispatch, self).dispatch(*args, **kwargs)


class SDISAgreementAcknowledge(BaseAcknowledgeView):
    model = SDISAvis

    @method_decorator(sdis_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(SDISAgreementAcknowledge, self).dispatch(*args, **kwargs)


class SDISAgreementResend(BaseResendView):
    model = SDISAvis

    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        avis.notify_creation(agents=avis.authorization.manifestation.departure_city.get_departement().sdis.sdisagents.all())
        avis.log_resend(recipient=avis.get_sdis().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect('/authorizations/{id}'.format(id=avis.authorization.id))


###########################################################
# DDSP Agreements Views
###########################################################
class DDSPAgreementDetail(DetailView):
    model = DDSPAvis

    @method_decorator(ddsp_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(DDSPAgreementDetail, self).dispatch(*args, **kwargs)


class DDSPAgreementDispatch(BaseDispatchView):
    model = DDSPAvis
    form_class = DDSPAvisDispatchForm

    @method_decorator(ddsp_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(DDSPAgreementDispatch, self).dispatch(*args, **kwargs)


class DDSPAgreementAcknowledge(BaseAcknowledgeView):
    model = DDSPAvis

    @method_decorator(ddsp_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(DDSPAgreementAcknowledge, self).dispatch(*args, **kwargs)


class DDSPAgreementResend(BaseResendView):
    model = DDSPAvis

    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        avis.notify_creation(agents=avis.authorization.manifestation.departure_city.get_departement().ddsp.ddspagents.all())
        avis.log_resend(recipient=avis.get_ddsp().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect('/authorizations/{id}'.format(id=avis.authorization.id))


###########################################################
# CG Agreements Views
###########################################################
class CGAgreementDetail(DetailView):
    model = CGAvis

    @method_decorator(cg_required)
    def dispatch(self, *args, **kwargs):
        return super(CGAgreementDetail, self).dispatch(*args, **kwargs)


class CGAgreementDispatch(BaseDispatchView):
    model = CGAvis
    form_class = CGAvisDispatchForm

    @method_decorator(cg_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CGAgreementDispatch, self).dispatch(*args, **kwargs)


class CGAgreementFormat(BaseFormatView):
    model = CGAvis

    @method_decorator(cg_agent_required)
    def dispatch(self, *args, **kwargs):
        return super(CGAgreementFormat, self).dispatch(*args, **kwargs)


class CGAgreementAcknowledge(BaseAcknowledgeView):
    model = CGAvis

    @method_decorator(cg_superior_required)
    def dispatch(self, *args, **kwargs):
        return super(CGAgreementAcknowledge, self).dispatch(*args, **kwargs)


class CGAgreementResend(BaseResendView):
    model = CGAvis

    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        avis.notify_creation(agents=avis.authorization.manifestation.departure_city.get_departement().cg.cgagents.all())
        avis.log_resend(recipient=avis.get_cg().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect('/authorizations/{id}'.format(id=avis.authorization.id))
