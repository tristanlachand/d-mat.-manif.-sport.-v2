# coding: utf-8
from __future__ import unicode_literals

from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver

from administration.models import Service
from administrative_division.models import Commune
from agreements.models.cg import CGAvis
from agreements.models.ddsp import DDSPAvis
from agreements.models.edsr import EDSRAvis
from agreements.models.federation import FederationAvis
from agreements.models.ggd import GGDAvis
from agreements.models.mairie import MairieAvis
from agreements.models.sdis import SDISAvis
from agreements.models.service import ServiceAvis
from authorizations.models import ManifestationAutorisation
from authorizations.signals import authorization_published
from core.util.application import get_instance_settings


@receiver(m2m_changed, sender=ManifestationAutorisation.concerned_services.through)
def create_service_agreements(instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            ServiceAvis.objects.get_or_create(authorization=instance, service=Service.objects.get(pk=pk))


@receiver(m2m_changed, sender=ManifestationAutorisation.concerned_cities.through)
def create_townhall_agreements(instance, action, pk_set, **kwargs):
    if action == 'post_add':
        for pk in pk_set:
            MairieAvis.objects.get_or_create(
                authorization=instance,
                commune=Commune.objects.get(pk=pk)
            )


@receiver(post_save, sender=ManifestationAutorisation)
def create_federal_agreement(created, instance, **kwargs):
    if created:
        FederationAvis.objects.create(authorization=instance)


@receiver(post_save, sender=ManifestationAutorisation)
def create_edsr_agreement(created, instance, **kwargs):
    if instance.edsr_concerned:
        if get_instance_settings('EDSR_OR_GGD').lower() in {'ggd_agentedsr', 'edsr'}:
            EDSRAvis.objects.get_or_create(authorization=instance)


@receiver(post_save, sender=ManifestationAutorisation)
def create_ggd_agreement(created, instance, **kwargs):
    if instance.ggd_concerned:
        departement = instance.manifestation.departure_city.get_departement()
        edsr = departement.edsr
        GGDAvis.objects.get_or_create(authorization=instance, concerned_edsr=edsr)


@receiver(post_save, sender=ManifestationAutorisation)
def create_sdis_agreement(created, instance, **kwargs):
    if instance.sdis_concerned:
        SDISAvis.objects.get_or_create(authorization=instance)


@receiver(post_save, sender=ManifestationAutorisation)
def create_ddsp_agreement(created, instance, **kwargs):
    if instance.ddsp_concerned:
        DDSPAvis.objects.get_or_create(authorization=instance)


@receiver(post_save, sender=ManifestationAutorisation)
def create_cg_agreement(created, instance, **kwargs):
    if instance.cg_concerned:
        CGAvis.objects.get_or_create(authorization=instance)
