# coding: utf-8
from __future__ import unicode_literals

from django.db.models.signals import post_save
from django.dispatch import receiver

from agreements.models.cg import CGAvis
from agreements.models.ddsp import DDSPAvis
from agreements.models.edsr import EDSRAvis
from agreements.models.federation import FederationAvis
from agreements.models.ggd import GGDAvis
from agreements.models.mairie import MairieAvis
from agreements.models.sdis import SDISAvis
from agreements.models.service import ServiceAvis
from authorizations.models import ManifestationAutorisation
from authorizations.signals import authorization_published


@receiver(post_save, sender=ServiceAvis)
def notify_service_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(post_save, sender=MairieAvis)
def notify_townhall_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(post_save, sender=FederationAvis)
def notify_federal_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(post_save, sender=EDSRAvis)
def notify_edsr_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(post_save, sender=GGDAvis)
def notify_ggd_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(post_save, sender=SDISAvis)
def notify_sdis_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents())


@receiver(post_save, sender=DDSPAvis)
def notify_ddsp_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 institutional=instance.get_ddsp())


@receiver(post_save, sender=CGAvis)
def notify_cg_agreement(created, instance, **kwargs):
    if created:
        instance.notify_creation(agents=instance.get_agents(),
                                 institutional=instance.get_cg())


@receiver(authorization_published, sender=ManifestationAutorisation)
def notify_authorization_publication(instance, **kwargs):
    for avis in ServiceAvis.objects.filter(authorization=instance):
        avis.notify_publication(agents=avis.get_agents())
    for avis in MairieAvis.objects.filter(authorization=instance):
        avis.notify_publication(agents=avis.get_agents())
    for avis in FederationAvis.objects.filter(authorization=instance):
        avis.notify_publication(agents=avis.get_agents())
    for avis in EDSRAvis.objects.filter(authorization=instance):
        avis.notify_publication(agents=avis.get_agents())
    for avis in SDISAvis.objects.filter(authorization=instance):
        avis.notify_publication(agents=avis.get_agents())
    for avis in DDSPAvis.objects.filter(authorization=instance):
        avis.notify_publication(agents=avis.get_agents(),
                                institutional=avis.get_ddsp())
    for avis in CGAvis.objects.filter(authorization=instance):
        avis.notify_publication(agents=avis.get_agents(),
                                institutional=avis.get_cg())
    for avis in GGDAvis.objects.filter(authorization=instance):
        avis.notify_publication(agents=avis.get_agents())
