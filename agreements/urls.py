from __future__ import unicode_literals

from django.conf.urls import url

from .views import *


urlpatterns = [
    url(r'^dashboard/$', Dashboard.as_view(), name='dashboard'),

    # Service Agreements
    url(r'^service/(?P<pk>\d+)/$', ServiceAgreementDetail.as_view(), name='service_agreement_detail'),
    url(r'^service/(?P<pk>\d+)/acknowledge/$', ServiceAgreementAcknowledge.as_view(), name='service_agreement_acknowledge'),
    url(r'^service/(?P<pk>\d+)/resend/$', ServiceAgreementResend.as_view(), name='service_agreement_resend'),
    # Federal Agreements
    url(r'^federation/(?P<pk>\d+)/$', FederalAgreementDetail.as_view(), name='federal_agreement_detail'),
    url(r'^federation/(?P<pk>\d+)/acknowledge/$', FederalAgreementAcknowledge.as_view(), name='federal_agreement_acknowledge'),
    url(r'^federation/(?P<pk>\d+)/resend/$', FederalAgreementResend.as_view(), name='federal_agreement_resend'),
    # TownHall Agreements
    url(r'^townhall/(?P<pk>\d+)/$', TownHallAgreementDetail.as_view(), name='townhall_agreement_detail'),
    url(r'^townhall/(?P<pk>\d+)/acknowledge/$', TownHallAgreementAcknowledge.as_view(), name='townhall_agreement_acknowledge'),
    url(r'^townhall/(?P<pk>\d+)/resend/$', TownHallAgreementResend.as_view(), name='townhall_agreement_resend'),
    # EDSR Agreements
    url(r'^edsr/(?P<pk>\d+)/dispatch/$', EDSRAgreementDispatch.as_view(), name='edsr_agreement_dispatch'),
    url(r'^edsr/(?P<pk>\d+)/$', EDSRAgreementDetail.as_view(), name='edsr_agreement_detail'),
    url(r'^edsr/(?P<pk>\d+)/format/$', EDSRAgreementFormat.as_view(), name='edsr_agreement_format'),
    url(r'^edsr/(?P<pk>\d+)/acknowledge/$', EDSRAgreementAcknowledge.as_view(), name='edsr_agreement_acknowledge'),
    url(r'^edsr/(?P<pk>\d+)/resend/$', EDSRAgreementResend.as_view(), name='edsr_agreement_resend'),
    # GGD Agreements
    url(r'^ggd/(?P<pk>\d+)/dispatch/$', GGDAgreementDispatch.as_view(), name='ggd_agreement_dispatch'),
    url(r'^ggd/(?P<pk>\d+)/$', GGDAgreementDetail.as_view(), name='ggd_agreement_detail'),
    url(r'^ggd/(?P<pk>\d+)/format/$', GGDAgreementFormat.as_view(), name='ggd_agreement_format'),
    url(r'^ggd/(?P<pk>\d+)/acknowledge/$', GGDAgreementAcknowledge.as_view(), name='ggd_agreement_acknowledge'),
    url(r'^ggd/(?P<pk>\d+)/resend/$', GGDAgreementResend.as_view(), name='ggd_agreement_resend'),
    # SDIS Agreements
    url(r'^sdis/(?P<pk>\d+)/dispatch/$', SDISAgreementDispatch.as_view(), name='sdis_agreement_dispatch'),
    url(r'^sdis/(?P<pk>\d+)/$', SDISAgreementDetail.as_view(), name='sdis_agreement_detail'),
    url(r'^sdis/(?P<pk>\d+)/acknowledge/$', SDISAgreementAcknowledge.as_view(), name='sdis_agreement_acknowledge'),
    url(r'^sdis/(?P<pk>\d+)/resend/$', SDISAgreementResend.as_view(), name='sdis_agreement_resend'),
    # DDSP Agreements
    url(r'^ddsp/(?P<pk>\d+)/dispatch/$', DDSPAgreementDispatch.as_view(), name='ddsp_agreement_dispatch'),
    url(r'^ddsp/(?P<pk>\d+)/$', DDSPAgreementDetail.as_view(), name='ddsp_agreement_detail'),
    url(r'^ddsp/(?P<pk>\d+)/acknowledge/$', DDSPAgreementAcknowledge.as_view(), name='ddsp_agreement_acknowledge'),
    url(r'^ddsp/(?P<pk>\d+)/resend/$', DDSPAgreementResend.as_view(), name='ddsp_agreement_resend'),
    # CG Agreements
    url(r'^cg/(?P<pk>\d+)/dispatch/$', CGAgreementDispatch.as_view(), name='cg_agreement_dispatch'),
    url(r'^cg/(?P<pk>\d+)/$', CGAgreementDetail.as_view(), name='cg_agreement_detail'),
    url(r'^cg/(?P<pk>\d+)/format/$', CGAgreementFormat.as_view(), name='cg_agreement_format'),
    url(r'^cg/(?P<pk>\d+)/acknowledge/$', CGAgreementAcknowledge.as_view(), name='cg_agreement_acknowledge'),
    url(r'^cg/(?P<pk>\d+)/resend/$', CGAgreementResend.as_view(), name='cg_agreement_resend'),
]
