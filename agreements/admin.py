# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from sub_agreements.admin import PreAvisCGDInline
from sub_agreements.admin import PreAvisCommissariatInline
from sub_agreements.admin import PreAvisCompagnieInline
from sub_agreements.admin import PreAvisServiceCGInline
from .models import *


class CGAvisAdmin(ImportExportActionModelAdmin):
    """ Configuration admin de l'avis CG """
    list_display = ['pk', 'state', 'request_date', 'reply_date', 'favorable', 'authorization']
    list_filter = ['request_date', 'reply_date', 'favorable', 'state']
    inlines = [PreAvisServiceCGInline]
    search_fields = ['authorization__manifestation__name', 'state', 'authorization__manifestation__description']
    list_per_page = 25


class DDSPAvisAdmin(ImportExportActionModelAdmin):
    """ Configuration admin de l'avis DDSP """
    list_display = ['pk', 'state', 'request_date', 'reply_date', 'favorable', 'authorization', 'get_formatted_commissariats']
    list_filter = ['request_date', 'reply_date', 'favorable', 'state', 'commissariats_concernes']
    inlines = [PreAvisCommissariatInline]
    search_fields = ['authorization__manifestation__name', 'state', 'authorization__manifestation__description']
    list_per_page = 25


class SDISAvisAdmin(ImportExportActionModelAdmin):
    """ Configuration admin de l'avis SDIS """
    list_display = ['pk', 'state', 'request_date', 'reply_date', 'favorable', 'authorization']
    list_filter = ['request_date', 'reply_date', 'favorable', 'state', 'authorization__manifestation__description']
    inlines = [PreAvisCompagnieInline]
    search_fields = ['authorization__manifestation__name', 'state']
    list_per_page = 25


class EDSRAvisAdmin(ImportExportActionModelAdmin):
    """ Configuration admin de l'avis EDSR """
    list_display = ['pk', 'state', 'request_date', 'reply_date', 'favorable', 'authorization']
    list_filter = ['request_date', 'reply_date', 'favorable', 'state']
    inlines = [PreAvisCGDInline]
    search_fields = ['authorization__manifestation__name', 'state', 'authorization__manifestation__description']
    list_per_page = 25


class GGDAvisAdmin(ImportExportActionModelAdmin):
    """ Configuration admin de l'avis GGD """
    list_display = ['pk', 'state', 'request_date', 'reply_date', 'favorable', 'authorization']
    list_filter = ['request_date', 'reply_date', 'favorable', 'state']
    inlines = [PreAvisCGDInline]
    search_fields = ['authorization__manifestation__name', 'state', 'authorization__manifestation__description']
    list_per_page = 25


class BaseAvisAdmin(ImportExportActionModelAdmin):
    """ Configuration admin de l'avis GGD """
    list_display = ['pk', 'state', 'request_date', 'reply_date', 'favorable', 'authorization']
    list_filter = ['request_date', 'reply_date', 'favorable', 'state']
    search_fields = ['authorization__manifestation__name', 'state', 'authorization__manifestation__description']
    list_per_page = 25


# Enregistrer les modèles dans l'admin
admin.site.register(FederationAvis, BaseAvisAdmin)
admin.site.register(ServiceAvis, BaseAvisAdmin)
admin.site.register(MairieAvis, BaseAvisAdmin)
admin.site.register(DDSPAvis, DDSPAvisAdmin)
admin.site.register(SDISAvis, SDISAvisAdmin)
admin.site.register(CGAvis, CGAvisAdmin)
admin.site.register(GGDAvis, GGDAvisAdmin)
if get_instance_settings('EDSR_OR_GGD') != 'GGD_SubAgentEDSR':
    admin.site.register(EDSRAvis, EDSRAvisAdmin)
