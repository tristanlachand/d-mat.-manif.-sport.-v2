# coding: utf-8
import datetime

from django.core.urlresolvers import reverse
from django.db import models
from django_fsm import transition

from agreements.models.avis import Avis, AvisQuerySet
from ddcs_loire.utils import add_notification_entry


class SDISAvisQuerySet(AvisQuerySet):
    """ Queryset pour les avis SDIS """

    def for_fire_service(self, fire_service):
        """ Renvoyer les avis SDIS pour le service passé """
        return self.filter(preavis__preaviscompagnie__concerned_cis=fire_service)


class SDISAvis(Avis):
    """ Avis SDIS """
    avis_ptr = models.OneToOneField("agreements.avis", parent_link=True,
                                    related_name='sdisavis')
    compagnies_concernees = models.ManyToManyField('administration.compagnie',
                                                  verbose_name="compagnies concernées")
    objects = SDISAvisQuerySet.as_manager()

    def __str__(self):
        manifestation = self.get_manifestation()
        departement = manifestation.departure_city.get_departement()
        return ' - '.join([str(manifestation), ' '.join(['SDIS', departement.name])])

    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('agreements:sdis_agreement_detail', kwargs={'pk': self.pk})

    def get_sdis(self):
        """ Renvoyer le SDIS pour l'avis """
        return self.get_manifestation().departure_city.get_departement().sdis

    def get_agents(self):
        """ Renvoyer les agents concernés par l'avis """
        return self.get_sdis().get_sdisagents()

    def preavis_validated(self):
        """ Renvoyer si tous les préavis ont été rendus """
        return super(SDISAvis, self).preavis_validated()

    def notify_ack(self, content_object):
        """ Notifier et envoyer un mail pour le rendu de l'avis """
        super(SDISAvis, self).notify_ack(content_object)
        agents_codis = self.get_manifestation().departure_city.get_departement().codis.get_codisagents()
        # Notifier les agents CODIS
        add_notification_entry(agents=agents_codis, manifestation=self.get_manifestation(), subject="avis rendu", content_object=content_object)
        # Notifier tous les agents de toutes les compagnies concernées
        add_notification_entry(agents=[agent for compagnie in self.compagnies_concernees.all() for agent in compagnie.get_compagnieagentslocaux()],
                               manifestation=self.get_manifestation(), subject='avis rendu', content_object=content_object)

    @transition(field='state', source='created', target='dispatched')
    def dispatch(self):
        """ Dispatch """
        self.log_dispatch(agents=self.get_agents())

    @transition(field='state', source='dispatched', target='acknowledged', conditions=[preavis_validated])
    def acknowledge(self):
        """ Rendre l'avis """
        self.reply_date = datetime.date.today()
        self.save()
        self.notify_ack(content_object=self.get_sdis())
        self.log_ack(agents=self.get_agents())

    class Meta:
        verbose_name = "Avis SDIS"
        verbose_name_plural = "Avis SDIS"
        app_label = 'agreements'
        default_related_name = 'sdisaviss'
