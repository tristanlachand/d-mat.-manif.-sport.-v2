# coding: utf-8
import datetime

from django.core.urlresolvers import reverse
from django_fsm import transition

from django.db import models

from agreements.models.avis import Avis, AvisQuerySet


class FederationAvisQuerySet(AvisQuerySet):
    """ Queryset des avis de fédération """

    def for_federation(self, federation):
        """
        Renvoyer tous les avis pour la fédération passée

        :param federation: objet de type administration.federation
        """
        return self.filter(authorization__manifestation__activite__discipline__federation=federation)


class FederationAvis(Avis):
    """ Avis de fédération sportive """
    avis_ptr = models.OneToOneField("agreements.avis", parent_link=True,
                                       related_name='federationavis')
    objects = FederationAvisQuerySet.as_manager()

    def __str__(self):
        manifestation = self.get_manifestation()
        return ' - '.join([str(manifestation), str(manifestation.activite.discipline.federation)])

    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('agreements:federal_agreement_detail', kwargs={'pk': self.pk})

    def get_federation(self):
        """ Renvoyer la fédération sportive de l'avis """
        return self.get_manifestation().activite.discipline.federation

    def get_agents(self):
        """ Renvoyer les agents concernés par l'avis """
        return self.get_federation().federationagents.all()  # TODO

    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        """ Rendre l'avis """
        self.reply_date = datetime.date.today()
        self.save()
        self.notify_ack(content_object=self.get_federation())
        self.log_ack(agents=self.get_agents())

    class Meta:
        verbose_name = "Avis fédération"
        verbose_name_plural = "Avis fédération"
        app_label = 'agreements'
        default_related_name = 'federationaviss'
