# coding: utf-8
import datetime

from django.core.urlresolvers import reverse
from django.db import models
from django_fsm import transition

from agreements.models.avis import AvisQuerySet, Avis
from core.util.application import get_instance_settings


class GGDAvis(Avis):
    """ Avis GGD """
    avis_ptr = models.OneToOneField("agreements.avis", parent_link=True,
                                    related_name='ggdavis')
    concerned_cgd = models.ManyToManyField('administration.cgd', verbose_name="CGD concernés")
    concerned_edsr = models.ForeignKey('administration.edsr', verbose_name="EDSR concerné")
    objects = AvisQuerySet.as_manager()

    def __str__(self):
        manifestation = self.get_manifestation()
        departure_city = manifestation.departure_city
        departement = departure_city.get_departement()
        return ' - '.join([str(manifestation), ' '.join(['GGD', departement.name])])

    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('agreements:ggd_agreement_detail', kwargs={'pk': self.pk})

    def get_edsr(self):
        """ Renvoyer l'EDSR concerné par l'avis """
        return self.concerned_edsr or self.get_manifestation().departure_city.get_departement().edsr

    def get_ggd(self):
        """ Renvoyer le GGD concerné par l'avis """
        return self.get_manifestation().departure_city.get_departement().ggd

    def get_agents(self):
        """ Renvoyer tous les agents de la chaîne de l'avis """
        ggd_agents = self.get_manifestation().departure_city.get_departement().ggd.get_ggdagents()
        edsr_agents = self.get_manifestation().departure_city.get_departement().edsr.get_edsr_agents()
        edsr_agentslocaux = self.get_manifestation().departure_city.get_departement().edsr.edsragentslocaux.all()  # TODO
        # Les agents concernés dépendent de la configuration d'application :
        # voir settings.DEPARTMENT_SETTINGS['EDSR_OR_GGD']
        # Un avis GGD n'est créé/émis que dans les configurations ggd_agentedsr et
        # ggd_agentlocaledsr. edsr ne concerne que la configuration pour les avis EDSR.
        config = get_instance_settings('EDSR_OR_GGD').lower()
        if config == 'ggd_agentedsr':
            return list(ggd_agents) + list(edsr_agents)
        elif config == 'ggd_subagentedsr':
            return list(ggd_agents) + list(edsr_agentslocaux)
        else:
            # Ne dervrait pas se produire
            return ggd_agents

    def preavis_validated(self):
        """ Renvoyer si les préavis sont tous rendus """
        return super(GGDAvis, self).preavis_validated()

    def all_agreements_validated(self):
        """ Renvoie si l'(pré)avis EDSR est formaté, et si les préavis CGD sont rendus """
        cgd_acknowledged = self.preavis_validated()
        edsr_formatted = self.state == 'formatted' or get_instance_settings('EDSR_OR_GGD').lower() != 'edsr'
        return cgd_acknowledged and edsr_formatted

    def edsr_or_ggd(self):
        config = get_instance_settings('EDSR_OR_GGD').lower()
        if config == 'ggd_agentedsr' and self.state == 'formatted':
            return True
        elif config == 'ggd_subagentedsr' and self.state == 'dispatched':
            return True
        else:
            return False

    @transition(field='state', source='created', target='dispatched')
    def dispatch(self):
        """ Dispatch """
        self.log_dispatch(agents=self.get_agents())

    @transition(field='state', source='dispatched', target='formatted', conditions=[preavis_validated])
    def format(self):
        """
        Mettre en forme l'avis (uniquement si tous les préavis sont rendus

        Et consigner à tous les agents + envoyer des mails aux agents GGD
        """
        departement = self.get_manifestation().departure_city.get_departement()
        self.notify_format(agents=departement.ggd.get_ggdagents(), content_object=self.get_ggd())
        self.log_format(agents=self.get_agents())

    @transition(field='state', source='*', target='acknowledged', conditions=[all_agreements_validated, edsr_or_ggd])
    def acknowledge(self):
        """ Rendre l'avis, si les préavis sont rendus et l'avis formaté """
        self.reply_date = datetime.date.today()
        self.save()
        ggd = self.get_manifestation().departure_city.get_departement().ggd
        self.notify_ack(content_object=ggd)
        self.log_ack(agents=ggd.get_ggdagents())

    class Meta:
        verbose_name = "Avis GGD"
        verbose_name_plural = "Avis GGD"
        app_label = 'agreements'
        default_related_name = 'ggdaviss'
