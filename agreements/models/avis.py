# coding: utf-8
import datetime
from datetime import timedelta

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils import timezone

from agreements.models.abstract import AvisBase
from ddcs_loire.utils import add_log_entry
from ddcs_loire.utils import add_notification_entry


class AvisQuerySet(models.QuerySet):
    """ Queryset par défaut pour les avis """

    def to_process(self):
        """ Renvoyer les avis pour lesquels l'événement n'est pas terminé """
        return self.filter(authorization__manifestation__end_date__gt=timezone.now())

    def acknowledged(self):
        """
        Renvoyer les avis rendus

        L'état acknowledged indique qu'un avis est rendu.
        """
        return self.filter(state='acknowledged')

    def dispatched(self):
        """ Renvoyer les avis pour lesquels les demandes de préavis ont été envoyées  """
        return self.filter(state='dispatched')

    def to_format(self):
        """
        Renvoyer les avis qui peuvent être formatés

        Avant d'être rendu, un agent autorisé peut formater un avis
        en ajoutant des informations sensibles pour l'agent en charge
        de valider l'avis ou de ne pas trancher positivement.
        """
        return self.filter(state__in=['created', 'dispatched'])

    def to_acknowledge(self):
        """
        Renvoyer les avis qui peuvent être rendus

        Les avis qui peuvent être (re)rendus sont des avis qui sont
        soit à l'état formaté (donc prêts à soumettre à l'agent probateur),
        soit à l'état rendu (dans ce cas, on peut à nouveau rendre l'avis.
        """
        return self.filter(state__in=['formatted', 'acknowledged'])

    def to_dispatch(self):
        """
        Renvoyer les avis qui peuvent être dispatchés

        On peut redispatcher un avis rendu, ou qui a déjà été dispatché (rappel aux agents)
        """
        return self.filter(state__in=['acknowledged', 'dispatched'])


class Avis(AvisBase):
    """
    Avis (classe parent commune aux autres types d'avis) (9 champs)

    Champs hérités : state, request_date, reply_date, favorable, prescriptions
    """
    authorization = models.ForeignKey('authorizations.manifestationautorisation', verbose_name="Autorisation")
    attached_document = models.FileField(max_length=512, upload_to='avis/%Y/%m/%d', blank=True, null=True, verbose_name="Pièce jointe")
    objects = AvisQuerySet.as_manager()

    def get_deadline(self):
        """ Renvoyer la date maximum de rendu de l'avis selon le type d'avis """
        delays = {'federationavis': timedelta(weeks=4)}
        default_delay = timedelta(weeks=3)
        for related_field in delays:
            try:
                getattr(self, related_field)
                return self.request_date + delays[related_field]
            except ObjectDoesNotExist:
                pass
        return self.request_date + default_delay

    def get_days_left_before_deadline(self):
        """ Renvoyer le nombre de jours restant avant la deadline d'instruction """
        return self.get_deadline() - datetime.date.today()

    def delay_exceeded(self):
        """ Renvoyer si le délai légal d'instruction a expiré """
        return datetime.date.today() > self.get_deadline()

    def get_validated_sub_agreements(self):
        """ Renvoyer les préavis validés/rendus """
        return self.preavis_set.filter(state='acknowledged')

    def preavis_validated(self):
        """ Renvoyer si tous les préavis en cours sont rendus """
        validated = self.get_validated_sub_agreements().count()
        return validated == self.preavis_set.all().count()

    def sub_agreements_issued(self):
        """ Renvoyer le nombre de préavis générés """
        return self.preavis_set.all().count()

    def sub_agreements_missing(self):
        """ Renvoyer le nombre de préavis non rendus/validés """
        return self.sub_agreements_issued() - self.get_validated_sub_agreements().count()

    def get_manifestation(self):
        """ Renvoyer l'événement sportif pour cet avis """
        return self.authorization.manifestation

    def log_ack(self, agents):
        """
        Consigner l'événement lorsque l'avis est rendu

        :param agents: agents/utilisateurs qui recevront le log
        """
        add_log_entry(agents=agents, action="avis rendu", manifestation=self.get_manifestation())

    def log_dispatch(self, agents):
        """
        Consigner l'événement dispatch : les demandes de préavis sont envoyées

        :param agents: agents/utilisateurs qui recevront le log
        """
        add_log_entry(agents=agents, action="demandes de pré-avis envoyées", manifestation=self.get_manifestation())

    def log_format(self, agents):
        """
        Consigner l'événement lorsque l'avis est formaté/mis en forme

        :param agents: agents/utilisateurs qui recevront le log
        """
        add_log_entry(agents=agents, action="avis mis en forme", manifestation=self.get_manifestation())

    def log_resend(self, recipient):
        """
        Consigner l'événement lorsque qu'une demande d'avis est relancée

        Les agents notifiés sont les instructeurs attachés à la préfecture de l'autorisation
        :param recipient: TODO: Documenter
        """
        prefecture = self.authorization.get_concerned_prefecture()
        add_log_entry(agents=prefecture.get_instructeurs(),
                      action="demande d'avis relancée : " + recipient,
                      manifestation=self.get_manifestation())

    def notify_creation(self, agents, institutional=None):
        """
        Consigner l'événement et envoyer un mail aux agents lorsque l'avis est créé

        :param agents: agents qui reçoivent le mail et sont notifiés
        :param institutional: un objet avec un champ email, ex. Préfecture etc. qui sera notifié par mail
        """
        try:
            prefecture = self.authorization.get_concerned_prefecture()
            add_notification_entry(agents=agents, manifestation=self.get_manifestation(), subject="avis requis", content_object=prefecture, institutional=institutional)
        except ObjectDoesNotExist:
            pass

    def notify_ack(self, content_object):
        """
        Consigner l'événement et envoyer un mail aux instructeurs lorsqu'un avis est rendu

        Envoie également un mail à la préfecture (institutional)
        :param content_object: TODO: à documenter. L'objet fait partie de la notification
        """
        try:
            prefecture = self.authorization.get_concerned_prefecture()
            add_notification_entry(agents=prefecture.get_instructeurs(), manifestation=self.get_manifestation(), subject="avis rendu", content_object=content_object,
                                   institutional=prefecture)
        except ObjectDoesNotExist:
            pass

    def notify_format(self, agents, content_object):
        """ Consigner et envoyer un mail lorsque l'avis est formaté """
        add_notification_entry(agents=agents, manifestation=self.get_manifestation(), subject="avis mis en forme", content_object=content_object)

    def notify_publication(self, agents, institutional=None):
        """ Consigner et envoyer un mail lorsque l'arrêté (bylaw) est publié """
        prefecture = self.authorization.get_concerned_prefecture()
        add_notification_entry(agents=agents, manifestation=self.get_manifestation(), subject='arrêté publié', content_object=prefecture, institutional=institutional)

    class Meta:
        verbose_name = "Avis"
        verbose_name_plural = "Avis"
        ordering = ["authorization__manifestation__begin_date"]
        app_label = 'agreements'
        default_related_name = 'aviss'
