# coding: utf-8
import datetime

from django.core.urlresolvers import reverse
from django.db import models
from django_fsm import transition

from agreements.models.avis import Avis, AvisQuerySet


class EDSRAvis(Avis):
    """ Avis EDSR """
    avis_ptr = models.OneToOneField("agreements.avis", parent_link=True,
                                    related_name='edsravis')
    concerned_cgd = models.ManyToManyField('administration.cgd',
                                           verbose_name="compagnies de gendarmerie concernées")
    objects = AvisQuerySet.as_manager()

    def __str__(self):
        manifestation = self.get_manifestation()
        departement = manifestation.departure_city.get_departement()
        return ' - '.join([str(manifestation), ' '.join(['EDSR', departement.name])])

    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('agreements:edsr_agreement_detail', kwargs={'pk': self.pk})

    def get_edsr(self):
        """ Renvoyer l'EDSR concerné par l'avis """
        return self.get_manifestation().departure_city.get_departement().edsr

    def get_agents(self):
        """
        Renvoyer les agents concernés par l'avis

        TODO: S'assurer que les agents sont uniquement les agents EDSR :
        le workflow EDSR indique qu'un agent GGD rend l'avis, et que les
        agents CGD existent aussi dans les workflow.
        """
        return self.get_edsr().get_edsr_agents()

    def preavis_validated(self):
        """ Renvoyer si les préavis sont tous rendus """
        return super(EDSRAvis, self).preavis_validated()

    @transition(field='state', source='created', target='dispatched')
    def dispatch(self):
        """ Dispatch """
        self.log_dispatch(agents=self.get_agents())

    @transition(field='state', source='dispatched', target='formatted', conditions=[preavis_validated])
    def format(self):
        """ Mettre en forme l'avis """
        departement = self.get_manifestation().departure_city.get_departement()
        self.notify_format(agents=departement.ggd.get_ggdagents(), content_object=self.get_edsr())
        self.log_format(agents=self.get_agents())

    @transition(field='state', source='formatted', target='acknowledged')
    def acknowledge(self):
        """ Rendre l'avis """
        self.reply_date = datetime.date.today()
        self.save()
        ggd = self.get_manifestation().departure_city.get_departement().ggd
        self.notify_ack(content_object=ggd)
        self.log_ack(agents=ggd.get_ggdagents())

    class Meta:
        verbose_name = "Avis EDSR"
        verbose_name_plural = "Avis EDSR"
        app_label = 'agreements'
        default_related_name = 'edsraviss'
