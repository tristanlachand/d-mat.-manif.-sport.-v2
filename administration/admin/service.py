# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from administration.models.service import Prefecture, CGService, Service, EDSR, SDIS, DDSP, CG, GGD, \
    CODIS, Brigade, CGD, Compagnie, Commissariat, CIS


class PrefectureAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'arrondissement', 'get_departement', 'email']
    search_fields = ['email', 'departement__name']


class ServiceAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'get_departement', 'name']
    search_fields = ['name']


class DepartementAndEmailAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'get_departement', 'email']


class DepartementOnlyAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'get_departement']


class CommuneOnlyAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'get_commune', 'get_departement']


class CISAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'get_commune', 'get_departement', 'compagnie']
    list_filter = ['compagnie']
    search_fields = ['name']


class BrigadeAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'get_commune', 'get_departement', 'kind', 'cgd']
    list_filter = ['cgd', 'kind']
    search_fields = ['commune__name']


class CGDAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'arrondissement', 'get_departement']
    list_filter = ['arrondissement__departement']
    search_fields = ['arrondissement__name', 'arrondissement__departement__name']


class CompagnieAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'sdis', 'number']
    list_filter = ['sdis']
    search_fields = ['number']


class CGServiceAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'name', 'cg', 'service_type']
    list_filter = ['service_type', 'cg']
    search_fields = ['name']


# Enregistrer les classes d'admin
admin.site.register(Prefecture, PrefectureAdmin)
admin.site.register(CGService, CGServiceAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(EDSR, DepartementOnlyAdmin)
admin.site.register(GGD, DepartementOnlyAdmin)
admin.site.register(CODIS, DepartementOnlyAdmin)
admin.site.register(SDIS, DepartementOnlyAdmin)
admin.site.register(DDSP, DepartementAndEmailAdmin)
admin.site.register(CG, DepartementAndEmailAdmin)
admin.site.register(Brigade, BrigadeAdmin)
admin.site.register(CGD, CGDAdmin)
admin.site.register(Compagnie, CompagnieAdmin)
admin.site.register(Commissariat, CommuneOnlyAdmin)
admin.site.register(CIS, CISAdmin)
