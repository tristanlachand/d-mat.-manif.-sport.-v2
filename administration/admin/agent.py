# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from administration.models.agent import CGAgent, CGSuperieur, GGDAgent, DDSPAgent, EDSRAgent, BrigadeAgent, SDISAgent, CODISAgent, CISAgent, ServiceAgent, \
    FederationAgent, MairieAgent
from core.util.application import get_instance_settings


class CGAgentAdmin(ImportExportActionModelAdmin):
    """ Configuration admin CGAgent et CGSupérieur """
    list_display = ['pk', 'agent', 'cg']
    list_filter = ['cg']
    search_fields = ['user__username']


class GGDAgentAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agent', 'ggd']
    list_filter = ['ggd']
    search_fields = ['user__username']


class EDSRAgentAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agent', 'edsr']
    list_filter = ['edsr']
    search_fields = ['user__username']


class DDSPAgentAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agent', 'ddsp']
    list_filter = ['ddsp']
    search_fields = ['user__username']


class SDISAgentAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agent', 'sdis']
    list_filter = ['sdis']
    search_fields = ['user__username']


class CODISAgentAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agent', 'codis']
    list_filter = ['codis']
    search_fields = ['user__username']


class CISAgentAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agent', 'cis']
    list_filter = ['cis']
    search_fields = ['user__username']


class ServiceAgentAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agent', 'service']
    list_filter = ['service']
    search_fields = ['user__username']


class FederationAgentAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agent', 'federation']
    list_filter = ['federation']
    search_fields = ['user__username']


class MairieAgentAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agent', 'commune']
    list_filter = ['commune']
    search_fields = ['user__username']


class BrigadeAgentAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agent', 'brigade']
    list_filter = ['brigade']
    search_fields = ['user__username']


# Enregistrer les classes d'admin
admin.site.register(CGAgent, CGAgentAdmin)
admin.site.register(CGSuperieur, CGAgentAdmin)
admin.site.register(GGDAgent, GGDAgentAdmin)
admin.site.register(DDSPAgent, DDSPAgentAdmin)
admin.site.register(BrigadeAgent, BrigadeAgentAdmin)
admin.site.register(SDISAgent, SDISAgentAdmin)
admin.site.register(CODISAgent, CODISAgentAdmin)
admin.site.register(CISAgent, CISAgentAdmin)
admin.site.register(ServiceAgent, ServiceAgentAdmin)
admin.site.register(FederationAgent, FederationAgentAdmin)
admin.site.register(MairieAgent, MairieAgentAdmin)
if get_instance_settings('EDSR_OR_GGD') != 'GGD_SubAgentEDSR':
    admin.site.register(EDSRAgent, EDSRAgentAdmin)
