# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from administration.models.agentlocal import CGServiceAgentLocal, CGDAgentLocal, CompagnieAgentLocal, CommissariatAgentLocal, \
    EDSRAgentLocal
from core.util.application import get_instance_settings


class CGServiceAgentLocalAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agentlocal', 'cg_service']
    list_filter = ['cg_service']
    search_fields = ['user__username']


class CompagnieAgentLocalAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agentlocal', 'compagnie']
    list_filter = ['compagnie']
    search_fields = ['user__username']


class CommissariatAgentLocalAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agentlocal', 'commissariat']
    list_filter = ['commissariat']
    search_fields = ['user__username']


class CGDAgentLocalAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agentlocal', 'cgd']
    list_filter = ['cgd']
    search_fields = ['user__username']


class EDSRAgentLocalAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'agentlocal', 'edsr']
    list_filter = ['edsr']
    search_fields = ['user__username']


# Enregistrer les classes d'admin
admin.site.register(CGServiceAgentLocal, CGServiceAgentLocalAdmin)
admin.site.register(CGDAgentLocal, CGDAgentLocalAdmin)
admin.site.register(CompagnieAgentLocal, CompagnieAgentLocalAdmin)
admin.site.register(CommissariatAgentLocal, CommissariatAgentLocalAdmin)
if get_instance_settings('EDSR_OR_GGD') == 'GGD_SubAgentEDSR':
    admin.site.register(EDSRAgentLocal, EDSRAgentLocalAdmin)
