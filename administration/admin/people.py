# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from administration.models.people import Instructeur, Secouriste


class InstructeurAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'user', 'prefecture']
    list_filter = ['prefecture']
    search_fields = ['user__username']


class SecouristeAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """
    list_display = ['pk', 'user', 'association']
    list_filter = ['association']
    search_fields = ['user__username']


# Enregistrer les classes d'admin
admin.site.register(Instructeur, InstructeurAdmin)
admin.site.register(Secouriste, SecouristeAdmin)
