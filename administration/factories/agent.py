# coding: utf-8
import factory
from factory.django import DjangoModelFactory

from administration.factories.service import ServiceFactory
from administration.models.agent import ServiceAgent, MairieAgent, FederationAgent
from administration.models.agentlocal import AgentLocal
from administrative_division.factories import CommuneFactory
from events.factories import UserFactory
from sports.factories import FederationFactory


class ServiceAgentFactory(DjangoModelFactory):
    """ Factory """
    user = factory.SubFactory(UserFactory)
    service = factory.SubFactory(ServiceFactory)

    class Meta:
        model = ServiceAgent


class MairieAgentFactory(DjangoModelFactory):
    """ Factory """
    user = factory.SubFactory(UserFactory)
    commune = factory.SubFactory(CommuneFactory)

    class Meta:
        model = MairieAgent


class FederationAgentFactory(DjangoModelFactory):
    """ Factory """
    user = factory.SubFactory(UserFactory)
    federation = factory.SubFactory(FederationFactory)

    class Meta:
        model = FederationAgent
