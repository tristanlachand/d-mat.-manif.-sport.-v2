# coding: utf-8
import factory
from factory.django import DjangoModelFactory

from administration.factories.service import PrefectureFactory
from administration.models.people import Instructeur
from events.factories import UserFactory


class InstructeurFactory(DjangoModelFactory):
    """ Factory """
    user = factory.SubFactory(UserFactory)
    prefecture = factory.SubFactory(PrefectureFactory)

    class Meta:
        model = Instructeur
