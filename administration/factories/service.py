# coding: utf-8
import factory
from factory.django import DjangoModelFactory

from administration.models.service import GGD, SDIS, CG, DDSP, EDSR, Prefecture, Service, Commissariat, CGD, Compagnie, CIS, CGService
from administrative_division.factories import ArrondissementFactory
from administrative_division.factories import CommuneFactory
from administrative_division.factories import DepartementFactory


class GGDFactory(DjangoModelFactory):
    """ Factory """
    departement = factory.SubFactory(DepartementFactory)

    class Meta:
        model = GGD


class SDISFactory(DjangoModelFactory):
    """ Factory """
    departement = factory.SubFactory(DepartementFactory)

    class Meta:
        model = SDIS


class CGFactory(DjangoModelFactory):
    """ Factory """
    departement = factory.SubFactory(DepartementFactory)

    class Meta:
        model = CG


class DDSPFactory(DjangoModelFactory):
    """ Factory """
    departement = factory.SubFactory(DepartementFactory)

    class Meta:
        model = DDSP


class EDSRFactory(DjangoModelFactory):
    """ Factory """
    departement = factory.SubFactory(DepartementFactory)

    class Meta:
        model = EDSR


class PrefectureFactory(DjangoModelFactory):
    """ Factory """
    sous_prefecture = True
    arrondissement = factory.SubFactory(ArrondissementFactory)
    email = factory.Sequence(lambda n: 'pref{0}@loire.gouv.fr'.format(n))

    class Meta:
        model = Prefecture


class ServiceFactory(DjangoModelFactory):
    """ Factory """
    name = 'sncf'
    departement = factory.SubFactory(DepartementFactory)

    class Meta:
        model = Service


class CommissariatFactory(DjangoModelFactory):
    """ Factory """
    commune = factory.SubFactory(CommuneFactory)

    class Meta:
        model = Commissariat


class CGDFactory(DjangoModelFactory):
    """ Factory """
    arrondissement = factory.SubFactory(ArrondissementFactory)

    class Meta:
        model = CGD


class CompagnieFactory(DjangoModelFactory):
    """ Factory """
    sdis = factory.SubFactory(SDISFactory)
    number = factory.Sequence(lambda n: '%d.%d' % (n, n))

    class Meta:
        model = Compagnie


class CISFactory(DjangoModelFactory):
    """ Factory """
    commune = factory.SubFactory(CommuneFactory)
    compagnie = factory.SubFactory(CompagnieFactory)

    class Meta:
        model = CIS


class CGServiceFactory(DjangoModelFactory):
    """ Factory """
    cg = factory.SubFactory(CGFactory)
    name = factory.Sequence(lambda n: 'service_name.%d' % n)
    service_type = factory.Iterator(CGService.SERVICE_TYPE_CHOICES, getter=lambda c: c[0])

    class Meta:
        model = CGService
