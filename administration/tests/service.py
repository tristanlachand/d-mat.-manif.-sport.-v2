# coding: utf-8
from django.test import TestCase
from unidecode import unidecode

from administration.factories import *


class PrefectureTests(TestCase):
    """ Tester les fonctionnalités du modèle Préfecture """

    def test_prefecture_string(self):
        """ Teste que la méthode __str__ indique bien le type de préfecture """
        sous_prefecture = PrefectureFactory.build(sous_prefecture=True)
        prefecture = PrefectureFactory.build(sous_prefecture=False)
        # La sous-préfecture doit contenir le nom 'sous-préfecture'
        self.assertIn("sous-prefecture", unidecode(str(sous_prefecture).lower()))
        # La préfecture contient préfecture mais pas sous-préfecture
        self.assertIn("prefecture", unidecode(str(prefecture).lower()))
        self.assertNotIn("sous-prefecture", unidecode(str(prefecture).lower()))


class InstructeurTests(TestCase):
    """ Teste les fonctionnalités de l'instructeur """

    def test_instructeur_string(self):
        """
        Tester que la méthode __str__ indique bien le nom d'utilisateur

        TODO: Test inutile
        """
        instructeur = InstructeurFactory.build(user__username='jdupont')
        self.assertIn('jdupont', str(instructeur))


class ServiceTests(TestCase):
    """ Teste les fonctionnalités des services divers """

    def test_service_string(self):
        """ Tester que __str__ renvoie bien les bonnes informations """
        service = ServiceFactory.create()
        self.assertIn(service.get_departement().name, str(service))
        self.assertIn(service.name, str(service))


class CISTests(TestCase):
    """ Teste les fonctionnalités du service CIS (incendies) """

    def test_cis_string(self):
        """ Tester que __str__ renvoie les bonnes informations """
        cis_service = CISFactory.build()
        self.assertIn(cis_service.get_commune().name, str(cis_service))
        self.assertIn('cis', str(cis_service).lower())
