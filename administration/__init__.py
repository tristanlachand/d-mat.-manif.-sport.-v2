# coding: utf-8
from django.apps import AppConfig

default_app_config = 'administration.AdministrationConfig'


class AdministrationConfig(AppConfig):
    """ Configuration de l'application Administration """
    name = 'administration'
    verbose_name = "Administration"
