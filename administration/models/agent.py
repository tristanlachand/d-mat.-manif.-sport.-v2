# coding: utf-8
from django.db import models

from administrative_division.mixins.divisions import ManyPerCommuneMixin


class Agent(models.Model):
    """ Base agent départemental """
    user = models.OneToOneField('auth.user', related_name="agent", verbose_name='utilisateur')

    def __str__(self):
        return self.user.get_username()

    class Meta:
        verbose_name = "Agent"
        verbose_name_plural = "Agents"
        app_label = 'administration'
        default_related_name = 'agents'


class GGDAgent(Agent):
    """ Agent départemental GGD """
    ggd = models.ForeignKey('administration.ggd', verbose_name='GGD')
    agent = models.OneToOneField('administration.agent', related_name="ggdagent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "GGD - Agent départemental"
        verbose_name_plural = "GGD - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'ggdagents'


class EDSRAgent(Agent):
    """ Agent départemental EDSR (assigné aux avis) """
    edsr = models.ForeignKey('administration.edsr', verbose_name='EDSR')
    agent = models.OneToOneField('administration.agent', related_name="edsragent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "EDSR - Agent départmental"
        verbose_name_plural = "EDSR - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'edsragents'


class BrigadeAgent(Agent):
    """ Agent de brigade EDSR """
    brigade = models.ForeignKey('administration.brigade', verbose_name='brigade')
    agent = models.OneToOneField('administration.agent', related_name="brigadeagent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "EDSR - Brigade - Agent départemental"
        verbose_name_plural = "EDSR - Brigade - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'brigadeagents'


class CODISAgent(Agent):
    """ Agent SDIS / CODIS """
    codis = models.ForeignKey('administration.codis',
                              verbose_name='CODIS')
    agent = models.OneToOneField('administration.agent', related_name="codisagent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "SDIS - CODIS - Agent départemental"
        verbose_name_plural = "SDIS - CODIS - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'codisagents'


class SDISAgent(Agent):
    """ Agent SDIS """
    sdis = models.ForeignKey('administration.sdis',
                             verbose_name='SDIS')
    agent = models.OneToOneField('administration.agent', related_name="sdisagent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "SDIS - Agent départemental"
        verbose_name_plural = "SDIS - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'sdisagents'


class CISAgent(Agent):
    """ Agent CIS (!) """
    cis = models.ForeignKey('administration.cis', verbose_name='CIS')
    agent = models.OneToOneField('administration.agent', related_name="cisagent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "SDIS - CIS - Agent départemental"
        verbose_name_plural = "SDIS - CIS - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'cisagents'


class DDSPAgent(Agent):
    """ Agent DDSP départemental """
    ddsp = models.ForeignKey('administration.ddsp', verbose_name='DDSP')
    agent = models.OneToOneField('administration.agent', related_name="ddspagent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "DDSP - Agent départemental"
        verbose_name_plural = "DDSP - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'ddspagents'


class FederationAgent(Agent):
    """ Agent de fédération sportive """
    federation = models.ForeignKey('sports.federation', verbose_name='fédération')
    agent = models.OneToOneField('administration.agent', related_name="federationagent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "Fédération - Agent départemental"
        verbose_name_plural = "Fédération - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'federationagents'


class MairieAgent(Agent, ManyPerCommuneMixin):
    """ Agenr de mairie (départemental ?) """
    agent = models.OneToOneField('administration.agent', related_name="mairieagent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "Agent municipal"
        verbose_name_plural = "Agents municipaux"
        app_label = 'administration'
        default_related_name = 'mairieagents'


class CGAgent(Agent):
    """ Agent du Conseil général """
    cg = models.ForeignKey('administration.cg', verbose_name='CG')
    agent = models.OneToOneField('administration.agent', related_name="cgagent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "CG - Agent départemental"
        verbose_name_plural = 'CG - Agents départementaux'
        app_label = 'administration'
        default_related_name = 'cgagents'


class CGSuperieur(Agent):
    """ Agent supérieur du Conseil général """
    cg = models.ForeignKey('administration.cg', verbose_name='CG')
    agent = models.OneToOneField('administration.agent', related_name="cgsuperieuragent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "CG - Agent départemental n+1"
        verbose_name_plural = "CG - Agents départementaux n+1"
        app_label = 'administration'
        default_related_name = 'cgsuperieurs'


class ServiceAgent(Agent):
    """ Agent d'un service départemental """
    service = models.ForeignKey('administration.service', verbose_name='service')
    agent = models.OneToOneField('administration.agent', related_name="serviceagent", parent_link=True, verbose_name="ID Agent")

    class Meta:
        verbose_name = "Autre service - Agent départemental"
        verbose_name_plural = "Autre service - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'serviceagents'
