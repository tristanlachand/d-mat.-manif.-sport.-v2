# coding: utf-8
from __future__ import unicode_literals

from django.db import models

from administrative_division.mixins.divisions import OnePerDepartementMixin, ManyPerDepartementMixin, OnePerArrondissementMixin, ManyPerCommuneMixin


class GGD(OnePerDepartementMixin):
    """ Groupe départemental de gendarmerie """

    def __str__(self):
        return ' '.join(['GGD', self.get_departement().name])

    def get_ggdagents(self):
        """ Renvoyer les agents du GGD """
        return self.ggdagents.all()

    class Meta:
        verbose_name = "GGD"
        verbose_name_plural = "GGD"
        app_label = 'administration'
        # default_related_name = 'ggds'


class CG(OnePerDepartementMixin):
    """ Conseil général de département """
    email = models.EmailField("e-mail", max_length=200)

    def __str__(self):
        return ' '.join(['CG', self.get_departement().name])

    def get_cgsuperieurs(self):
        """ Renvoyer les agents n+1 du CG """
        return self.cgsuperieurs.all()

    def get_cgagents(self):
        """ Renvoyer les agents n du CG """
        return self.cgagents.all()

    class Meta:
        verbose_name = "CG"
        verbose_name_plural = "CG"
        app_label = 'administration'
        # default_related_name = 'cgs'


class DDSP(OnePerDepartementMixin):
    """ Direction départementale de sécurité publique """
    email = models.EmailField("e-mail", max_length=200)

    def __str__(self):
        return ' '.join(['DDSP', self.get_departement().name])

    def get_ddspagents(self):
        """ Renvoyer les agents DDSP """
        return self.ddspagents.all()

    class Meta:
        verbose_name = 'DDSP'
        verbose_name_plural = 'DDSP'
        app_label = 'administration'
        # default_related_name = 'ddsps'


class SDIS(OnePerDepartementMixin):
    """ Service départemental incendies et secourisme """

    def __str__(self):
        return ' '.join(['SDIS', self.get_departement().name])

    def get_sdisagents(self):
        """ Renvoyer les agents SDIS """
        return self.sdisagents.all()

    class Meta:
        verbose_name = 'SDIS'
        verbose_name_plural = 'SDIS'
        app_label = 'administration'
        # default_related_name = 'sdiss'


class CODIS(OnePerDepartementMixin):
    """ SDIS : Centre d'opérations départemental incendies et secourisme """

    def __str__(self):
        return ' '.join(['CODIS', self.get_departement().name])

    def get_codisagents(self):
        """ Renvoyer les agents codis """
        return self.codisagents.all()

    class Meta:
        verbose_name = 'SDIS - CODIS'
        verbose_name_plural = 'SDIS - CODIS'
        app_label = 'administration'
        # default_related_name = 'codiss'


class EDSR(OnePerDepartementMixin):
    """ Escadron départemental de sécurité routière """

    def __str__(self):
        return ' '.join(['EDSR', self.get_departement().name])

    def get_edsr_agents(self):
        """ Renvoyer les agents EDSR """
        return self.edsragents.all()

    class Meta:
        verbose_name = 'EDSR'
        verbose_name_plural = 'EDSR'
        app_label = 'administration'
        # default_related_name = 'edsrs'


class Prefecture(OnePerArrondissementMixin):
    """ Préfécture (et sous-préfectures) """
    sous_prefecture = models.BooleanField("Sous-préfecture", default=False)
    email = models.EmailField('e-mail', max_length=200)

    def __str__(self):
        typename = "sous-préfecture" if self.sous_prefecture else "préfecture"
        return ' '.join([typename.capitalize(), 'de', self.arrondissement.name])

    def get_instructeurs(self):
        """ Renvoyer les instructeurs de la préfecture """
        return self.instructeurs.all()

    class Meta:
        verbose_name = 'Préfecture'
        verbose_name_plural = 'Préfectures'
        app_label = 'administration'
        # default_related_name = 'prefectures'


class CGD(OnePerArrondissementMixin):
    """ Compagnie de gendarmerie départementale """

    def __str__(self):
        return ' - '.join(['CGD', self.arrondissement.name])

    class Meta:
        verbose_name = 'CGD'
        verbose_name_plural = 'CGD'
        app_label = 'administration'
        # default_related_name = 'cgds'


class Brigade(ManyPerCommuneMixin):
    """ Brigade EDSR """
    KIND_CHOICES = (('cob', 'Brigade de communauté'), ('bta', 'Brigade autonome'))

    kind = models.CharField('type', max_length=12, choices=KIND_CHOICES)
    cgd = models.ForeignKey('administration.cgd', verbose_name='CGD')

    def __str__(self):
        return ' - '.join([self.kind.upper(), self.commune.name])

    class Meta:
        verbose_name = 'EDSR - Brigade'
        verbose_name_plural = 'EDSR - Brigades'
        app_label = 'administration'
        default_related_name = 'brigades'


class Compagnie(models.Model):
    """ Compagnie SDIS (Incendie et secourisme) """
    sdis = models.ForeignKey('administration.sdis', verbose_name='SDIS')
    number = models.CharField("Numéro de compagnie/groupement", max_length=12)

    def __str__(self):
        return ' '.join(['compagnie/groupement', self.number])

    def get_compagnieagentslocaux(self):
        """ Renvoyer les agents locaux """
        return self.compagnieagentslocaux.all()

    class Meta:
        verbose_name = 'SDIS - Compagnie/groupement'
        verbose_name_plural = 'SDIS - Compagnies/groupements'
        ordering = ['number']
        app_label = 'administration'
        default_related_name = 'compagnies'


class CIS(ManyPerCommuneMixin):
    """ Caserne / Service feu """
    name = models.CharField("Nom", max_length=255, blank=True)
    compagnie = models.ForeignKey('administration.compagnie', verbose_name='compagnie')

    def __str__(self):
        return ' '.join(filter(None, ['CIS', self.commune.name.capitalize(), self.name]))

    class Meta:
        verbose_name = 'SDIS - CIS'
        verbose_name_plural = 'SDIS - CIS'
        app_label = 'administration'
        default_related_name = 'ciss'


class Commissariat(ManyPerCommuneMixin):
    """ Commissariat de police """

    def __str__(self):
        return ' '.join(['commissariat', self.commune.name])

    class Meta:
        verbose_name = 'DDSP - Commissariat'
        verbose_name_plural = 'DDSP - Commissariats'
        app_label = 'administration'
        default_related_name = 'commissariats'


class Service(ManyPerDepartementMixin):
    """ Autre service """
    name = models.CharField("Nom", max_length=255, blank=False)

    def __str__(self):
        return ' - '.join([self.get_departement().name, self.name])

    def get_serviceagents(self):
        """ Renvoyer les agents """
        return self.serviceagents.all()

    class Meta:
        verbose_name = "Autre service"
        verbose_name_plural = "Autres services"
        unique_together = [['name', 'departement']]
        app_label = 'administration'
        default_related_name = 'services'


class CGService(models.Model):
    """ Service de conseil général """
    SERVICE_TYPE_CHOICES = (
        ('STD', 'Service technique départemental'),
        ('DTM', 'Direction des transports et de la mobilité'),
    )

    name = models.CharField("Nom", max_length=255)
    cg = models.ForeignKey('administration.cg', verbose_name='CG')
    service_type = models.CharField("type de service", max_length=3, choices=SERVICE_TYPE_CHOICES)

    def __str__(self):
        return ' '.join([self.service_type, self.name])

    class Meta:
        verbose_name = 'CG - Service'
        verbose_name_plural = 'CG - Services'
        app_label = 'administration'
        default_related_name = 'cgservices'
