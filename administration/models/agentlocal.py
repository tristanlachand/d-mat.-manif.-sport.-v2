# coding: utf-8
from django.db import models


class AgentLocal(models.Model):
    """ Base agent local """
    user = models.OneToOneField('auth.user', related_name="agentlocal", verbose_name='utilisateur')

    def __str__(self):
        return self.user.get_username()

    class Meta:
        verbose_name = "Agent local"
        verbose_name_plural = "Agents locaux"
        app_label = 'administration'
        default_related_name = 'agentslocaux'


class EDSRAgentLocal(AgentLocal):
    """ Agent EDSR local (assigné aux préavis uniquement) """
    edsr = models.ForeignKey('administration.edsr', verbose_name='EDSR')
    agentlocal = models.OneToOneField('administration.agentlocal', parent_link=True, related_name='edsragentlocal',
                                      verbose_name="ID Agent Local")

    class Meta:
        verbose_name = "EDSR - Agent local"
        verbose_name_plural = "EDSR - Agents locaux"
        app_label = 'administration'
        default_related_name = 'edsragentslocaux'


class CGDAgentLocal(AgentLocal):
    """ Agent CGD (local) """
    cgd = models.ForeignKey('administration.cgd', verbose_name='CGD')
    agentlocal = models.OneToOneField('administration.agentlocal', parent_link=True, related_name='cgdagentlocal',
                                      verbose_name="ID Agent Local")

    class Meta:
        verbose_name = "CGD - Agent local"
        verbose_name_plural = "CGD - Agents locaux"
        app_label = 'administration'
        default_related_name = 'ggdagentslocaux'


class CompagnieAgentLocal(AgentLocal):
    """ Agent local de Compagnie SDIS """
    compagnie = models.ForeignKey('administration.compagnie', verbose_name='compagnie/groupement')
    agentlocal = models.OneToOneField('administration.agentlocal', parent_link=True, related_name='compagnieagentlocal',
                                      verbose_name="ID Agent Local")

    class Meta:
        verbose_name = "SDIS - Compagnie/groupement - Agent local"
        verbose_name_plural = "SDIS - Compagnie/groupement - Agents locaux"
        app_label = 'administration'
        default_related_name = 'compagnieagentslocaux'


class CommissariatAgentLocal(AgentLocal):
    """ Agent local de commissariat """
    commissariat = models.ForeignKey('administration.commissariat', verbose_name='commissariat')
    agentlocal = models.OneToOneField('administration.agentlocal', parent_link=True, related_name='commissariatagentlocal',
                                      verbose_name="ID Agent Local")

    class Meta:
        verbose_name = "DDSP - Commissariat - Agent local"
        verbose_name_plural = "DDSP - Commissariat - Agents locaux"
        app_label = 'administration'
        default_related_name = 'commissariatagentslocaux'


class CGServiceAgentLocal(AgentLocal):
    """ Agent local d'un service du Conseil général """
    cg_service = models.ForeignKey('administration.cgservice', verbose_name='Service CG')
    agentlocal = models.OneToOneField('administration.agentlocal', parent_link=True, related_name='cgserviceagentlocal',
                                      verbose_name="ID Agent Local")

    class Meta:
        verbose_name = "CG - Service - Agent local"
        verbose_name_plural = "CG - Service - Agents locaux"
        app_label = 'administration'
        default_related_name = 'cgserviceagentslocaux'
