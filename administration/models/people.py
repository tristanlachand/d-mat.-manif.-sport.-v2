# coding: utf-8
from __future__ import unicode_literals

from django.db import models


class Instructeur(models.Model):
    """ Instructeur à la préfecture : instruit le dossier """
    user = models.OneToOneField('auth.user', related_name="instructeur", verbose_name='utilisateur')
    prefecture = models.ForeignKey('administration.prefecture', verbose_name='préfecture')

    def __str__(self):
        return self.user.get_username()

    class Meta:
        verbose_name = "Préfecture - Instructeur"
        verbose_name_plural = "Préfecture - Instructeurs"
        app_label = 'administration'
        default_related_name = 'instructeurs'


class Observateur(models.Model):
    """ Observateur """
    user = models.OneToOneField('auth.user', related_name="observateur", verbose_name='utilisateur')

    def __str__(self):
        return self.user.get_username()

    class Meta:
        app_label = 'administration'
        verbose_name = "Observateur"
        verbose_name_plural = "Observateurs"
        default_related_name = 'observateurs'


class Secouriste(Observateur):
    """ Observateur : Secouriste """
    observateur_ptr = models.OneToOneField("administration.observateur", parent_link=True,
                                           related_name='secouriste')
    association = models.ForeignKey('emergencies.association1erssecours',
                                    verbose_name="Association de premiers secours")

    class Meta:
        verbose_name = "Secouriste"
        verbose_name_plural = "Secouristes"
        app_label = 'administration'
        default_related_name = 'secouristes'
